//
//  ViewController.m
//  MobilePortal
//
//  Created by Chenhc on 15/3/3.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "MPLoginController.h"
#import "SlideMenuController.h"
#import "MPLeftMenuController.h"
#import "MPMyAppController.h"
#import "CTNavigationController.h"
#import "CTCheckBox.h"
#import "MPSettingViewController.h"
#import "MPCommentViewController.h"
#import "MPAboutViewController.h"
#import "MPAppCenterViewController.h"
#import "MPTrackViewController.h"
#import "MPStoreManager.h"
#import "MPWebService.h"
#import "MPAuthViewController.h"

@interface MPLoginController () <LeftMenuDelegate, MainDelegate, CTCheckBoxDelegate, UITextFieldDelegate, MPTrackViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *inputBg;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIView *cbContainer;
@property (weak, nonatomic) IBOutlet UITextField *tfUserName;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;
@property (weak, nonatomic) IBOutlet UILabel *version;
@property (nonatomic, nonatomic) CTCheckBox *cbRememberPwd;
@property (nonatomic, nonatomic) CTCheckBox *cbAutoLogin;
@property (nonatomic, strong) SlideMenuController * slideMenu;
@property (nonatomic, strong) MPTrackViewController * trackController;

- (IBAction)login:(id)sender;

@end

@implementation MPLoginController


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)login:(id)sender
{
    BiPerson * user = [MPWebService user];
    if (user == nil) {
        user = [[BiPerson alloc] init];
    }
    user.userName = @"longwc";
    user.personCode = @"12010007";
    user.password = @"1";
    user.orgCode = @"13500200";
    user.deptCode = @"135002000017";
    user.personName = @"龙伟超";
    user.sessionCode = @"AS123S3J35BBDS923";
    user.deptName = @"海沧客户服务部";
    user.duty = @"GWDMZ";
    user.dutyName = @"客户经理";
    user.orgName = @"厦门市烟草专卖局（公司）";
    user.personMb = @"8680181";
    user.sex = @"男";
    [MPWebService setUser:user];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    
    MPLeftMenuController * menu = [[MPLeftMenuController alloc] initWithNibName:nil bundle:nil];
    menu.delegate = self;
    
    MPMyAppController * main = [[MPMyAppController alloc] initWithNibName:nil bundle:nil];
    main.delegate = self;
    
    CTNavigationController * nav = [[CTNavigationController alloc] initWithRootViewController:main];
    _slideMenu = [[SlideMenuController alloc] initWithLeftViewController:menu centerViewController:nav];
    
    __weak MPLoginController *wekSelf = self;
    [self presentViewController:_slideMenu animated:YES completion:^(void) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSString *appId = [userDefaults valueForKey:AUTH_APPID];
        if (appId || appId.length > 0) {
            MPAuthViewController *controller = [[MPAuthViewController alloc] initWithNibName:@"MPAuthViewController" bundle:nil];
            controller.appId = appId;
            [wekSelf presentViewController:controller animated:YES completion:nil];
            [userDefaults setValue:nil forKey:AUTH_APPID];
            [userDefaults synchronize];
        }
    }];
    
    [[MPStoreManager sharedInstance] setStoreId:user.personCode];
    [[MPStoreManager sharedInstance] createAsiDownLoadTable];
}

- (void)initUI
{
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"LoginBg"]];
    
    self.inputBg.image = [[UIImage imageNamed:@"TIIInputBox"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 9, 14, 9)];
    [self.btnLogin setBackgroundImage:[[UIImage imageNamed:@"TIIBtnRed"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 9, 15, 9)] forState:UIControlStateNormal];
    [self.btnLogin setBackgroundImage:[[UIImage imageNamed:@"TIIBtnRedPressed"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 9, 15, 9)] forState:UIControlStateHighlighted];
    
    self.version.text = [NSString stringWithFormat:@"v%@", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    
    self.cbRememberPwd = [[CTCheckBox alloc] initWithFrame:CGRectMake(0, 0, 100, self.cbContainer.frame.size.height)];
    self.cbRememberPwd.text = @"记住密码";
    [self.cbRememberPwd setTextColor:[UIColor colorWithRed:142.0/255.0 green:84.0/255.0 blue:158.0/255.0 alpha:1]];
    self.cbRememberPwd.delegate = self;
    [self.cbRememberPwd setTextHidden:NO];
    [self.cbContainer addSubview:self.cbRememberPwd];
    
    self.cbAutoLogin = [[CTCheckBox alloc] initWithFrame:CGRectMake(self.cbContainer.frame.size.width / 2, 0, 100, self.cbContainer.frame.size.height)];
    self.cbAutoLogin.text = @"自动登录";
    [self.cbAutoLogin setTextColor:[UIColor colorWithRed:142.0/255.0 green:84.0/255.0 blue:158.0/255.0 alpha:1]];
    self.cbAutoLogin.delegate = self;
    [self.cbAutoLogin setTextHidden:NO];
    [self.cbContainer addSubview:self.cbAutoLogin];
    

}
#pragma mark -
#pragma mark CTCheckBox Delegate
- (void)ctCheckBox:(CTCheckBox *)checkBox didValueChanged:(BOOL)checked
{
    if (checkBox == self.cbRememberPwd) {
        if (!checked) {
            [self.cbAutoLogin setChecked:NO];
        }
    } else {
        [self.cbRememberPwd setChecked:checked];
    }
}

#pragma mark -
#pragma mark UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.tfUserName) {
        [self.tfPassword becomeFirstResponder];
    } else {
        [self.tfPassword resignFirstResponder];
    }
    return YES;
}

#pragma mark -
#pragma mark LeftMenuController Delegate
- (void)leftMenuController:(MPLeftMenuController *)controller didItemClicked:(LeftMenuFunctionName)leftMenuFunctionName
{
    [_slideMenu setLeftViewOpened:NO];
    
    switch (leftMenuFunctionName) {
        case LeftMenuFunctionUserInfo:
            break;
            
        case LeftMenuFunctionAppCenter:
        {
            MPAppCenterViewController *appCenter = [[MPAppCenterViewController alloc] initWithNibName:@"MPAppCenterViewController" bundle:nil];
            [(CTNavigationController *)self.slideMenu.centerViewController pushViewController:appCenter animated:YES];
            break;
        }
        case LeftMenuFunctionHistory:
        {
            UIWindow *screenWindow = [[UIApplication sharedApplication] keyWindow];
            self.trackController = [[MPTrackViewController alloc] initWithNibName:@"MPTrackViewController" bundle:nil];
            self.trackController.view.frame = [UIScreen mainScreen].bounds;
            [screenWindow addSubview:self.trackController.view];
            self.trackController.delegate = self;
            break;
        }
        case LeftMenuFunctionSetting:
        {
            MPSettingViewController *setting = [[MPSettingViewController alloc] init];
            [(CTNavigationController *)self.slideMenu.centerViewController pushViewController:setting animated:YES];
            break;
        }
        case LeftMenuFunctionAppComment:
        {
            MPCommentViewController *comm = [[MPCommentViewController alloc] init];
            [(CTNavigationController *)self.slideMenu.centerViewController pushViewController:comm animated:YES];
            break;
        }
        case LeftMenuFunctionAbout:
        {
            MPAboutViewController *about = [[MPAboutViewController alloc] init];
            [(CTNavigationController *)self.slideMenu.centerViewController pushViewController:about animated:YES];
            break;
        }
            
        default:
            break;
    }
}

#pragma mark -
#pragma mark MainController Delegate
- (void)mainControllerDidMenuButtonClick:(MPMyAppController *)controller
{
    if (_slideMenu.slideMenuState == SlideMenuStateClosed) {
        [_slideMenu setLeftViewOpened:YES];
    } else {
        [_slideMenu setLeftViewOpened:NO];
    }
}

- (void)mainControllerWillAppear:(MPMyAppController *)controller {
    [_slideMenu setTapGestureEnabled:NO];
    [_slideMenu setPanGestureEnabled:YES];
}

- (void)mainControllerWillDisappear:(MPMyAppController *)controller {
    [_slideMenu setTapGestureEnabled:NO];
    [_slideMenu setPanGestureEnabled:NO];
}

- (void)mainControllerNeedsPresentViewController:(UIViewController *)controller
{
    [self performSelector:@selector(show:) withObject:controller afterDelay:0.1];
}

- (void)show:(UIViewController *)controller
{
    [self.slideMenu presentViewController:controller animated:YES completion:nil];
}
#pragma mark -
#pragma mark MainController Delegate
- (void)mpTrackViewControllerBack:(UIViewController *)tagController
{
    [self.trackController.view removeFromSuperview];
    self.trackController = nil;
    
    if (tagController) {
        [(CTNavigationController *)self.slideMenu.centerViewController pushViewController:tagController animated:YES];
    }
    return;
}


@end
