//
//  LeftMenuController.m
//  MobilePortal
//
//  Created by Chenhc on 15/3/4.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "MPLeftMenuController.h"

#define kLeftViewOffsetX 80
#define kInsetX 10

@interface MPLeftMenuController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView * tableView;

@end

@implementation MPLeftMenuController

- (void)initUI {
    
    UIImageView * bg = [[UIImageView alloc] initWithFrame:self.view.bounds];
    bg.image = [UIImage imageNamed:@"menu_bg"];
    [self.view addSubview:bg];
    
    UIImageView * header = [[UIImageView alloc] initWithFrame:CGRectMake(kInsetX, 30, 46, 46)];
    header.image = [UIImage imageNamed:@"menu_icon_head"];
    [self.view addSubview:header];
    
    UILabel * nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(header.frame.origin.x + header.frame.size.width + 20, header.frame.origin.y, self.view.bounds.size.width - (header.frame.origin.x + header.frame.size.width + 20), header.frame.size.height / 2)];
    nameLabel.text = @"用户名";
    nameLabel.font = [UIFont systemFontOfSize:20.0];
    nameLabel.textColor = [UIColor whiteColor];
    [self.view addSubview:nameLabel];
    
    UILabel * subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(header.frame.origin.x + header.frame.size.width + 20, header.frame.origin.y + header.frame.size.height / 2, self.view.bounds.size.width - (header.frame.origin.x + header.frame.size.width + 20), header.frame.size.height / 2)];
    subTitleLabel.text = @"部门名称";
    subTitleLabel.font = [UIFont systemFontOfSize:15.0];
    subTitleLabel.textColor = [UIColor lightGrayColor];
    [self.view addSubview:subTitleLabel];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(kInsetX, header.frame.origin.x + header.frame.size.height + 30, self.view.bounds.size.width - 10 - kLeftViewOffsetX, self.view.bounds.size.height -  (header.frame.origin.x + header.frame.size.height + 30 + 40)) style:UITableViewStylePlain];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    // 此tableview点击状态栏不滚到顶部，否则其他界面有tableview的话，系统就无法判断哪个滚到顶部
    self.tableView.scrollsToTop = NO;
    [self.view addSubview:self.tableView];
    
    UILabel * versionLabel = [[UILabel alloc] initWithFrame:CGRectMake(kInsetX * 2, self.view.bounds.size.height - 40, self.tableView.frame.size.width - kInsetX, 40)];
    versionLabel.text = [NSString stringWithFormat:@"v%@", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    versionLabel.textColor = [UIColor colorWithRed:78.0/255.0 green:38.0/255.0 blue:82.0/255.0 alpha:1];
    versionLabel.font = [UIFont systemFontOfSize:16.0f];
    [self.view addSubview:versionLabel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark UITableView DataSource & delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 56;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.textLabel.font = [UIFont systemFontOfSize:16.0f];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    switch (indexPath.row) {
            
        case 0:
            cell.imageView.image = [UIImage imageNamed:@"menu_icon_appcenter"];
            cell.textLabel.text = @"应用中心";
            cell.tag = LeftMenuFunctionAppCenter;
            break;
            
        case 1:
            cell.imageView.image = [UIImage imageNamed:@"menu_icon_timeline"];
            cell.textLabel.text = @"历史记录";
            cell.tag = LeftMenuFunctionHistory;
            break;
            
        case 2:
            cell.imageView.image = [UIImage imageNamed:@"menu_icon_mine"];
            cell.textLabel.text = @"我的设置";
            cell.tag = LeftMenuFunctionSetting;
            break;
            
        case 3:
            cell.imageView.image = [UIImage imageNamed:@"menu_icon_comment"];
            cell.textLabel.text = @"应用评论";
            cell.tag = LeftMenuFunctionAppComment;
            break;
            
        case 4:
            cell.imageView.image = [UIImage imageNamed:@"menu_icon_about"];
            cell.textLabel.text = @"关于";
            cell.tag = LeftMenuFunctionAbout;
            break;
            
        default:
            break;
    }
    
    UIView * seperator = [[UIView alloc] initWithFrame:CGRectMake(0, 55, tableView.frame.size.width, 1)];
    seperator.backgroundColor = [UIColor colorWithRed:62.0/255.0 green:14.0/255.0 blue:66.0/255.0 alpha:1];
    [cell addSubview:seperator];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([self.delegate respondsToSelector:@selector(leftMenuController:didItemClicked:)]) {
        [self.delegate leftMenuController:self didItemClicked:cell.tag];
    }
}
@end
