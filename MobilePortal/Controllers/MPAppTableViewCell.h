//
//  ZbAppTableViewCell.h
//  TestProject
//
//  Created by 张斌 on 15/3/4.
//  Copyright (c) 2015年 007. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^BtnTouchBlock)(void);

typedef enum _AppCellBtnState {
    AppCellBtnStateGet = 0,
    AppCellBtnStateOpen = 1,
    AppCellBtnStateUpdate = 2,
    AppCellBtnStateComment = 3,
} AppCellBtnState;

@class TaPhoPersonalRes;

@interface MPAppTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIProgressView *percentView;
@property (nonatomic, strong) IBOutlet UIImageView *ivAppIcon;
@property (nonatomic, strong) IBOutlet UIView *updateDot;
@property (nonatomic, strong) IBOutlet UILabel *labAppName;
@property (nonatomic, strong) IBOutlet UILabel *labAppVersion;
@property (nonatomic, strong) IBOutlet UILabel *labAppUpdateDate;
@property (nonatomic, strong) IBOutlet UIButton *btnOper;
@property (nonatomic, strong) IBOutlet UIView *bottomLine;
@property (nonatomic, copy) BtnTouchBlock btnTouchBlock;

- (IBAction)btnOperOnClick:(id)sender;
- (void)setBottomLineShow:(BOOL)isShow;
- (void)setRedDotShow:(BOOL)isShow;
- (void)setBtnState:(AppCellBtnState)state;
- (void)setIsShowDownLoad:(BOOL)isShow;
- (void)setCurAppInfo:(TaPhoPersonalRes *)appInfo;

@end
