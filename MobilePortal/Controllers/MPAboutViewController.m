//
//  MPAboutViewController.m
//  MobilePortal
//
//  Created by hsit on 15-3-4.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "MPAboutViewController.h"
#import "MPAscViewController.h"

@interface MPAboutViewController ()

@property (nonatomic, strong) IBOutlet UILabel *versLab;

- (IBAction)applicationIntroduce:(UIControl *)control;
- (IBAction)updateRecoder:(UIControl *)control;

@end

@implementation MPAboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"关于";
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initUI {
    self.versLab.text = [NSString stringWithFormat:@"iPhone V%@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (IBAction)applicationIntroduce:(UIButton *)control {
    MPAscViewController *asc = [[MPAscViewController alloc] init];
    [asc setType:StatusTypeWeb];
    [self.navigationController pushViewController:asc animated:YES];
}

- (IBAction)updateRecoder:(UIButton *)control {
    MPAscViewController *asc = [[MPAscViewController alloc] init];
    [asc setType:StatusTypeTable];
    [self.navigationController pushViewController:asc animated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
