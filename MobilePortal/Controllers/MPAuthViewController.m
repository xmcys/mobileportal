//
//  MPAuthViewController.m
//  MobilePortal
//
//  Created by 张斌 on 15/4/1.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "MPAuthViewController.h"
#import "CTImageView.h"

@interface MPAuthViewController ()

@property (nonatomic, strong) IBOutlet UILabel *labAppName;
@property (nonatomic, strong) IBOutlet CTImageView *ivAppIcon;
@property (nonatomic, strong) NSString *tagUrlScheme;
@property (nonatomic, strong) NSString *appName;
@property (nonatomic, strong) NSString *appIconUrl;
@property (nonatomic, strong) NSString *token;

- (IBAction)btnCancelOnClick:(id)sender;
- (IBAction)btnOKOnClick:(id)sender;

@end

@implementation MPAuthViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.ivAppIcon.contentMode = UIViewContentModeScaleAspectFill;
    self.appName = @"榕烟微商圈";
    self.appIconUrl = @"http://sztv.cutv.com/uploads/allimg/150305/378_150305154906_1.jpg";
    self.token = @"7758520";
    self.tagUrlScheme = @"";
    
    self.ivAppIcon.url = self.appIconUrl;
    [self.ivAppIcon loadImage];
    self.labAppName.text = self.appName;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)btnCancelOnClick:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"mp7758521://?cancel"]]];
    return;
}

- (IBAction)btnOKOnClick:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"mp7758521://?%@", self.token]]];
    return;
}


@end
