//
//  MPAscViewController.m
//  MobilePortal
//
//  Created by hsit on 15-3-5.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "MPAscViewController.h"
#import "CTTableView.h"

@interface MPAscViewController ()<UITableViewDataSource, UITableViewDelegate, CTTableViewDelegate, UITextFieldDelegate>

@property (nonatomic) StatusTypeName statusType;
@property (nonatomic, strong) IBOutlet UIView *pawdChView;
@property (nonatomic, strong) IBOutlet UITextField *curPawd;
@property (nonatomic, strong) IBOutlet UITextField *nPawd;
@property (nonatomic, strong) IBOutlet UITextField *ncPwad;
@property (nonatomic, strong) IBOutlet UILabel *introduceLab;
@property (nonatomic, strong) CTTableView *tv;
@property (nonatomic, strong) NSMutableArray *array;


@end

@implementation MPAscViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_ok"] style:UIBarButtonItemStylePlain target:self action:@selector(sureClick)];
    self.navigationItem.rightBarButtonItem = item;
    self.array = [[NSMutableArray alloc] init];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)sureClick {
    NSLog(@"更改");
}

- (void)initUI {
    switch (self.statusType) {
        case StatusTypeWeb:
            self.title = @"应用介绍";
            self.pawdChView.hidden = YES;
            self.introduceLab.hidden = NO;
            self.navigationItem.rightBarButtonItem = nil;
            [self showDetail];
            break;
            
        case StatusTypeTable:
            self.title = @"更新记录";
            self.pawdChView.hidden = YES;
            self.introduceLab.hidden = YES;
            self.navigationItem.rightBarButtonItem = nil;
            [self showRecoder];
            break;
            
        case StatusTypePwd:
            self.title = @"密码修改";
            self.pawdChView.hidden = NO;
            self.introduceLab.hidden = YES;
            [self changePwd];
            break;
            
        default:
            break;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}

- (void)setType:(StatusTypeName)statusType {
    self.statusType = statusType;
}

- (void)showDetail {
    self.introduceLab.text = @"坚持依宪施政，依法行政，把政府工作全面纳入法治轨道。宪法是我们根本的活动准则，各级政府及其工作人员都必须严格遵守。要尊法学法守法用法，依法全面履行职责，所有行政行为都要于法有据，任何政府部门都不得法外设权。深化行政执法体制改革，严格规范公正文明执法，加快推进综合执法，全面落实行政执法责任制。一切违法违规的行为都要追究，一切执法不严不公的现象都必须纠正。[10:33]";
}

- (void)showRecoder {
    if (self.tv == nil) {
        self.tv = [[CTTableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.ctTableViewDelegate = self;
        self.tv.pullDownViewEnabled = NO;
        self.tv.pullUpViewEnabled = NO;
        [self.view addSubview:self.tv];
    }
    [self loadData];
}

- (void)changePwd {
}

- (void)loadData {
    
}

#pragma mark UITextField - Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.curPawd) {
        [self.nPawd becomeFirstResponder];
    } else if (textField == self.nPawd) {
        [self.ncPwad becomeFirstResponder];
    } else {
        [self.ncPwad resignFirstResponder];
    }
    return  YES;
}
#pragma  mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}
#pragma mark - CTTableViewDelegate
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView {
    return NO;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return self.array.count;
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 75.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        UIView *view = [[[NSBundle mainBundle] loadNibNamed:@"MPRecoderCell" owner:nil options:nil] lastObject];
        view.frame = CGRectMake(0, 0, self.view.frame.size.width, 75.0);
        view.backgroundColor = [UIColor clearColor];
        [cell addSubview:view];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
