//
//  ZbAppTableViewCell.m
//  TestProject
//
//  Created by 张斌 on 15/3/4.
//  Copyright (c) 2015年 007. All rights reserved.
//

#import "MPAppTableViewCell.h"
#import "TaPhoPersonalRes.h"
#import "MPDownloadItem.h"
#import "MPDownloadManager.h"
#import "MPStoreManager.h"

@interface MPAppTableViewCell ()

@property (nonatomic, assign) BOOL isShowDownLoad;
@property (nonatomic, weak) TaPhoPersonalRes *curAppInfo;
@property (nonatomic, assign) AppCellBtnState curBtnState;

@end

@implementation MPAppTableViewCell

- (void)awakeFromNib
{
    self.updateDot.layer.cornerRadius = 4;
    self.updateDot.layer.masksToBounds = YES;
    self.isShowDownLoad = NO;
    self.percentView.hidden = YES;
    return;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    return;
}

- (IBAction)btnOperOnClick:(id)sender
{
    if (self.curBtnState == AppCellBtnStateGet) {
        self.btnOper.hidden = YES;
        [[MPDownloadManager sharedInstance] startDownload:self.curAppInfo.resUrl withLocalPath:[[MPStoreManager sharedInstance] asiFileSavePath:self.curAppInfo.resId withResUrl:self.curAppInfo.resUrl]];
    } else if (self.curBtnState == AppCellBtnStateOpen) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CLICKAPP object:self.curAppInfo];
    } else if (self.curBtnState == AppCellBtnStateUpdate) {
    }
    if (self.btnTouchBlock) {
        self.btnTouchBlock();
    }
    return;
}

-(void)downloadNotification:(NSNotification *)notif
{
    MPDownloadItem *notifItem = notif.object;
    if (![[notifItem.url description] isEqualToString:self.curAppInfo.resUrl]) {
        return;
    }
    [self updateCell:notifItem];
    
    if (notifItem.downloadState == MPDownloadFinished) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_GETAPP object:self.curAppInfo];
    }
}

- (void)setIsShowDownLoad:(BOOL)isShow
{
    _isShowDownLoad = isShow;
    if (isShow) {
        self.btnOper.hidden = NO;
        self.percentView.hidden = NO;
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(downloadNotification:) name:kDownloadManagerNotification object:nil];
    } else {
        self.btnOper.hidden = YES;
        self.percentView.hidden = YES;
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
}

- (void)updateCell:(MPDownloadItem *)downloadItem
{
    self.btnOper.hidden = downloadItem.downloadState == MPDownloading;
    // 按钮状态
    double downloadPercent = 0.0;
    if (downloadItem) {
        if (downloadItem.downloadState == MPDownloadFinished) {
            // 已下载完
            [self setBtnState:AppCellBtnStateOpen];
        } else {
            // 下到一半
            [self setBtnState:AppCellBtnStateGet];
            downloadPercent = downloadItem.downloadPercent;
        }
    } else {
        // 没下载过
        [self setBtnState:AppCellBtnStateGet];
    }
    
    // 下载比例
    self.percentView.progress = downloadPercent;
    self.percentView.hidden = downloadPercent <= 0;
    return;
}

- (void)setCurAppInfo:(TaPhoPersonalRes *)appInfo
{
    _curAppInfo = appInfo;
    self.labAppName.text = appInfo.resName;
    self.labAppVersion.text = [NSString stringWithFormat:@"版本：%@", appInfo.appVer];
    self.labAppUpdateDate.text = [NSString stringWithFormat:@"更新日期：%@", appInfo.lastModifyTime];
    MPDownloadItem *task = [[MPDownloadManager sharedInstance] getDownloadItemByUrl:appInfo.resUrl];
    [self updateCell:task];
}

- (void)setBottomLineShow:(BOOL)isShow
{
    self.bottomLine.hidden = !isShow;
    return;
}

- (void)setRedDotShow:(BOOL)isShow
{
    self.updateDot.hidden = !isShow;
    return;
}

- (void)setBtnState:(AppCellBtnState)state
{
    self.curBtnState = state;
    switch (state) {
        case AppCellBtnStateGet:
            [self.btnOper setImage:QuickImage(@"btn_add_1") forState:UIControlStateNormal];
            [self.btnOper setImage:QuickImage(@"btn_add_2") forState:UIControlStateHighlighted];
            break;
        case AppCellBtnStateOpen:
            [self.btnOper setImage:QuickImage(@"btn_open_1") forState:UIControlStateNormal];
            [self.btnOper setImage:QuickImage(@"btn_open_2") forState:UIControlStateHighlighted];
            break;
        case AppCellBtnStateUpdate:
            [self.btnOper setImage:QuickImage(@"btn_update_1") forState:UIControlStateNormal];
            [self.btnOper setImage:QuickImage(@"btn_update_2") forState:UIControlStateHighlighted];
            break;
        case AppCellBtnStateComment:
            [self.btnOper setImage:QuickImage(@"btn_Comment_1") forState:UIControlStateNormal];
            [self.btnOper setImage:QuickImage(@"btn_Comment_2") forState:UIControlStateHighlighted];
            break;
            
        default:
            break;
    }
    return;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    return;
}

@end
