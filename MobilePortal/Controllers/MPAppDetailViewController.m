//
//  MPAppDetailViewController.m
//  MobilePortal
//
//  Created by 张斌 on 15/3/5.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "MPAppDetailViewController.h"
#import "MPAppTableViewCell.h"
#import "MPAppVersionTableViewCell.h"
#import "MPAppPicTableViewCell.h"
#import "MPAppCommentTableViewCell.h"
#import "MPAppComment.h"

static NSString * const AppVersionCellIdentifier = @"AppVersionCell";
static NSString * const AppPicCellIdentifier = @"AppPicCell";
static NSString * const AppCommentCellIdentifier = @"AppCommentCell";

@interface MPAppDetailViewController () <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tv;
@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) MPAppTableViewCell *appInfoView;
@property (nonatomic, strong) UISegmentedControl *curSegControlInTop;
@property (nonatomic, strong) UISegmentedControl *curSegViewInTop;
@property (nonatomic, strong) UISegmentedControl *curSegControlInHeader;
@property (nonatomic, strong) UISegmentedControl *curSegViewInHeader;
@property (nonatomic, strong) NSMutableArray *picArray;
@property (nonatomic, strong) NSMutableArray *commentArray;

- (void)loadCommentData;

@end

@implementation MPAppDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"应用详情";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = [NSString stringWithFormat:@"appId=%@", self.appId];
    [self loadCommentData];
    self.picArray = [NSMutableArray arrayWithArray:@[@"http://img5.cache.netease.com/photo/0003/2014-12-12/AD9DN2KA00AJ0003.jpg", @"http://img5.cache.netease.com/photo/0003/2014-12-12/AD9DN1JD00AJ0003.jpg", @"http://img3.cache.netease.com/photo/0003/2014-12-12/AD9DN5E500AJ0003.jpg", @"http://img6.cache.netease.com/photo/0003/2014-12-12/AD9DN98700AJ0003.jpg"]];
    
    [self.tv registerNib:[UINib nibWithNibName:@"MPAppVersionTableViewCell" bundle:nil] forCellReuseIdentifier:AppVersionCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"MPAppPicTableViewCell" bundle:nil] forCellReuseIdentifier:AppPicCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"MPAppCommentTableViewCell" bundle:nil] forCellReuseIdentifier:AppCommentCellIdentifier];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // 顶部app信息
    CGFloat headerHeight = 0;
    self.appInfoView = [[[NSBundle mainBundle] loadNibNamed:@"MPAppTableViewCell" owner:self options:nil] lastObject];
    self.appInfoView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.appInfoView.frame.size.height);
    [self.appInfoView setBottomLineShow:NO];
    headerHeight += self.appInfoView.frame.size.height;
    
    self.curSegViewInHeader = [[[NSBundle mainBundle] loadNibNamed:@"MPAppDesSegView" owner:self options:nil] lastObject];
    self.curSegViewInHeader.frame = CGRectMake(0, self.appInfoView.frame.size.height, self.view.frame.size.width, self.curSegViewInHeader.frame.size.height);
    self.curSegControlInHeader = (UISegmentedControl *)[self.curSegViewInHeader viewWithTag:1];
    [self.curSegControlInHeader addTarget:self action:@selector(segChangeInHeader:) forControlEvents:UIControlEventValueChanged];
    self.curSegControlInHeader.selectedSegmentIndex = 0;
    headerHeight += self.curSegViewInHeader.frame.size.height;
    
    self.headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, headerHeight)];
    self.headerView.backgroundColor = [UIColor whiteColor];
    [self.headerView addSubview:self.appInfoView];
    [self.headerView addSubview:self.curSegViewInHeader];
    [self.tv setTableHeaderView:self.headerView];
    
    // 当tableview往上移时，显示的选项卡
    self.curSegViewInTop = [[[NSBundle mainBundle] loadNibNamed:@"MPAppDesSegView" owner:self options:nil] lastObject];
    self.curSegViewInTop.frame = CGRectMake(0, 0, self.view.frame.size.width, self.curSegViewInTop.frame.size.height);
    self.curSegControlInTop = (UISegmentedControl *)[self.curSegViewInTop viewWithTag:1];
    [self.curSegControlInTop addTarget:self action:@selector(segChangeInTop:) forControlEvents:UIControlEventValueChanged];
    self.curSegControlInTop.selectedSegmentIndex = 0;
    self.curSegViewInTop.hidden = YES;
    [self.view addSubview:self.curSegViewInTop];
    return;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)initUI
{
    return;
}

# pragma mark - url requests
- (void)loadCommentData
{
    MPAppComment *comment1 = [[MPAppComment alloc] init];
    comment1.personName = @"测试员";
    comment1.content = @"人民日报全媒体平台3月5日电  （记者杜尚泽）习近平参加上海代表团审议时，杨雄第一个发言";
    comment1.operDate = @"2015年1月1日";
    MPAppComment *comment2 = [[MPAppComment alloc] init];
    comment2.personName = @"测试员";
    comment2.content = @"人民日报全媒体平台3月5日电  （记者杜尚泽）习近平参加上海代表团审议时，杨雄第一个发言，主题是加快建设具有全球影响力的科技创新中心。讲到环境保护时，习近平格外关注。“空气质量优良的能占多少？”“70%。”这时有人插话说：“有时候是靠天吃饭”。笑声中，习近平接话说：“不能只靠借东风啊！事在人为。”";
    comment2.operDate = @"2015年1月1日";
    MPAppComment *comment3 = [[MPAppComment alloc] init];
    comment3.personName = @"测试员";
    comment3.content = @"人民日报全媒体平台3月5日电  （记者杜尚泽）习近平参加上海代表团审议时，杨雄第一个发言，主题是加快建设具有全球影响力的科技创新中心。";
    comment3.operDate = @"2015年1月1日";
    MPAppComment *comment4 = [[MPAppComment alloc] init];
    comment4.personName = @"测试员";
    comment4.content = @"人民日报全媒体平台3月5日电  （记者杜尚泽）习近平参加上海代表团审议时，杨雄第一个发言，主题是加快建设具有全球影响力的科技创新中心。讲到环境保护时，习近平格外关注。“空气质量优良的能占多少？”“70%。”这时有人插话说：“有时候是靠天吃饭”。笑声中，习近平接话说：“不能只靠借东风啊！事在人为。";
    comment4.operDate = @"2015年1月1日";
    MPAppComment *comment5 = [[MPAppComment alloc] init];
    comment5.personName = @"测试员";
    comment5.content = @"测试内容5";
    comment5.operDate = @"2015年1月1日";
    self.commentArray = [NSMutableArray arrayWithArray:@[comment1,comment2, comment3, comment4, comment5]];
    return;
}

# pragma mark - button click envents
- (void)segChangeInHeader:(UISegmentedControl *)seg
{
    NSInteger segIndex = seg.selectedSegmentIndex;
    self.curSegControlInTop.selectedSegmentIndex = segIndex;
    switch (segIndex) {
        case 0:
            break;
            
        default:
            break;
    }
    [self.tv reloadData];
    return;
}

- (void)segChangeInTop:(UISegmentedControl *)seg
{
    NSInteger segIndex = seg.selectedSegmentIndex;
    self.curSegControlInHeader.selectedSegmentIndex = segIndex;
    switch (segIndex) {
        case 0:
            break;
            
        default:
            break;
    }
    [self.tv reloadData];
    return;
}

# pragma mark - UIScrollView delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y >= self.curSegViewInHeader.frame.origin.y) {
        self.curSegViewInHeader.hidden = YES;
        self.curSegViewInTop.hidden = NO;
    } else {
        self.curSegViewInHeader.hidden = NO;
        self.curSegViewInTop.hidden = YES;
    }
    return;
}

# pragma mark - UITableView delegate && datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger segIndex = self.curSegControlInHeader.selectedSegmentIndex;
    if (segIndex == 0) {
        // 版本
        return 1;
    } else if (segIndex == 1) {
        // 图片
        return 1;
    } else {
        // 评论
        return self.commentArray.count;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSInteger segIndex = self.curSegControlInHeader.selectedSegmentIndex;
    if (segIndex == 0) {
        // 版本
        return 50;
    } else if (segIndex == 1) {
        // 图片
        NSInteger picCellHeight = CGRectGetHeight(self.view.bounds) - CGRectGetHeight(self.headerView.frame);
        return picCellHeight;
    } else {
        // 评论
        static MPAppCommentTableViewCell *sizingCell = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            sizingCell = [tableView dequeueReusableCellWithIdentifier:AppCommentCellIdentifier];
            sizingCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.tv.frame), CGRectGetHeight(sizingCell.bounds));
        });
        MPAppComment *comment = [self.commentArray objectAtIndex:indexPath.row];
        return [sizingCell calcCellHeight:comment];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger segIndex = self.curSegControlInHeader.selectedSegmentIndex;
    if (segIndex == 0) {
        // 版本
        MPAppVersionTableViewCell *cell = (MPAppVersionTableViewCell *)[self.tv dequeueReusableCellWithIdentifier:AppVersionCellIdentifier forIndexPath:indexPath];
        return cell;
    } else if (segIndex == 1) {
        // 图片
        NSInteger picCellHeight = CGRectGetHeight(self.view.bounds) - CGRectGetHeight(self.headerView.frame);
        MPAppPicTableViewCell *cell = (MPAppPicTableViewCell *)[self.tv dequeueReusableCellWithIdentifier:AppPicCellIdentifier forIndexPath:indexPath];
        [cell setPicArray:self.picArray withCellHeight:picCellHeight];
        return cell;
    } else {
        // 评论
        MPAppCommentTableViewCell *cell = (MPAppCommentTableViewCell *)[self.tv dequeueReusableCellWithIdentifier:AppCommentCellIdentifier forIndexPath:indexPath];
        
        MPAppComment *comment = [self.commentArray objectAtIndex:indexPath.row];
        [cell setComment:comment withIndex:indexPath.row];
        cell.btnTouchBlock = ^(void) {
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        };
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
