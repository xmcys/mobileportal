//
//  MPAppPicTableViewCell.h
//  MobilePortal
//
//  Created by 张斌 on 15/3/7.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MPAppPicTableViewCell : UITableViewCell

- (void)setPicArray:(NSArray *)picArray withCellHeight:(NSInteger)height;

@end
