//
//  MPCommDetailViewController.m
//  MobilePortal
//
//  Created by hsit on 15-3-9.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "MPCommDetailViewController.h"
#import "CTTableView.h"
#import "MPAppCommentTableViewCell.h"
#import "MPAppComment.h"


@interface MPCommDetailViewController ()<UITextViewDelegate, UITableViewDataSource, UITableViewDelegate, CTTableViewDelegate>

@property (nonatomic, strong) IBOutlet UISegmentedControl *segmentControl;
@property (nonatomic, strong) IBOutlet UIView *headView;
@property (nonatomic, strong) IBOutlet UIView *fistView;
@property (nonatomic, strong) IBOutlet UIView *secondView;
@property (nonatomic, strong) IBOutlet UILabel *defaultLab;
@property (nonatomic, strong) IBOutlet UITextView *textView;
@property (nonatomic, strong) UIBarButtonItem *menuBaritem;
@property (nonatomic, strong) CTTableView *tv;
@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, strong) NSString *identifier;

@end

@implementation MPCommDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
     self.menuBaritem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_ok"] style:UIBarButtonItemStylePlain target:self action:@selector(sureClick)];
    self.navigationItem.rightBarButtonItem = self.menuBaritem;
    self.array = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initUI {
    self.tv = [[CTTableView alloc] initWithFrame:self.secondView.bounds];
    self.tv.backgroundColor = [UIColor whiteColor];
    self.tv.delegate = self;
    self.tv.dataSource = self;
    self.tv.ctTableViewDelegate = self;
    self.tv.pullUpViewEnabled = NO;
    [self.secondView addSubview:self.tv];
    self.identifier = @"Cell";
    [self.tv registerNib:[UINib nibWithNibName:@"MPAppCommentTableViewCell" bundle:nil] forCellReuseIdentifier:self.identifier];
    [self.segmentControl addTarget:self action:@selector(changeClick:) forControlEvents:UIControlEventValueChanged];
    self.secondView.hidden = YES;
    [self loadData];
    return;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)sureClick {
    NSLog(@"确认提交");
}

- (void)loadData {
    MPAppComment *comment1 = [[MPAppComment alloc] init];
    comment1.personName = @"测试员";
    comment1.content = @"人民日报全媒体平台3月5日电  （记者杜尚泽）习近平参加上海代表团审议时，杨雄第一个发言";
    comment1.operDate = @"2015年1月1日";
    MPAppComment *comment2 = [[MPAppComment alloc] init];
    comment2.personName = @"测试员";
    comment2.content = @"人民日报全媒体平台3月5日电  （记者杜尚泽）习近平参加上海代表团审议时，杨雄第一个发言，主题是加快建设具有全球影响力的科技创新中心。讲到环境保护时，习近平格外关注。“空气质量优良的能占多少？”“70%。”这时有人插话说：“有时候是靠天吃饭”。笑声中，习近平接话说：“不能只靠借东风啊！事在人为。”";
    comment2.operDate = @"2015年1月1日";
    MPAppComment *comment3 = [[MPAppComment alloc] init];
    comment3.personName = @"测试员";
    comment3.content = @"人民日报全媒体平台3月5日电  （记者杜尚泽）习近平参加上海代表团审议时，杨雄第一个发言，主题是加快建设具有全球影响力的科技创新中心。";
    comment3.operDate = @"2015年1月1日";
    self.array = [NSMutableArray arrayWithArray:@[comment1,comment2, comment3]];
}

- (void)changeClick:(UISegmentedControl *)control {
    if (control.selectedSegmentIndex == 0) {
        self.fistView.hidden = NO;
        self.secondView.hidden = YES;
        self.navigationItem.rightBarButtonItem = self.menuBaritem;
        [self.view endEditing:YES];
    } else {
        self.fistView.hidden = YES;
        self.secondView.hidden = NO;
        self.navigationItem.rightBarButtonItem = nil;
    }
}
#pragma mark - TextView - Deleagte
- (void)textViewDidChange:(UITextView *)textView {
    if (textView.text.length > 0) {
        self.defaultLab.hidden = YES;
    } else {
        self.defaultLab.hidden = NO;
    }
}
#pragma  mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark - CTTableView Delegate 
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView {
    [self loadData];
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    static MPAppCommentTableViewCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [tableView dequeueReusableCellWithIdentifier:self.identifier];
        sizingCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.tv.frame), CGRectGetHeight(sizingCell.bounds));
    });
    MPAppComment *comment = [self.array objectAtIndex:indexPath.row];
    return [sizingCell calcCellHeight:comment];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MPAppCommentTableViewCell *cell = (MPAppCommentTableViewCell *)[self.tv dequeueReusableCellWithIdentifier:self.identifier forIndexPath:indexPath];
    MPAppComment *comment = [self.array objectAtIndex:indexPath.row];
    [cell setComment:comment withIndex:indexPath.row];
    cell.btnTouchBlock = ^(void) {
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    };
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
