//
//  MPAppSearchViewController.m
//  TestProject
//
//  Created by 张斌 on 15/3/4.
//  Copyright (c) 2015年 007. All rights reserved.
//

#import "MPAppSearchViewController.h"
#import "MPAppTableViewCell.h"
#import "CustomPlaceHolderTextField.h"
#import "TaPhoPersonalRes.h"
#import "MPAppDetailViewController.h"
#import "MPStoreManager.h"
#import "RMMapper.h"

static NSString * AppCellIdentifier = @"MPAppTableViewCell";

@interface MPAppSearchViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tv;
@property (nonatomic, strong) IBOutlet UIView *searchView;
@property (nonatomic, strong) IBOutlet CustomPlaceHolderTextField *tfSearch;
@property (nonatomic, strong) NSMutableArray *array;

- (void)btnSearchOnClick:(id)btn;
- (void)loadData;

@end

@implementation MPAppSearchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIButton * rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.backgroundColor = [UIColor clearColor];
    [rightBtn setImage:[UIImage imageNamed:@"icon_h40_search"] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(btnSearchOnClick:) forControlEvents:UIControlEventTouchUpInside];
    rightBtn.frame = CGRectMake(0, 0, 40, 44);
    rightBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    
    [self.tv registerNib:[UINib nibWithNibName:@"MPAppTableViewCell" bundle:nil] forCellReuseIdentifier:AppCellIdentifier];
    
    self.searchView.layer.cornerRadius = 5;
    self.tfSearch.tintColor = [UIColor whiteColor];
    [self.tfSearch setPlaceHolderColor:RGB(195, 179, 200, 1)];
    
    [self loadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    CGFloat searchViewWidth = ScreenWidth - self.navigationItem.backBarButtonItem.width - self.navigationItem.rightBarButtonItem.width;
    CGRect searchViewFrame = CGRectMake(0, 0, searchViewWidth, CGRectGetHeight(self.searchView.frame));
    self.searchView.frame = searchViewFrame;
    self.navigationItem.titleView = self.searchView;
}

- (void)loadData
{
    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"appCenterItems" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:jsonPath];
    NSArray *appArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    NSArray *appResArray = [RMMapper arrayOfClass:TaPhoPersonalRes.class fromArrayOfDictionary:appArray];
    self.array = [NSMutableArray arrayWithArray:appResArray];
    return;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)btnSearchOnClick:(id)btn
{
    [self.tfSearch resignFirstResponder];
    return;
}

# pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

# pragma mark - UITableView delegate && datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.array.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 71;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MPAppTableViewCell *cell = (MPAppTableViewCell *)[self.tv dequeueReusableCellWithIdentifier:AppCellIdentifier forIndexPath:indexPath];
    [cell setIsShowDownLoad:YES];
    TaPhoPersonalRes *appInfo = [self.array objectAtIndex:indexPath.row];
    [cell setCurAppInfo:appInfo];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    MPAppDetailViewController *appDetail = [[MPAppDetailViewController alloc] initWithNibName:@"MPAppDetailViewController" bundle:nil];
    TaPhoPersonalRes *app = [self.array objectAtIndex:indexPath.row];
    appDetail.appId = app.resId;
    [self.navigationController pushViewController:appDetail animated:YES];
    
    // 存储使用记录
    [[MPStoreManager sharedInstance] addTrack:app.resId];
}

@end
