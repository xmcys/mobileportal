//
//  MPCommentViewController.m
//  MobilePortal
//
//  Created by hsit on 15-3-5.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "MPCommentViewController.h"
#import "CTTableView.h"
#import "MPAppTableViewCell.h"
#import "MPCommDetailViewController.h"

@interface MPCommentViewController ()<UITableViewDataSource, UITableViewDelegate, CTTableViewDelegate>

@property (nonatomic, strong) IBOutlet UIView *headerView;
@property (nonatomic, strong) IBOutlet UIView *searchView;
@property (nonatomic, strong) CTTableView *tv;
@property (nonatomic, strong) NSString *identifier;

@end

@implementation MPCommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"应用评论";
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initUI {
    self.searchView.layer.cornerRadius = 5.0f;
    self.searchView.layer.masksToBounds = YES;
    self.tv = [[CTTableView alloc] initWithFrame:CGRectMake(0, self.headerView.frame.size.height, self.view.frame.size.width, self.view.frame.size.height - self.headerView.frame.size.height)];
    self.tv.dataSource = self;
    self.tv.delegate = self;
    self.tv.ctTableViewDelegate = self;
        self.tv.pullUpViewEnabled = NO;
    self.identifier = @"Cell";
    [self.tv registerNib:[UINib nibWithNibName:@"MPAppTableViewCell" bundle:nil] forCellReuseIdentifier:self.identifier];
    [self.view addSubview:self.tv];
}

- (void)loadData {
    
}

#pragma CTTableViewDelegate
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView {
    [self loadData];
    return YES;
}
#pragma mark UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 75;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MPAppTableViewCell *cell = (MPAppTableViewCell *)[self.tv dequeueReusableCellWithIdentifier:self.identifier forIndexPath:indexPath];
    [cell setBtnState:AppCellBtnStateComment];
    cell.btnTouchBlock = ^(void){
//        NSLog(@"%ld", indexPath.row);
    };
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    MPCommDetailViewController *controller = [[MPCommDetailViewController alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
