//
//  MPAppPicTableViewCell.m
//  MobilePortal
//
//  Created by 张斌 on 15/3/7.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#define PIC_HEIGHT_WIDTH_SCALE 1.8  // 图片高宽比
#define PIC_SPACE 10                // 图片间隔

#import "MPAppPicTableViewCell.h"
#import "CTImageView.h"

@interface MPAppPicTableViewCell ()

@property (nonatomic, strong) IBOutlet UIScrollView *sv;

@end

@implementation MPAppPicTableViewCell

- (void)awakeFromNib
{
    self.sv.scrollsToTop = NO;
    return;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    return;
}

- (void)setPicArray:(NSArray *)picArray withCellHeight:(NSInteger)height;
{
    CGFloat picHeight = height - PIC_SPACE * 2;
    CGFloat picWidth = picHeight / PIC_HEIGHT_WIDTH_SCALE;
    for (NSInteger i = 0; i < picArray.count; i++) {
        NSString *picUrl = [picArray objectAtIndex:i];
        CGRect picFrame = CGRectMake(PIC_SPACE * (i + 1) + picWidth * i, PIC_SPACE, picWidth, picHeight);
        CTImageView *iv = [[CTImageView alloc] initWithFrame:picFrame];
        iv.url = picUrl;
        [iv loadImage];
        iv.contentMode = UIViewContentModeScaleAspectFill;
        iv.clipsToBounds = YES;
        [self.sv addSubview:iv];
    }
    CGFloat svContentWidth = picWidth * picArray.count + PIC_SPACE * (picArray.count + 1);
    self.sv.contentSize = CGSizeMake(svContentWidth, CGRectGetHeight(self.sv.frame));
    return;
}

@end
