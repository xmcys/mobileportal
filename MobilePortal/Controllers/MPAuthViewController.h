//
//  MPAuthViewController.h
//  MobilePortal
//
//  Created by 张斌 on 15/4/1.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"

@interface MPAuthViewController : CTViewController

@property (nonatomic, copy) NSString *appId;

@end
