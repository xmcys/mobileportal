//
//  ButtonCell.m
//  NewiPhone
//
//  Created by xjkj on 14/12/15.
//  Copyright (c) 2014年 xjkj. All rights reserved.
//

#import "MPAppTypeCell.h"

@implementation MPAppTypeCell

- (void)awakeFromNib {
    // Initialization code
}

-(void)setState:(NSDictionary*)dic
       wtihType:(int)type
  withIndexPath:(NSIndexPath *)indexPath{
  
    self.titleLab.text = [dic objectForKey:@"title"];
    self.bgView.hidden = type != indexPath.row;
    if (type == indexPath.row) {
        self.titleLab.textColor = RGB(182, 21, 135, 1);
    }else{
        self.titleLab.textColor = RGB(56, 56, 56, 1);
    }
}

@end
