//
//  TIIWebViewController.m
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-29.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIIWebViewController.h"
#import "CTAlertView.h"
#import "TIIWeekPicker.h"
#import "TIIDatePicker.h"
#import <QuartzCore/QuartzCore.h>
#import "MPWebService.h"
#import "TaPhoPersonalRes.h"
#import "TIIWebView.h"

@interface TIIWebViewController () <TIIWebViewDelegate, CTAlertViewDelegate>

@property (nonatomic, strong) TIIWebView * webView;
@property (nonatomic, strong) UIButton * btnBack;

- (void)dismiss;

@end

@implementation TIIWebViewController


- (void)initUI
{
    
    NSLog(@"%.2f", self.view.bounds.size.height);
    self.view.backgroundColor = [UIColor colorWithRed:86.0/255.9 green:35.0/255.0 blue:94.0/255.0 alpha:0.5];
    
    int y = 0;
    if ([[UIDevice currentDevice].systemVersion intValue] >= 7) {
        y = 20;
        
    }
    
    self.webView = [[TIIWebView alloc] initWithFrame:CGRectMake(0, y, self.view.bounds.size.width, self.view.bounds.size.height - y)];
    self.webView.tiiWebViewDelegate = self;
//    self.webView.res = _res;
    [self.view addSubview:self.webView];
    
    self.btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btnBack setImage:[UIImage imageWithContentsOfFile:HBResourcesBundle(@"NavBack.png")] forState:UIControlStateNormal];
    [self.btnBack setImage:[UIImage imageWithContentsOfFile:HBResourcesBundle(@"NavBack.png")] forState:UIControlStateHighlighted];
    self.btnBack.frame = CGRectMake(10, self.view.frame.size.height - 40 - 10, 40, 40);
    self.btnBack.backgroundColor = [UIColor colorWithRed:86.0/255.9 green:35.0/255.0 blue:94.0/255.0 alpha:0.5];
    self.btnBack.layer.cornerRadius = 20.0f;
    [self.btnBack addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    self.btnBack.showsTouchWhenHighlighted = YES;
    [self.view addSubview:self.btnBack];
    
    NSString* path = [[NSBundle mainBundle] pathForResource:@"index" ofType:@"html"];
    NSURL * url = [NSURL URLWithString:[path stringByAppendingString:@"?wxId=oP9mfjhXc93_DR8Z1MBwado4zS7M&userType=1&t=1"]];
    NSURLRequest* request = [NSURLRequest requestWithURL:url] ;
    [self.webView loadRequest:request];
}

- (void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
    if (self.delegate && [self.delegate respondsToSelector:@selector(recvDiss:Bool:)]) {
        [self.delegate recvDiss:self Bool:YES];
    }
}


#pragma mark -
#pragma mark CTAlertView Delegate
- (void)ctAlertView:(CTAlertView *)alertView didClickedButtonOnIndex:(NSInteger)index
{
    self.webView = nil;
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark TIIWebView Delegate
- (void)tiiWebViewNeedsDestory:(TIIWebView *)tiiWebView
{
    self.webView = nil;
    [self dismissViewControllerAnimated:YES completion:nil];
    if (self.delegate && [self.delegate respondsToSelector:@selector(recvDiss:Bool:)]) {
        [self.delegate recvDiss:self Bool:YES];
    }
}

- (void)tiiWebViewDidFinishedLoading:(TIIWebView *)tiiWebView
{
//    self.btnBack.hidden = YES;
}

- (void)tiiWebView:(TIIWebView *)tiiWebView didFailLoadWithError:(NSError *)error
{
    NSLog(@"%@", error);
    CTAlertView * alert = [[CTAlertView alloc] initWithTitle:@"系统提示" message:@"页面加载失败，请稍后再试！" delegate:self cancelButtonTitle:nil confirmButtonTitle:@"退出"];
    [alert show];
}

@end
