//
//  SlideView.h
//  NewiPhone
//
//  Created by xjkj on 14/12/15.
//  Copyright (c) 2014年 xjkj. All rights reserved.
//

#import <UIKit/UIKit.h>

#define EachButtonWidth 75.0f

@interface MPAppTypeView : UIView

@property (strong, nonatomic) UICollectionView *collectionView;
@property(strong,nonatomic) NSMutableArray *dataArry;
@property(assign,nonatomic) int selectedRow;//选中的row
// 按钮点击
@property (copy,nonatomic) void (^touchIndexPath)(NSIndexPath *indexPath);

//数据刷新
-(void)setData;

@end
