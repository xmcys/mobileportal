//
//  TIIWebViewController.h
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-29.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CTViewController.h"

@class TIIWebViewController;

@protocol TIIWebViewControllerDelegate <NSObject>

- (void)recvDiss:(TIIWebViewController *)controller Bool:(BOOL)able;

@end

@class TaPhoPersonalRes;

@interface TIIWebViewController : CTViewController

@property (nonatomic, strong) TaPhoPersonalRes * res;

@property (nonatomic, strong) id<TIIWebViewControllerDelegate> delegate;

@end
