//
//  MPAscViewController.h
//  MobilePortal
//
//  Created by hsit on 15-3-5.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"

typedef NS_ENUM(NSInteger, StatusTypeName) {
    StatusTypeWeb = 1000,
    StatusTypeTable,
    StatusTypePwd
};

@interface MPAscViewController : CTViewController

- (void)setType:(StatusTypeName)statusType;

@end
