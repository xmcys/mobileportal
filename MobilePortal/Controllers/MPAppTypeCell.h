//
//  ButtonCell.h
//  NewiPhone
//
//  Created by xjkj on 14/12/15.
//  Copyright (c) 2014年 xjkj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MPAppTypeCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *iconIv;

-(void)setState:(NSDictionary*)dic
       wtihType:(int)type
  withIndexPath:(NSIndexPath *)indexPath;

@end
