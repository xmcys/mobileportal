//
//  MPSettingViewController.m
//  MobilePortal
//
//  Created by hsit on 15-3-4.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "MPSettingViewController.h"
#import "MPAscViewController.h"
#import "CTAlertView.h"

@interface MPSettingViewController ()<UIActionSheetDelegate, CTAlertViewDelegate>

- (IBAction)updateCheck:(UIControl *)control;
- (IBAction)pwdChange:(UIControl *)control;
- (IBAction)logout:(UIButton *)btn;


@end

@implementation MPSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的设置";
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initUI {
    return;
}

- (IBAction)updateCheck:(UIControl *)control {
    CTAlertView *alert = [[CTAlertView alloc] initWithTitle:@"检测到有更新" message:@"发现版本2.0.0" delegate:self cancelButtonTitle:@"稍后更新" confirmButtonTitle:@"马上更新"];
    [alert show];
}
#pragma mark CTAlertView - Delegate
- (void)ctAlertView:(CTAlertView *)alertView didClickedButtonOnIndex:(NSInteger)index {
    if (index == 1) {
        NSLog(@"下载中..,请等待");
    }
}

- (IBAction)pwdChange:(UIControl *)control {
    MPAscViewController *asc = [[MPAscViewController alloc] init];
    [asc setType:StatusTypePwd];
    [self.navigationController pushViewController:asc animated:YES];
}

- (IBAction)logout:(UIButton *)btn {
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    [self presentViewController:[storyboard instantiateInitialViewController] animated:YES completion:nil];
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"确定退出当前账号?" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"确定" otherButtonTitles: nil];
    [action showInView:self.view];
}
#pragma mark
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_LOGOUT object:nil];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
