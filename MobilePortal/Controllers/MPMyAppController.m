//
//  MainController.m
//  MobilePortal
//
//  Created by Chenhc on 15/3/4.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "MPMyAppController.h"
#import "LauncherScrollView.h"
#import "TaPhoPersonalResTool.h"
#import "NSUserDefaults+RMSaveCustomObject.h"
#import "RMMapper.h"
#import "TIIWebView.h"
#import "TIIWebViewController.h"
#import "MPStoreManager.h"
#import "TaPhoPersonalResTool.h"

@interface MPMyAppController () <LauncherItemDelegate, LauncherScrollDelegate, UIAlertViewDelegate, UITabBarDelegate>

@property (nonatomic, strong) LauncherScrollView * launcherScroll;
@property (nonatomic, strong) LauncherItem * targetItem;
@property (nonatomic, strong) UIBarButtonItem * menuBarButton;
@property (nonatomic, strong) UIBarButtonItem * msgBarButton;
@property (nonatomic, strong) UITabBar * tabBar;
@property (nonatomic, strong) TIIWebView * tiiWebView;

@property (nonatomic, strong) UIView * myAppView;

- (void)initTabBar;
- (void)initMyAppView;
- (void)endLauncherScrollEditState;
- (void)openOrCloseLeftView;
- (void)showMsgList;

@end

@implementation MPMyAppController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clickAppNotify:) name:NOTIFICATION_CLICKAPP object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getAppSuccessNotify:) name:NOTIFICATION_GETAPP object:nil];
    return;
}

- (void)initUI {
    
    self.title = @"我的应用";
    
    self.menuBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu_icon_btn"] style:UIBarButtonItemStyleBordered target:self action:@selector(openOrCloseLeftView)];
    self.navigationItem.leftBarButtonItem = self.menuBarButton;
    
    self.msgBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_msg"] style:UIBarButtonItemStylePlain target:self action:@selector(showMsgList)];
    self.navigationItem.rightBarButtonItem = self.msgBarButton;
    
    
    UIImageView * bg = [[UIImageView alloc] initWithFrame:self.view.bounds];
    bg.image = [UIImage imageNamed:@"folder_bg"];
    bg.contentMode = UIViewContentModeScaleToFill;
    [self.view insertSubview:bg atIndex:0];
    
    [self initTabBar];
    
    float navHeight = [UIDevice currentDevice].systemVersion.floatValue >= 7.0 ? 0 : 20;
    _launcherScroll = [[LauncherScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - self.tabBar.frame.size.height - navHeight) withColumn:4];
    _launcherScroll.backgroundColor = [UIColor clearColor];
    _launcherScroll.launcherScrollDelegate = self;
    
    [self.view addSubview:_launcherScroll];
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray * resArray = [defaults rm_customObjectForKey:@"MY_APP_CONFIG"];
    if (resArray == nil || resArray.count == 0) {
        [self initLauncherItemDemo];
    } else {
        [TaPhoPersonalResTool resArrayToLauncherPages:resArray complete:^(NSMutableArray * pages){
            for (int i = 0; i < pages.count; i++) {
                NSMutableArray * page = [pages objectAtIndex:i];
                for (int j = 0; j < page.count; j++) {
                    LauncherItem * item = [page objectAtIndex:j];
                    item.delegate = self;
                    if (item.isFolder) {
                        for (NSMutableArray * folderPage in item.folderItems) {
                            for (LauncherItem * folderItem in folderPage) {
                                folderItem.delegate = self;
                            }
                        }
                    }
                }
            }
            [_launcherScroll layoutLauncherItems:pages];
        }];
    }
}

- (void)initLauncherItemDemo
{
    NSMutableArray * pages = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < 2; i++) {
        NSMutableArray * page = [[NSMutableArray alloc] init];
        int pageCount = arc4random() % 5 + 5;
        
        for (int j = 0; j < pageCount; j++) {
            LauncherItem * item = [[LauncherItem alloc] initWithFrame:CGRectMake(0, 0, LAUNCHER_ITEM_WIDTH, LAUNCHER_ITEM_HEIGHT) isFolder:!(fmod(j, 6))];
            if (item.isFolder) {
                item.title = [NSString stringWithFormat:@"文件夹%d%d", i, j];
            } else {
                item.title = [NSString stringWithFormat:@"标题%d%d", i, j];
                item.imageUrl = @"http://ww1.sinaimg.cn/thumbnail/50f1ca9agw1eo89svb6e9j208c08cwfb.jpg";
                item.newFlag = j % 2;
            }
            item.delegate = self;
            item.index = j;
            item.pageNum = i;
            item.itemId = [NSString stringWithFormat:@"%d%d", i, j];
            [page addObject:item];
            if (item.isFolder) {
                for (int k = 0; k < 2; k++) {
                    NSMutableArray * folderPage = [[NSMutableArray alloc] init];
                    int pageCount = arc4random() % 9;
                    
                    for (int l = 0; l < pageCount; l++) {
                        LauncherItem * item = [[LauncherItem alloc] initWithFrame:CGRectMake(0, 0, LAUNCHER_ITEM_WIDTH, LAUNCHER_ITEM_HEIGHT) isFolder:NO];
                        item.title = [NSString stringWithFormat:@"标题%d%d%d%d", i, j, k, l];
                        item.delegate = self;
                        item.index = l;
                        item.pageNum = k;
                        item.folderId = [NSString stringWithFormat:@"%d%d", i, j];
                        item.itemId = [NSString stringWithFormat:@"%d%d%d%d", i, j, k, l];
                        [folderPage addObject:item];
                    }
                    [item.folderItems addObject:folderPage];
                }
            }
        }
        [pages addObject:page];
    }
    
    [_launcherScroll layoutLauncherItems:pages];
    
    [TaPhoPersonalResTool launcherPagesToResArray:_launcherScroll.pages complete:^(NSMutableArray * resArray){
        NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
        [defaults rm_setCustomObject:resArray forKey:@"MY_APP_CONFIG"];
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([self.delegate respondsToSelector:@selector(mainControllerWillAppear:)]) {
        [self.delegate mainControllerWillAppear:self];
    }
    return;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if ([self.delegate respondsToSelector:@selector(mainControllerWillDisappear:)]) {
        [self.delegate mainControllerWillDisappear:self];
    }
    return;
}

- (void)initTabBar
{
    float navHeight = [UIDevice currentDevice].systemVersion.floatValue >= 7.0 ? 0 : 20;
    
    self.tabBar = [[UITabBar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - 49 - navHeight, self.view.bounds.size.width, 49)];
    [self.tabBar setBackgroundImage:[UIImage imageNamed:@"tab_bg"]];
    self.tabBar.delegate = self;
    [self.view addSubview:self.tabBar];
    
    
    UITabBarItem * tab1 = [[UITabBarItem alloc] initWithTitle:@"我的应用" image:[[UIImage imageNamed:@"tab_my_app"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"tab_my_app_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tab1 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:158.0/255.0 green:0.0/255.0 blue:112.0/255.0 alpha:1], UITextAttributeTextColor,nil] forState:UIControlStateSelected];
    [tab1 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:56/255.0 green:56/255.0 blue:56/255.0 alpha:1], UITextAttributeTextColor,nil] forState:UIControlStateNormal];
    
    UITabBarItem * tab2 = [[UITabBarItem alloc] initWithTitle:@"实时订单" image:[[UIImage imageNamed:@"tab_theme"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"tab_theme_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tab2 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:158.0/255.0 green:0.0/255.0 blue:112.0/255.0 alpha:1], UITextAttributeTextColor,nil] forState:UIControlStateSelected];
    [tab2 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:56/255.0 green:56/255.0 blue:56/255.0 alpha:1], UITextAttributeTextColor,nil] forState:UIControlStateNormal];
    
    UITabBarItem * tab3 = [[UITabBarItem alloc] initWithTitle:@"主题应用2" image:[[UIImage imageNamed:@"tab_theme"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"tab_theme_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tab3 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:158.0/255.0 green:0.0/255.0 blue:112.0/255.0 alpha:1], UITextAttributeTextColor,nil] forState:UIControlStateSelected];
    [tab3 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:56/255.0 green:56/255.0 blue:56/255.0 alpha:1], UITextAttributeTextColor,nil] forState:UIControlStateNormal];
    [self.tabBar setItems:[NSArray arrayWithObjects:tab1, tab2, tab3, nil]];
    
    
    [self.tabBar setSelectedItem:tab1];
}

- (void)initMyAppView
{
    
}

- (void)openOrCloseLeftView
{
    if ([self.delegate respondsToSelector:@selector(mainControllerDidMenuButtonClick:)]) {
        [self.delegate mainControllerDidMenuButtonClick:self];
    }
}

- (void)endLauncherScrollEditState
{
    [_launcherScroll endEdit:_launcherScroll];
    self.navigationItem.rightBarButtonItem = self.msgBarButton;
    if ([self.delegate respondsToSelector:@selector(mainControllerWillAppear:)]) {
        [self.delegate mainControllerWillAppear:self];
    }
    self.navigationItem.leftBarButtonItem = self.menuBarButton;
    [TaPhoPersonalResTool launcherPagesToResArray:_launcherScroll.pages complete:^(NSMutableArray * resArray){
        NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
        [defaults rm_setCustomObject:resArray forKey:@"MY_APP_CONFIG"];
    }];
}

- (void)showMsgList
{
//    TIIMsgController * msgController = [[TIIMsgController alloc] initWithNibName:@"TIIMsgController" bundle:nil];
//    [self.navigationController pushViewController:msgController animated:YES];
}

# pragma mark - notification
// 收到点击打开app的通知
- (void)clickAppNotify:(NSNotification *)notification
{
    TaPhoPersonalRes * res = notification.object;
    [self clickAppLogic:res];
    return;
}

// 打开app
- (void)clickAppLogic:(TaPhoPersonalRes *)res
{
    TIIWebViewController * controller = [[TIIWebViewController alloc] initWithNibName:nil bundle:nil];
    res.resUrl = @"http://183.250.160.9:80/mobile-test/pages-lrp/rtOrderQuery.html";
    controller.res = res;
    [self.delegate mainControllerNeedsPresentViewController:controller];
    // 存储使用记录
    [[MPStoreManager sharedInstance] addTrack:res.resId];
    return;
}

// 收到获取app成功的通知
- (void)getAppSuccessNotify:(NSNotification *)notification
{
    TaPhoPersonalRes * res = notification.object;
    LauncherItem *item = [TaPhoPersonalResTool taPhoPersonalResToLauncherItem:res];
    item.delegate = self;
    [_launcherScroll addLauncherItem:item];
    [TaPhoPersonalResTool launcherPagesToResArray:_launcherScroll.pages complete:^(NSMutableArray * resArray){
        NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
        [defaults rm_setCustomObject:resArray forKey:@"MY_APP_CONFIG"];
    }];
    return;
}

#pragma mark -
#pragma mark LauncherItem Delegate
- (void)launcherItemDidClickDelete:(LauncherItem *)item
{
    self.targetItem = item;
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"是否移除应用：%@", item.title] message:nil delegate:nil cancelButtonTitle:@"否" otherButtonTitles:@"是", nil];
    alert.delegate = self;
    [alert show];
}

- (void)launcherItemDidClick:(LauncherItem *)item
{
    if (item.launcherState == LauncherStateEndEdit) {
        [self.launcherScroll removeFolderScroll];
        TaPhoPersonalRes * res = [TaPhoPersonalResTool launcherItemToTaPhoPersonalRes:item];
        [self clickAppLogic:res];
    }
}

#pragma mark -
#pragma mark LauncherScrollView Delegate
- (void)launcherScrollDidBeginEdit:(LauncherScrollView *)scrollView
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(endLauncherScrollEditState)];
    self.navigationItem.leftBarButtonItem = nil;
    if ([self.delegate respondsToSelector:@selector(mainControllerWillDisappear:)]) {
        [self.delegate mainControllerWillDisappear:self];
    }
}

- (void)launcherScrollDidEndEdit:(LauncherScrollView *)scrollView
{
    
}

#pragma mark -
#pragma mark UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [self.launcherScroll deleteLauncherItem:_targetItem];
    }
}

#pragma mark -
#pragma mark UITabBar Delegate
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    self.title = item.title;
    if ([self.title isEqualToString:@"我的应用"]) {
        self.launcherScroll.hidden = NO;
        self.tiiWebView.hidden = YES;
    } else if ([self.title isEqualToString:@"实时订单"]){
        self.launcherScroll.hidden = YES;
        if (self.tiiWebView == nil) {
            self.tiiWebView = [[TIIWebView alloc] initWithFrame:self.launcherScroll.bounds];
            [self.view addSubview:self.tiiWebView];
            
            TaPhoPersonalRes * res = [[TaPhoPersonalRes alloc] init];
            res.resUrl = @"http://183.250.160.9:80/mobile-test/pages-lrp/rtOrderQuery.html";
            self.tiiWebView.res = res;
        }
        
        self.tiiWebView.hidden = NO;
    } else {
        self.launcherScroll.hidden = YES;
        self.tiiWebView.hidden = YES;
    }
}

@end
