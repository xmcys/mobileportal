//
//  ZbTrackMainViewController.m
//  TestProject
//
//  Created by 张斌 on 15/3/2.
//  Copyright (c) 2015年 007. All rights reserved.
//

// 痕迹视图缩小倍数
#define TrackViewScale 0.45
// 容器宽度
#define ScrollViewHeight    ([[UIScreen mainScreen] bounds].size.height * 0.74)
// 痕迹视图宽度
#define TrackViewWidth    ([[UIScreen mainScreen] bounds].size.width * TrackViewScale)
// 痕迹视图高度
#define TrackViewHeight    ([[UIScreen mainScreen] bounds].size.height * TrackViewScale)
// 痕迹视图间隔
#define TrackViewSpace 8
// 小图标宽高
#define TrackIconWidth 50
// 小图标距离痕迹视图距离
#define TrackIconToTop 15

#import "MPTrackViewController.h"
#import "MPAppDetailViewController.h"
#import "CTNavigationController.h"
#import "CTImageView.h"
#import "MPStoreManager.h"

@interface MPTrackViewController () <UIScrollViewDelegate, CTImageViewDelegate>

@property (nonatomic, strong) UIScrollView *sv;
@property (nonatomic, strong) NSArray *trackArray;

@end

@implementation MPTrackViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.trackArray = [[NSMutableArray alloc] init];
    // 获取存储的历史记录
    self.trackArray = [[MPStoreManager sharedInstance] trackArray];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.sv == nil) {
        // 滚动层
        CGRect svFrame = CGRectMake(0, ScreenHeight - ScrollViewHeight, ScreenWidth, ScrollViewHeight);
        self.sv = [[UIScrollView alloc] initWithFrame:svFrame];
        self.sv.backgroundColor = [UIColor clearColor];
        self.sv.delegate = self;
        self.sv.decelerationRate = UIScrollViewDecelerationRateFast;
        [self.view addSubview:self.sv];
        
        CGAffineTransform newTransform =
        CGAffineTransformScale(self.view.transform, TrackViewScale, TrackViewScale);
        CGFloat leftSpace = (ScreenWidth * (1 - TrackViewScale)) / 2;
        // 添加侧边栏缩略图
        CGRect leftFrame = CGRectMake(0, 0, leftSpace - TrackViewSpace, TrackViewHeight);
        CTImageView *ivLeft = [[CTImageView alloc] initWithFrame:leftFrame];
        ivLeft.image = QuickImage(@"menu_bg");
        ivLeft.contentMode = UIViewContentModeScaleToFill;
        ivLeft.delegate = self;
        [self.sv addSubview:ivLeft];
        
        NSString *className = @"MPAppDetailViewController";
        for (NSInteger i = 0; i < self.trackArray.count; i++) {
            CGRect frame = CGRectMake(leftSpace + i*TrackViewWidth + i*TrackViewSpace, 0, TrackViewWidth, TrackViewHeight);
            
            // 添加缩略版界面
            CTViewController *temp = [[CTViewController alloc] init];
            temp.title = @" ";
            MPAppDetailViewController *controller = (MPAppDetailViewController *)[[NSClassFromString(className) alloc] initWithNibName:className bundle:nil];
            controller.appId = self.trackArray[i];
            CTNavigationController * nav = [[CTNavigationController alloc] initWithRootViewController:temp];
            nav.view.tag = i;
            nav.viewControllers = @[temp, controller];
            [nav.view setTransform:newTransform];
            nav.view.frame = frame;
            [self addChildViewController:nav];
            [self.sv addSubview:nav.view];
            
            // 添加小图标
            CGRect iconFrame = CGRectMake(frame.origin.x + (frame.size.width - TrackIconWidth) / 2, CGRectGetMaxY(frame) + TrackIconToTop, TrackIconWidth, TrackIconWidth);
            CTImageView *ivIcon = [[CTImageView alloc] initWithFrame:iconFrame];
            ivIcon.url = @"http://img4q.duitang.com/uploads/item/201210/13/20121013155855_YxAL3.jpeg";
            [ivIcon loadImage];
            ivIcon.contentMode = UIViewContentModeScaleToFill;
            ivIcon.layer.cornerRadius = 5;
            ivIcon.layer.masksToBounds = YES;
            [self.sv addSubview:ivIcon];
            
            // 添加覆盖层
            UIView *coverView = [[UIView alloc] initWithFrame:frame];
            coverView.backgroundColor = [UIColor clearColor];
            UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(coverOnClick:)];
            tapGes.numberOfTapsRequired = 1;
            tapGes.numberOfTouchesRequired = 1;
            [coverView addGestureRecognizer:tapGes];
            coverView.tag = i;
            [self.sv addSubview:coverView];
        }
        if (self.trackArray.count != 0) {
            self.sv.contentSize = CGSizeMake(leftSpace * 2 + self.trackArray.count*TrackViewWidth + (self.trackArray.count-1)*TrackViewSpace, self.sv.frame.size.height);
        } else {
            self.sv.contentSize = CGSizeMake(leftSpace, self.sv.frame.size.height);
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    return;
}

# pragma mark - button click envents
- (void)coverOnClick:(UITapGestureRecognizer *)tap
{
    UIView *coverView = tap.view;
    NSInteger tag = coverView.tag;
    CTNavigationController *nav = self.childViewControllers[tag];
    UIViewController *detailController = nav.topViewController;
    [nav popToRootViewControllerAnimated:NO];
    if (self.delegate && [self.delegate respondsToSelector:@selector(mpTrackViewControllerBack:)]) {
        [self.delegate mpTrackViewControllerBack:detailController];
    }
    return;
}

# pragma mark - CTImageView delegate
- (void)ctImageViewLoadDone:(CTImageView *)ctImageView withResult:(CTImageViewState)state
{
    return;
}

- (void)ctImageViewDidClicked:(CTImageView *)ctImageView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(mpTrackViewControllerBack:)]) {
        [self.delegate mpTrackViewControllerBack:nil];
    }
    return;
}

# pragma mark - UIScrollView delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (!scrollView.tracking) {
        // 减速中
        
//        CGFloat dis = ABS(scrollView.contentOffset.x - self.oldOffset.x);
//        NSLog(@"dis=%f", dis);
//        NSLog(@"x=%f", scrollView.contentOffset.x);
    }
//    NSLog(@"decelerate=%hhd", scrollView.decelerating);
    return;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate) {
        // 拖拽结束并不再减速滚动
        [self scrollViewScrollEndLogic:scrollView];
    }
    return;
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
//    NSLog(@"current x=%f y=%f", scrollView.contentOffset.x, scrollView.contentOffset.y);
//    NSLog(@"speed x=%f y=%f", velocity.x, velocity.y);
//    NSLog(@"target x=%f y=%f", targetContentOffset->x, targetContentOffset->y);
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    // 减速滚动结束
    [self scrollViewScrollEndLogic:scrollView];
}

- (void)scrollViewScrollEndLogic:(UIScrollView *)scrollView
{
//    int x = scrollView.contentOffset.x;
//    int midW = TrackViewWidth + TrackViewSpace;
//    if((x%midW) > midW/2){
//        [scrollView setContentOffset:CGPointMake((x/midW + 1) * midW, 0) animated:YES];
//    }else{
//        [scrollView setContentOffset:CGPointMake((x/midW) * midW, 0) animated:YES];
//    }
    return;
}

@end
