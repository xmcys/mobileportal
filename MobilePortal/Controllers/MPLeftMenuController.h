//
//  LeftMenuController.h
//  MobilePortal
//
//  Created by Chenhc on 15/3/4.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CTViewController.h"

@class MPLeftMenuController;

typedef NS_ENUM(NSUInteger, LeftMenuFunctionName) {
    LeftMenuFunctionUserInfo = 900001,
    LeftMenuFunctionAppCenter,
    LeftMenuFunctionHistory,
    LeftMenuFunctionSetting,
    LeftMenuFunctionAppComment,
    LeftMenuFunctionAbout
};

@protocol LeftMenuDelegate <NSObject>

@optional
- (void)leftMenuController:(MPLeftMenuController *)controller didItemClicked:(LeftMenuFunctionName)leftMenuFunctionName;

@end

@interface MPLeftMenuController : CTViewController

@property (nonatomic, weak) id<LeftMenuDelegate> delegate;

@end
