//
//  MPAppCommentTableViewCell.h
//  MobilePortal
//
//  Created by 张斌 on 15/3/6.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

typedef void (^BtnTouchBlock)(void);

#import <UIKit/UIKit.h>
#import "MPAppComment.h"

@interface MPAppCommentTableViewCell : UITableViewCell

@property (nonatomic, copy) BtnTouchBlock btnTouchBlock;

- (void)setComment:(MPAppComment *)comment withIndex:(NSInteger)commentIndex;
- (CGFloat)calcCellHeight:(MPAppComment *)comment;

@end
