//
//  MPAppDetailViewController.h
//  MobilePortal
//
//  Created by 张斌 on 15/3/5.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"

@interface MPAppDetailViewController : CTViewController

@property (nonatomic, strong) NSString *appId;

@end
