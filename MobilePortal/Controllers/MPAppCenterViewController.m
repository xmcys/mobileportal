//
//  ZbAppCenterViewController.m
//  TestProject
//
//  Created by 张斌 on 15/3/3.
//  Copyright (c) 2015年 007. All rights reserved.
//

#import "MPAppCenterViewController.h"
#import "MPAppTypeView.h"
#import "MPAppTableViewCell.h"
#import "MPAppSearchViewController.h"
#import "TaPhoPersonalRes.h"
#import "MPAppDetailViewController.h"
#import "MPStoreManager.h"
#import "RMMapper.h"

static NSString * const AppCellIdentifier = @"MPAppTableViewCell";

@interface MPAppCenterViewController () <UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet MPAppTypeView *slideView;
@property (strong, nonatomic) IBOutlet UITableView *tv;
@property (strong, nonatomic) NSMutableArray *array;
@property (nonatomic, assign) BOOL isInit;

- (void)btnSearchOnClick:(id)btn;
- (void)loadAppData;

@end

@implementation MPAppCenterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"应用中心";
    self.array = [[NSMutableArray alloc] init];
    
    UIButton * rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.backgroundColor = [UIColor clearColor];
    [rightBtn setImage:[UIImage imageNamed:@"icon_h40_search"] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(btnSearchOnClick:) forControlEvents:UIControlEventTouchUpInside];
    rightBtn.frame = CGRectMake(0, 0, 40, 44);
    rightBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    
//    __weak MPAppCenterViewController *weakSelf = self;
    //应用类别点击事件
    self.slideView.touchIndexPath = ^(NSIndexPath *indexPath){
        //CGFloat tagOffset = indexPath.row * weakSelf.container.frame.size.width;
        //[weakSelf.container setContentOffset:CGPointMake(tagOffset, 0) animated:YES];
    };
    
    [self.tv registerNib:[UINib nibWithNibName:@"MPAppTableViewCell" bundle:nil] forCellReuseIdentifier:AppCellIdentifier];
    
    [self loadAppData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)initUI
{
    return;
}

# pragma mark - url requests
- (void)loadAppData
{
    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"appCenterItems" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:jsonPath];
    NSArray *appArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    NSArray *appResArray = [RMMapper arrayOfClass:TaPhoPersonalRes.class fromArrayOfDictionary:appArray];
    self.array = [NSMutableArray arrayWithArray:appResArray];
    return;
}

# pragma mark - button click envents
- (void)btnSearchOnClick:(id)btn
{
    MPAppSearchViewController *appSearchController = [[MPAppSearchViewController alloc] initWithNibName:@"MPAppSearchViewController" bundle:nil];
    [self.navigationController pushViewController:appSearchController animated:YES];
    return;
}

# pragma mark - UITableView delegate && datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.array.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 71;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MPAppTableViewCell *cell = (MPAppTableViewCell *)[self.tv dequeueReusableCellWithIdentifier:AppCellIdentifier forIndexPath:indexPath];
    cell.btnTouchBlock = ^(void) {
    };
    [cell setIsShowDownLoad:YES];
    TaPhoPersonalRes *appInfo = [self.array objectAtIndex:indexPath.row];
    [cell setCurAppInfo:appInfo];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    MPAppDetailViewController *appDetail = [[MPAppDetailViewController alloc] initWithNibName:@"MPAppDetailViewController" bundle:nil];
    TaPhoPersonalRes *app = [self.array objectAtIndex:indexPath.row];
    appDetail.appId = app.resId;
    [self.navigationController pushViewController:appDetail animated:YES];
    
    // 存储使用记录
    [[MPStoreManager sharedInstance] addTrack:app.resId];
}

@end
