//
//  SlideView.m
//  NewiPhone
//
//  Created by xjkj on 14/12/15.
//  Copyright (c) 2014年 xjkj. All rights reserved.
//

#import "MPAppTypeView.h"
#import "MPAppTypeCell.h"

static NSString * const MPAppTypeCellIdentifier = @"MPAppTypeCell";

@interface MPAppTypeView ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@end

@implementation MPAppTypeView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self == [super initWithFrame:frame]) {
    }
    return self;
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    if (self.collectionView == nil) {
        [self setUpFrame];
        [self setData];
    }
}

- (void)setData
{
    if (self.dataArry) {
       [self.dataArry removeAllObjects];
    }else{
        self.dataArry = [[NSMutableArray alloc]init];
        self.selectedRow = 0;
    }
    
    // 需设置数据
    NSDictionary *dic1 = @{@"title":@"人劳", @"":@""};
    NSDictionary *dic2 = @{@"title":@"批零在线", @"":@""};
    NSDictionary *dic3 = @{@"title":@"专卖", @"":@""};
    NSDictionary *dic4 = @{@"title":@"监察", @"":@""};
    NSDictionary *dic5 = @{@"title":@"报表", @"":@""};
    NSDictionary *dic6 = @{@"title":@"测试五个字", @"":@""};
    self.dataArry = [NSMutableArray arrayWithArray:@[dic1, dic2, dic3, dic4, dic5, dic6]];
//    self.dataArry = [NSMutableArray arrayWithArray:@[dic1, dic2]];
    
    [self.collectionView reloadData];
}

-(void)setUpFrame
{
    UICollectionViewFlowLayout *water = [[UICollectionViewFlowLayout alloc] init];
    water.minimumLineSpacing = 0;
    water.minimumInteritemSpacing = 0;
    water.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    CGRect bounds = self.bounds;
    self.collectionView = [[UICollectionView alloc] initWithFrame:bounds collectionViewLayout:water];
    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.collectionView.showsVerticalScrollIndicator = NO;
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    //self.collectionView.pagingEnabled=YES;
    self.collectionView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.collectionView];
  
    [self.collectionView registerNib:[UINib nibWithNibName:@"MPAppTypeCell" bundle:nil]
          forCellWithReuseIdentifier:MPAppTypeCellIdentifier];
}

#pragma mark ------------------- collectionView基础设置------------------
#pragma mark 设置分区
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 1;
}

#pragma mark 每个区内的元素个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.dataArry.count;
}

#pragma mark 设置元素内容
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MPAppTypeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MPAppTypeCellIdentifier forIndexPath:indexPath];
    NSDictionary *dic = [self.dataArry objectAtIndex:indexPath.row];
    
    [cell setState:dic
          wtihType:self.selectedRow
     withIndexPath:indexPath];
    return cell;
}

#pragma mark 设置元素大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(EachButtonWidth, 87);
}

#pragma mark ------------------- collectionView点击事件------------------
#pragma mark 点击元素触发事件
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    self.selectedRow = (int)indexPath.row;
    [self.collectionView reloadData];
    
    [collectionView scrollToItemAtIndexPath:indexPath
                                  atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                          animated:YES];
    //传出去点击事件
    if (self.touchIndexPath) {
        self.touchIndexPath(indexPath);
    }
}

-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
   // CLog(@"上一次点击%ld",(long)indexPath.row);
}

@end
