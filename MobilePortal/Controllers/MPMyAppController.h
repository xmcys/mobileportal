//
//  MainController.h
//  MobilePortal
//
//  Created by Chenhc on 15/3/4.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"

@class MPMyAppController;

@protocol MainDelegate <NSObject>

@optional
- (void)mainControllerDidMenuButtonClick:(MPMyAppController *)controller;
- (void)mainControllerWillAppear:(MPMyAppController *)controller;
- (void)mainControllerWillDisappear:(MPMyAppController *)controller;
- (void)mainControllerNeedsPresentViewController:(UIViewController *)controller;

@end

@interface MPMyAppController : CTViewController

@property (nonatomic, weak) id<MainDelegate> delegate;

@end
