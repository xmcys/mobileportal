//
//  ZbTrackMainViewController.h
//  TestProject
//
//  Created by 张斌 on 15/3/2.
//  Copyright (c) 2015年 007. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MPTrackViewControllerDelegate <NSObject>

- (void)mpTrackViewControllerBack:(UIViewController *)tagController;

@end

@interface MPTrackViewController : UIViewController

@property (nonatomic, weak) id<MPTrackViewControllerDelegate> delegate;

@end
