//
//  MPAppCommentTableViewCell.m
//  MobilePortal
//
//  Created by 张斌 on 15/3/6.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#define MaxNumOfLineWhenNotExpland 2   // 未展开时最大行数
#define BtnMoreBottomWhenShow 5        // "更多"按钮底部距离，当隐藏时
#define BtnMoreBottomWhenHide 18       // "更多"按钮底部距离，当显示时
#define BtnMoreHeight 30               // "更多"按钮高度

#import "MPAppCommentTableViewCell.h"

@interface MPAppCommentTableViewCell ()

@property (strong, nonatomic) IBOutlet UILabel *labName;
@property (strong, nonatomic) IBOutlet UILabel *labContent;
@property (strong, nonatomic) IBOutlet UIButton *btnExpand;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *alcBtnMoreHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *alcBtnMoreBottom;
@property (nonatomic, weak) MPAppComment *curComment;
@property (nonatomic, assign) NSInteger commentIndex;

- (IBAction)btnMoreOnClick:(UIButton *)sender;

@end

@implementation MPAppCommentTableViewCell

- (void)awakeFromNib {
    self.labContent.preferredMaxLayoutWidth = ScreenWidth - 35 - 8;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setComment:(MPAppComment *)comment withIndex:(NSInteger)commentIndex
{
    self.commentIndex = commentIndex;
    self.curComment = comment;
    self.labName.text = [NSString stringWithFormat:@"%ld.%@-%@", (long)self.commentIndex, comment.personName, comment.operDate];
    self.labContent.text = comment.content;
    
    if (self.curComment.isExpand) {
        self.alcBtnMoreHeight.constant = 0;
        self.alcBtnMoreBottom.constant = BtnMoreBottomWhenHide;
        self.btnExpand.hidden = YES;
        self.labContent.numberOfLines = 0;
        return;
    }
    CGSize size = [self.labContent.text sizeWithFont:self.labContent.font constrainedToSize:CGSizeMake(self.labContent.preferredMaxLayoutWidth, CGFLOAT_MAX) lineBreakMode:self.labContent.lineBreakMode];
    CGSize oneLineSize = [@"test" sizeWithFont:self.labContent.font constrainedToSize:CGSizeMake(self.labContent.preferredMaxLayoutWidth, CGFLOAT_MAX) lineBreakMode:self.labContent.lineBreakMode];
    if (size.height <= oneLineSize.height * MaxNumOfLineWhenNotExpland) {
        self.alcBtnMoreHeight.constant = 0;
        self.btnExpand.hidden = YES;
        self.alcBtnMoreBottom.constant = BtnMoreBottomWhenHide;
        self.labContent.numberOfLines = 0;
    } else {
        self.alcBtnMoreHeight.constant = BtnMoreHeight;
        self.btnExpand.hidden = NO;
        self.alcBtnMoreBottom.constant = BtnMoreBottomWhenShow;
        self.labContent.numberOfLines = MaxNumOfLineWhenNotExpland;
    }
    return;
}

- (CGFloat)calcCellHeight:(MPAppComment *)comment
{
    self.labContent.text = comment.content;
    
    if (comment.isExpand) {
        self.alcBtnMoreHeight.constant = 0;
        self.alcBtnMoreBottom.constant = BtnMoreBottomWhenHide;
        self.labContent.numberOfLines = 0;
    } else {
        CGSize size = [self.labContent.text sizeWithFont:self.labContent.font constrainedToSize:CGSizeMake(self.labContent.preferredMaxLayoutWidth, CGFLOAT_MAX) lineBreakMode:self.labContent.lineBreakMode];
        CGSize oneLineSize = [@"test" sizeWithFont:self.labContent.font constrainedToSize:CGSizeMake(self.labContent.preferredMaxLayoutWidth, CGFLOAT_MAX) lineBreakMode:self.labContent.lineBreakMode];
        if (size.height <= oneLineSize.height * MaxNumOfLineWhenNotExpland) {
            self.alcBtnMoreHeight.constant = 0;
            self.alcBtnMoreBottom.constant = BtnMoreBottomWhenHide;
            self.labContent.numberOfLines = 0;
        } else {
            self.alcBtnMoreHeight.constant = BtnMoreHeight;
            self.alcBtnMoreBottom.constant = BtnMoreBottomWhenShow;
            self.labContent.numberOfLines = MaxNumOfLineWhenNotExpland;
        }
    }
    CGSize size = [self.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 1.0f; // Add 1.0f for the cell separator height
}

- (IBAction)btnMoreOnClick:(UIButton *)sender
{
    self.curComment.isExpand = YES;
    if (self.btnTouchBlock) {
        self.btnTouchBlock();
    }
    return;
}

@end
