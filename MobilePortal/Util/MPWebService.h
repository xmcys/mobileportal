//
//  MPWebService.h
//  MobilePortal
//
//  Created by Chenhc on 15/3/18.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BiPerson.h"

@interface MPWebService : NSObject

+ (void)setUser:(BiPerson *)person;

+ (BiPerson *)user;

+ (NSString *)widePriceShow:(NSString *)companyCode;

@end
