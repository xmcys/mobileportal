//
//  MPWebService.m
//  MobilePortal
//
//  Created by Chenhc on 15/3/18.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "MPWebService.h"
#import "NSUserDefaults+RMSaveCustomObject.h"
#import "RMMapper.h"

static BiPerson * user;

@implementation MPWebService

+ (void)setUser:(BiPerson *)person
{
    user = person;
    NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
    [ud rm_setCustomObject:user forKey:@"BI_PERSON"];
}

+ (BiPerson *)user
{
    if (user == nil) {
        NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
        user = [ud rm_customObjectForKey:@"BI_PERSON"];
    }
    return user;
}

+ (NSString *)widePriceShow:(NSString *)companyCode
{
    return @"";
}

@end
