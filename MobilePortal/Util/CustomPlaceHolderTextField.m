//
//  CustomPlaceHolderTextField.m
//  MobilePortal
//
//  Created by 张斌 on 15/3/12.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustomPlaceHolderTextField.h"

@implementation CustomPlaceHolderTextField

- (instancetype)init
{
    if (self = [super init]) {
        self.placeholder = [UIColor whiteColor];
    }
    return self;
}

- (void)drawPlaceholderInRect:(CGRect)rect
{
    [self.placeHolderColor setFill];
    
    [[self placeholder] drawInRect:rect withFont:[UIFont systemFontOfSize:14]];
}

- (CGRect)placeholderRectForBounds:(CGRect)bounds
{
    CGSize size = [self.placeholder sizeWithFont:[UIFont systemFontOfSize:14]];
    CGRect placeHolderF = CGRectMake(0, 0, size.width, size.height);
    return placeHolderF;
}

@end
