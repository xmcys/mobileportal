//
//  CustomPlaceHolderTextField.h
//  MobilePortal
//
//  Created by 张斌 on 15/3/12.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPlaceHolderTextField : UITextField

@property (nonatomic, strong) UIColor *placeHolderColor;

@end
