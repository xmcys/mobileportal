//
//  DownloadManager.h
//
//
//  Created by zb.
//  Copyright (c) 2015 007. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MPDownloadItem.h"

#define kDownloadManagerNotification @"DownloadManagerNotification"

@interface MPDownloadManager : NSObject

+ (MPDownloadManager *)sharedInstance;

//已在下载队列中,不进行二次下载
//已下载完成的,重新下载
//默认不重新下载已下载的
- (void)startDownload:(NSString*)resourceUrl withLocalPath:(NSString *)localPath;
- (void)startDownload:(NSString*)resourceUrl withLocalPath:(NSString *)localPath reStartFinished:(BOOL)restart;
- (void)pauseDownload:(NSString *)resourceUrl;
- (void)pauseAllDownloadTask;
- (void)cancelDownload:(NSString *)resourceUrl;
- (void)cancelAllDownloadTask;

//是否正在下载
- (BOOL)isExistInDowningQueue:(NSString *)url;
//是否下载完成
- (BOOL)isExistInFinshQueue:(NSString *)url;

- (MPDownloadItem *)getDownloadItemByUrl:(NSString *)url;

- (NSMutableDictionary *)getDownloadingTask;
- (NSMutableDictionary *)getFinishedTask;


@end
