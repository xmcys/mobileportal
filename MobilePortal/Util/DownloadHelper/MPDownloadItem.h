//
//  MPDownloadItem.h
//
//
//  Created by zb.
//  Copyright (c) 2015 007. All rights reserved.
//

#import "ASIHTTPRequest.h"

typedef enum  {
    MPDownloadNotStart=0,
    MPDownloadWait=1,
    MPDownloading,
    MPDownloadPaused,
    MPDownloadFailed,
    MPDownloadFinished,
}MPDownloadItemState;

#define kDownloadItemStateChanaged @"DownloadItemStateChanged"
#define kDownloadItemProgressChanged @"DownloadItemProgressChanged"

@interface MPDownloadItem : ASIHTTPRequest<ASIProgressDelegate,ASIHTTPRequestDelegate>

@property(nonatomic, assign) MPDownloadItemState downloadState;
@property(nonatomic, strong) NSString *downloadStateDescription;
@property(nonatomic, strong) NSString *createDate;
@property(nonatomic, assign) double receivedLength;
@property(nonatomic, assign) double totalLength;
@property(nonatomic, assign) double downloadPercent;
@property(nonatomic, copy) void (^DownloadItemStateChangedCallback)(MPDownloadItem *callbackItem);
@property(nonatomic, copy) void (^DownloadItemProgressChangedCallback)(MPDownloadItem *callbackItem);


- (void)startDownloadTask;
- (void)pauseDownloadTask;
- (void)cancelDownloadTask;

+ (NSString *)getDownloadStateDescriptionFromState:(MPDownloadItemState)state;

@end
