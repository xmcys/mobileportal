//
//  DownloadManager.m
//
//
//  Created by zb.
//  Copyright (c) 2015 007. All rights reserved.
//

#import "MPDownloadManager.h"
#import "MPDownloadItem.h"
#import "MPStoreManager.h"

#define kMaxDownloadCount 3

static MPDownloadManager *_instance;
@interface MPDownloadManager()
{
}

@property(nonatomic, strong) NSMutableDictionary *pauseQueue;//1、正常暂停 2、下载失败暂停
@property(nonatomic, strong) NSMutableDictionary *waittingQueue;
@property(nonatomic, strong) NSMutableDictionary *downlodingQueue;
@property(nonatomic, strong) NSMutableDictionary *finishedQueue;


-(void)checkDownloadingQueue;
-(void)checkWaiitingQueue;
-(void)checkFinishedQueue;

-(void)startValidItem;

@end



@implementation MPDownloadManager

+(MPDownloadManager *)sharedInstance
{
   @synchronized(self)
    {
        if(_instance == nil)
        {
            _instance = [[MPDownloadManager alloc]init];
            _instance.downlodingQueue = [NSMutableDictionary new];
            _instance.waittingQueue = [NSMutableDictionary new];
            _instance.finishedQueue = [NSMutableDictionary new];
            _instance.pauseQueue = [NSMutableDictionary new];
            
            NSMutableArray *allTask = [[MPStoreManager sharedInstance] getAllStoreDownloadTask];
            for(MPDownloadItem *downItem in allTask)
            {
                [_instance registerDownloadItemCallback:downItem];
                if(downItem.downloadState == MPDownloadFinished)
                {
                    [_instance insertQueue:_instance.finishedQueue checkExistItem:downItem];
                }
                else
                {
                    downItem.downloadState = MPDownloadPaused;
                    [_instance insertQueue:_instance.pauseQueue checkExistItem:downItem];
                }
            }
        }
    }
    return _instance;
}

-(void)registerDownloadItemCallback:(MPDownloadItem *)downItem
{
    downItem.DownloadItemStateChangedCallback = ^(MPDownloadItem *callbackItem)
    {
        NSLog(@"%d",callbackItem.downloadState);
        switch (callbackItem.downloadState) {
            case MPDownloadFailed:
            {
                [_instance insertQueue:_instance.pauseQueue checkExistItem:callbackItem];
                [_instance checkPauseQueue];
            }
                break;
            case MPDownloadFinished:
            {
                [_instance insertQueue:_instance.finishedQueue checkExistItem:callbackItem];
                [_instance checkFinishedQueue];
            }
                break;
            case MPDownloadPaused:
            {
                [_instance insertQueue:_instance.pauseQueue checkExistItem:callbackItem];
                [_instance checkPauseQueue];
            }
                break;
            case MPDownloadWait:
            {
                
            }
                break;
            default:
                break;
        }
        [_instance startValidItem];
        [[NSNotificationCenter defaultCenter]postNotificationName:kDownloadManagerNotification object:callbackItem userInfo:nil];
    };
    downItem.DownloadItemProgressChangedCallback=^(MPDownloadItem *callbackItem)
    {
        //        NSLog(@"%f",callbackItem.downloadPercent);
        [[NSNotificationCenter defaultCenter]postNotificationName:kDownloadManagerNotification object:callbackItem userInfo:nil];
    };

}

-(void)checkPauseQueue
{
    [_pauseQueue.allValues enumerateObjectsUsingBlock:^(id obj,NSUInteger index,BOOL *stop){
        MPDownloadItem *item = obj;
        NSString *url=[item.url description];

        if([_downlodingQueue objectForKey:url])
        {
            [_downlodingQueue removeObjectForKey:url];
        }

        if([_waittingQueue objectForKey:url])
        {
            [_waittingQueue removeObjectForKey:url];
        }

        if([_finishedQueue objectForKey:url])
        {
            [_finishedQueue removeObjectForKey:url];
        }
    }];
}

-(void)checkDownloadingQueue
{
    [_downlodingQueue.allValues enumerateObjectsUsingBlock:^(id obj,NSUInteger index,BOOL *stop){
        MPDownloadItem *item = obj;
        NSString *url=[item.url description];

        if([_waittingQueue objectForKey:url])
        {
            [_waittingQueue removeObjectForKey:url];
        }
        
        if([_pauseQueue objectForKey:url])
        {
            [_pauseQueue removeObjectForKey:url];
        }
        
        if([_finishedQueue objectForKey:url])
        {
            [_finishedQueue removeObjectForKey:url];
        }
    }];

}

-(void)checkFinishedQueue
{
    [_finishedQueue.allValues enumerateObjectsUsingBlock:^(id obj,NSUInteger index,BOOL *stop){
        MPDownloadItem *item = obj;
        NSString *url=[item.url description];
        

        if([_waittingQueue objectForKey:url])
        {
            [_waittingQueue removeObjectForKey:url];
        }

        if([_pauseQueue objectForKey:url])
        {
            [_pauseQueue removeObjectForKey:url];
        }
        
        if([_downlodingQueue objectForKey:url])
        {
            [_downlodingQueue removeObjectForKey:url];
        }
    }];
  
}

-(void)checkWaiitingQueue
{
    [_waittingQueue.allValues enumerateObjectsUsingBlock:^(id obj,NSUInteger index,BOOL *stop){
        MPDownloadItem *item = obj;
        NSString *url=[item.url description];
 
        if([_finishedQueue objectForKey:url])
        {
            [_finishedQueue removeObjectForKey:url];
        }

        if([_downlodingQueue objectForKey:url])
        {
            [_downlodingQueue removeObjectForKey:url];
        }
        if([_pauseQueue objectForKey:url])
        {
            [_pauseQueue removeObjectForKey:url];
        }
    }];
}

-(void)startValidItem
{
    if([_downlodingQueue count]>=kMaxDownloadCount)
    {
        return;
    }
    
    MPDownloadItem *item=  [_waittingQueue.allValues lastObject];
    NSString *url=[item.url description];
    if(item)
    {
        if(![_downlodingQueue objectForKey:url])
        {
            [_downlodingQueue setObject:item forKey:url];
            [item startDownloadTask];
            [self checkDownloadingQueue];
            
        }
    }
}

//把等待队列和下载队列中的任务暂停，加入暂停队列
-(void)pauseDownload:(NSString *)resourceUrl
{
    MPDownloadItem *downItemInWait=[_waittingQueue objectForKey:resourceUrl];
    if(downItemInWait)
    {
        [downItemInWait pauseDownloadTask];
        [_waittingQueue removeObjectForKey:resourceUrl];
        [self insertQueue:_pauseQueue checkExistItem:downItemInWait];
    }
    
    MPDownloadItem *downItemInDowning=[_downlodingQueue objectForKey:resourceUrl];
    if(downItemInDowning)
    {
        [downItemInDowning pauseDownloadTask];
        [_downlodingQueue removeObjectForKey:resourceUrl];
        [self insertQueue:_pauseQueue checkExistItem:downItemInDowning];
    }
}

-(void)pauseAllDownloadTask
{
    [_waittingQueue.allValues enumerateObjectsUsingBlock:^(id obj,NSUInteger index,BOOL *stop){
        MPDownloadItem *downItem=obj;
        [downItem pauseDownloadTask];
        [self insertQueue:_pauseQueue checkExistItem:downItem];
    }];
    [_waittingQueue removeAllObjects];
    
    
    [_downlodingQueue.allValues enumerateObjectsUsingBlock:^(id obj,NSUInteger index,BOOL *stop){
        MPDownloadItem *downItem=obj;
        [downItem pauseDownloadTask];
        [self insertQueue:_pauseQueue checkExistItem:downItem];
    }];
    [_downlodingQueue removeAllObjects];
}

-(void)cancelDownload:(NSString *)resourceUrl
{
    MPDownloadItem *downItem=[_waittingQueue objectForKey:resourceUrl];
    if(downItem)
    {
        [downItem cancelDownloadTask];
        [_waittingQueue removeObjectForKey:resourceUrl];
    }
    
    downItem=[_downlodingQueue objectForKey:resourceUrl];
    if(downItem)
    {
        [downItem cancelDownloadTask];
        [_downlodingQueue removeObjectForKey:resourceUrl];
    }

    downItem=[_pauseQueue objectForKey:resourceUrl];
    if(downItem)
    {
        [downItem cancelDownloadTask];
        [_pauseQueue removeObjectForKey:resourceUrl];
    }

}


-(void)cancelAllDownloadTask
{
    [_waittingQueue.allValues enumerateObjectsUsingBlock:^(id obj,NSUInteger index,BOOL *stop){
        MPDownloadItem *downItem = obj;
        [downItem cancelDownloadTask];
    }];
    [_waittingQueue removeAllObjects];
    

    [_downlodingQueue.allValues enumerateObjectsUsingBlock:^(id obj,NSUInteger index,BOOL *stop){
        MPDownloadItem *downItem = obj;
        [downItem cancelDownloadTask];
    }];
    [_downlodingQueue removeAllObjects];
    
    
    [_pauseQueue.allValues enumerateObjectsUsingBlock:^(id obj,NSUInteger index,BOOL *stop){
        MPDownloadItem *downItem = obj;
        NSLog(@"downItem.downloadDestinationPath=%@,%@",downItem.downloadDestinationPath,downItem.temporaryFileDownloadPath);
        [downItem cancelDownloadTask];
    }];
    [_pauseQueue removeAllObjects];
}

-(void)insertDownloadingQueueItem:(MPDownloadItem *)downItem
{
    NSString *urlString = [downItem.url description];
    if([_downlodingQueue objectForKey:urlString])
    {
        [_downlodingQueue setObject:downItem forKey:urlString];
    }
}

//向队列queue新增一个下载任务
-(void)insertQueue:(NSMutableDictionary *)queue checkExistItem:(MPDownloadItem *)downItem
{
    if(queue==nil||downItem==nil||[downItem.url description]==nil)
    {
        return;
    }
    if(![queue objectForKey:[downItem.url description]])
    {
        [queue setObject:downItem forKey:[downItem.url description]];
    }
}

-(BOOL)isExistInDowningQueue:(NSString *)url
{
    return ([_downlodingQueue objectForKey:url])||([_waittingQueue objectForKey:url]);
}

-(BOOL)isExistInFinshQueue:(NSString *)url
{
    if([_finishedQueue objectForKey:url])
    {
        return YES;
    }
    return NO;
}

-(MPDownloadItem *)getDownloadItemByUrl:(NSString *)url
{
    if([_downlodingQueue objectForKey:url])
    {
        return [_downlodingQueue objectForKey:url];
    }
    else if([_waittingQueue objectForKey:url])
    {
        return [_waittingQueue objectForKey:url];
    }
    else if([_finishedQueue objectForKey:url])
    {
        return [_finishedQueue objectForKey:url];
    }
    else if([_pauseQueue objectForKey:url])
    {
        return [_pauseQueue objectForKey:url];
    }
    return nil;
}

-(void)startDownload:(NSString *)resourceUrl withLocalPath:(NSString *)localPath reStartFinished:(BOOL)restart
{
    NSLog(@"%@", localPath);
    if([self isExistInDowningQueue:resourceUrl])
    {
        return;
    }
    
    if(!restart)
    {
       if([self isExistInFinshQueue:resourceUrl])
       {
           return;
       }
    }
    
//    resourceUrl=[resourceUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //downloadItem 为一个ASIHttpRequest对象，取消后不能重新开始
    MPDownloadItem *originItem;
    if([_finishedQueue objectForKey:resourceUrl])
    {
        originItem = [_finishedQueue objectForKey:resourceUrl];
    }
    else if([_pauseQueue objectForKey:resourceUrl])
    {
        originItem = [_pauseQueue objectForKey:resourceUrl];
    }
    
    MPDownloadItem *downItem = [[MPDownloadItem alloc]initWithURL:[NSURL URLWithString:resourceUrl]];
    downItem.downloadDestinationPath = localPath;
    downItem.temporaryFileDownloadPath = [localPath stringByAppendingString:@".tmp"];
    downItem.receivedLength = originItem.receivedLength;
    downItem.totalLength = originItem.totalLength;
    downItem.downloadPercent = originItem.downloadPercent;
    [downItem setAllowResumeForFileDownloads:YES];
    [self registerDownloadItemCallback:downItem];
    
    downItem.downloadState = MPDownloadWait;
    [self insertQueue:_waittingQueue checkExistItem:downItem];
    [self checkWaiitingQueue];
    [self startValidItem];
}


-(void)startDownload:(NSString *)resourceUrl withLocalPath:(NSString *)localPath
{
    [self startDownload:resourceUrl withLocalPath:localPath reStartFinished:NO];
}

-(NSMutableDictionary *)getDownloadingTask
{
    NSMutableDictionary *downingQueue = [NSMutableDictionary new];
    [downingQueue addEntriesFromDictionary:_downlodingQueue];
    
    [downingQueue addEntriesFromDictionary:_waittingQueue];
    
    [downingQueue addEntriesFromDictionary:_pauseQueue];
    return downingQueue;
}

-(NSMutableDictionary *)getFinishedTask
{
    return _finishedQueue;
}
@end
