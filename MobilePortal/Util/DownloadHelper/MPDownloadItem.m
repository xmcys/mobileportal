//
//  DownloadItem.m
//  
//
//  Created by zb.
//  Copyright (c) 2015 007. All rights reserved.
//

#import "MPDownloadItem.h"
#import "MPStoreManager.h"

@interface MPDownloadItem ()
{
    NSTimer *_progressTimer;
    BOOL _isFristReciveBytes;//第一次返回的bytes是已传输的，防止与本地存储的大小累加
}

-(void)notifyItemStateChanged;
-(void)notifyItemProgressChanged;
-(void)monitorDownloadProgress:(NSTimer *)timer;

@end


@implementation MPDownloadItem

-(void)setDownloadState:(MPDownloadItemState)downloadState
{
    self.downloadStateDescription = [MPDownloadItem getDownloadStateDescriptionFromState:downloadState];
    if(_downloadState == downloadState)
    {
        return;
    }
    _downloadState = downloadState;
    
    [self notifyItemStateChanged];
}

-(void)notifyItemStateChanged
{
    if(self.DownloadItemStateChangedCallback)
    {
        self.DownloadItemStateChangedCallback(self);
    }
}
-(void)notifyItemProgressChanged
{
    if(self.DownloadItemProgressChangedCallback)
    {
        self.DownloadItemProgressChangedCallback(self);
    }
}

-(void)monitorDownloadProgress:(NSTimer *)timer
{
    [self notifyItemProgressChanged];
    
    //update db in background
    dispatch_async(dispatch_get_global_queue(0, 0), ^(void){
        [[MPStoreManager sharedInstance] updateDownloadTask:self];
    });
}

+(NSString *)getDownloadStateDescriptionFromState:(MPDownloadItemState)state
{
    NSString *stateDescription=@"";
    switch (state) {
        case MPDownloadFailed:
        {
            stateDescription=@"错误";
        }
            break;
        case MPDownloading:
        {
            stateDescription=@"下载中";
        }
            break;
        case MPDownloadFinished:
        {
            stateDescription=@"下载完成";
        }
            break;
        case MPDownloadNotStart:
        {
            stateDescription=@"未下载";
        }
            break;
        case MPDownloadWait:
        {
            stateDescription=@"等待中";
        }
            break;
        case MPDownloadPaused:
        {
            stateDescription=@"暂停";
        }
            break;
        default:
            break;
    }
    return stateDescription;
}

-(void)startDownloadTask
{
    [self setTimeOutSeconds:120];
    [self setDelegate:self];
    [self setDownloadProgressDelegate:self];
    [self startAsynchronous];
    
    //insert db in background
    dispatch_async(dispatch_get_global_queue(0, 0), ^(void){
        [[MPStoreManager sharedInstance]insertDownloadTask:self];
    });

    _progressTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(monitorDownloadProgress:) userInfo:nil repeats:YES];
    _isFristReciveBytes = YES;
    self.downloadState = MPDownloadWait;
    NSLog(@"start download url:%@",self.url);
}

-(void)pauseDownloadTask
{
    [self clearDelegatesAndCancel];
    if(_progressTimer)
    {
        [_progressTimer fire];
        [_progressTimer invalidate];
    }
    self.downloadState = MPDownloadPaused;
}

-(void)cancelDownloadTask
{
    self.receivedLength=0;
    self.totalLength=0;
    self.downloadPercent=0;
    //delete db in background
    dispatch_async(dispatch_get_global_queue(0, 0), ^(void){
        [[MPStoreManager sharedInstance] deleteDownloadTask:[self.url description]];
        
        //remove temp downloaded data
        [self removeTemporaryDownloadFile];
    });
    [self pauseDownloadTask];
    self.downloadState = MPDownloadNotStart;
}


#pragma mark - asi delegate
-(void)request:(ASIHTTPRequest *)request didReceiveResponseHeaders:(NSDictionary *)_responseHeaders
{
    NSLog(@"_responseHeaders=%@",_responseHeaders);
    NSString *rangeString=[_responseHeaders objectForKey:@"Content-Range"];
    if(rangeString)
    {
        NSInteger index = [rangeString rangeOfString:@"/"].location;
        if(index != NSNotFound)
        {
            _totalLength = [[rangeString substringFromIndex:index+1] doubleValue];
        }
    }
    else
    {
        NSString *lengthString = [_responseHeaders objectForKey:@"Content-Length"];
        if(lengthString)
        {
            _totalLength = [lengthString doubleValue];
        }
    }
    self.downloadState = MPDownloading;
    
    //update db in background
    dispatch_async(dispatch_get_global_queue(0, 0), ^(void){
        [[MPStoreManager sharedInstance] updateDownloadTask:self];
    });
}


-(void)requestFinished:(ASIHTTPRequest *)request
{
    if(_progressTimer)
    {
        [_progressTimer invalidate];
    }
    self.downloadState = MPDownloadFinished;
    
    //finished db in background
    dispatch_async(dispatch_get_global_queue(0, 0), ^(void){
        [[MPStoreManager sharedInstance] downloadFinishTask:self];
        [[MPStoreManager sharedInstance] updateDownloadTask:self];
    });
}

-(void)requestFailed:(ASIHTTPRequest *)_request
{
    NSLog(@"%@",_request.error);
    if(_progressTimer)
    {
        [_progressTimer invalidate];
    }
    self.downloadState = MPDownloadFailed;
}

#pragma mark - progress delegate
-(void)request:(ASIHTTPRequest *)request didReceiveBytes:(long long)bytes
{
    if(_isFristReciveBytes)
    {
        _receivedLength=bytes;
        _isFristReciveBytes=NO;
    }
    else
    {
        _receivedLength+=bytes;
    }
    if(_totalLength!=0)
    {
        self.downloadPercent=_receivedLength/_totalLength;
    }
}


@end
