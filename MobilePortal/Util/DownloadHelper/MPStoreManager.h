//
//  DownloadStoreManager.h
//
//
//  Created by zb.
//  Copyright (c) 2015 007. All rights reserved.
//

//交互对象是Model，不是Entity
#import <Foundation/Foundation.h>

@class MPDownloadItem;

@interface MPStoreManager : NSObject

+ (MPStoreManager *)sharedInstance;

- (void)setStoreId:(NSString *)storeId;

// 创建asi下载表
- (void)createAsiDownLoadTable;
- (NSString *)asiFileSavePath:(NSString *)appId withResUrl:(NSString *)resUrl;

// asi下载本地存储
- (NSMutableArray *)getAllStoreDownloadTask;
- (BOOL)isExistDownloadTask:(NSString *)url;
- (void)insertDownloadTask:(MPDownloadItem *)item;
- (void)deleteDownloadTask:(NSString *)url;
- (void)updateDownloadTask:(MPDownloadItem *)item;
-(void)downloadFinishTask:(MPDownloadItem *)item;

// 历史记录存储
- (void)addTrack:(NSString *)appId;
- (NSArray *)trackArray;

@end
