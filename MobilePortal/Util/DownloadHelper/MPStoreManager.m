//
//  DownloadStoreManager.m
//
//
//  Created by zb.
//  Copyright (c) 2015 007. All rights reserved.
//

#import "FMDB.h"
#import "MPStoreManager.h"
#import "MPDownloadItem.h"
#import "MPDownloadItemEntity.h"
#import "ZipArchive.h"

#define FMDBQuickCheck(SomeBool) { if (!(SomeBool)) { NSLog(@"Failure on line %d", __LINE__); } }

#define STORAGE_KEY_TRACK_ARRAY @"STORAGE_KEY_TRACK_ARRAY_%@"  // 历史记录存储key名

static MPStoreManager *_instance;

@interface MPStoreManager ()

// 存储id，如果需要将不同用户的存储分开，请设置此id
@property (nonatomic, strong) NSString *storeId;

@end


@implementation MPStoreManager

- (void)setStoreId:(NSString *)storeId
{
    _storeId = storeId;
}

- (FMDatabase *)openDBWithPath:(NSString *)dbPath
{
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    if (![db open]) {
        NSLog(@"Could not open db.");
        return nil;
    }
    return db;
}

- (NSString *)asiMainPath
{
    NSString* cacheDir = [NSSearchPathForDirectoriesInDomains( NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
    NSString* appDir = [cacheDir stringByAppendingPathComponent:@"app"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:appDir]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:appDir withIntermediateDirectories:YES attributes:nil error:nil];
    }
    NSString *userDir = [appDir stringByAppendingPathComponent:self.storeId];
    if (![[NSFileManager defaultManager] fileExistsAtPath:userDir]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:userDir withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return userDir;
}

- (NSString *)asiDBPath
{
    NSString *dbFileName = [NSString stringWithFormat:@"AsiDownloadRecord.sqlite"];
    return [[self asiMainPath] stringByAppendingPathComponent:dbFileName];
}

- (NSString *)fileNameFromUrl:(NSString *)url
{
    // 默认设置文件名为图片路径，去除http://，并且替换 / 为___
    NSString *fileName = [[url stringByReplacingOccurrencesOfString:@"http://" withString:@""] stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
    return fileName;
}

- (NSString *)asiFileSavePath:(NSString *)appId withResUrl:(NSString *)resUrl
{
    NSString *ext = [resUrl pathExtension];
    NSString *fileName = [appId stringByAppendingPathExtension:ext];
    return [[self asiMainPath] stringByAppendingPathComponent:fileName];
}

- (void)createAsiDownLoadTable
{
    FMDatabase *db = [self openDBWithPath:[self asiDBPath]];
    if (db == nil) {
        return;
    }
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS AsiDownload (downloadUrl text PRIMARY KEY,createDate text,downloadState text,currentSize text,totalSize text,downloadProgress text,downloadDestinationPath text,temporaryFileDownloadPath text)"];
    [db close];
    return;
}

+(MPDownloadItem *)transferToItem:(MPDownloadItemEntity *)entity
{
    MPDownloadItem *item = [[MPDownloadItem alloc]init];
    if(entity.downloadUrl)
    {
        item.url = [NSURL URLWithString:entity.downloadUrl];
    }
    item.temporaryFileDownloadPath = entity.temporaryFileDownloadPath;
    item.downloadDestinationPath = entity.downloadDestinationPath;
    item.totalLength = [entity.totalSize doubleValue];
    item.receivedLength = [entity.currentSize doubleValue];
    item.downloadState = [entity.downloadState intValue];
    item.downloadPercent = [entity.downloadProgress doubleValue];
    return item;
}

+(void)transfer:(MPDownloadItem *)item toEntity:(MPDownloadItemEntity *)entity;
{
    if([item.url description])
    {
        entity.downloadUrl = [item.url description];
    }
    entity.downloadDestinationPath = item.downloadDestinationPath;
    entity.temporaryFileDownloadPath = item.temporaryFileDownloadPath;
    entity.downloadState = [NSString stringWithFormat:@"%@", [NSNumber numberWithInt:item.downloadState]];
    entity.totalSize = [NSString stringWithFormat:@"%@", [NSNumber numberWithDouble:item.totalLength]];
    entity.currentSize = [NSString stringWithFormat:@"%@", [NSNumber numberWithDouble:item.receivedLength]];
    entity.downloadProgress = [NSString stringWithFormat:@"%@", [NSNumber numberWithDouble:item.downloadPercent]];
}

+(MPStoreManager *)sharedInstance
{
    @synchronized(self)
    {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            _instance = [[MPStoreManager alloc]init];
        });
    }
    return _instance;
}

-(BOOL)isExistDownloadTask:(NSString *)url
{
    if([self queryEntityByUrl:url])
    {
        return YES;
    }
    return NO;
}

-(MPDownloadItemEntity *)queryEntityByUrl:(NSString *)url
{
    FMDatabase *db = [self openDBWithPath:[self asiDBPath]];
    if (db == nil) {
        return nil;
    }
    
    MPDownloadItemEntity *curEntity = nil;
    FMResultSet *rs = [db executeQueryWithFormat:@"SELECT * FROM AsiDownload WHERE downloadUrl = %@", url];
    while ([rs next]) {
        curEntity = [[MPDownloadItemEntity alloc] init];
        curEntity.downloadUrl = [rs stringForColumn:@"downloadUrl"];
        curEntity.createDate = [rs stringForColumn:@"createDate"];
        curEntity.downloadState = [rs stringForColumn:@"downloadState"];
        curEntity.currentSize = [rs stringForColumn:@"currentSize"];
        curEntity.totalSize = [rs stringForColumn:@"totalSize"];
        curEntity.downloadProgress = [rs stringForColumn:@"downloadProgress"];
        curEntity.downloadDestinationPath = [rs stringForColumn:@"downloadDestinationPath"];
        curEntity.temporaryFileDownloadPath = [rs stringForColumn:@"temporaryFileDownloadPath"];
    }
    [rs close];
    [db close];
    return curEntity;
}

-(NSMutableArray *)getAllStoreDownloadTask
{
    NSMutableArray *downloadItemArray = [[NSMutableArray alloc] init];
    FMDatabase *db = [self openDBWithPath:[self asiDBPath]];
    if (db == nil) {
        return downloadItemArray;
    }
    
    FMResultSet *rs = [db executeQueryWithFormat:@"SELECT * FROM AsiDownload ORDER BY createDate desc"];
    while ([rs next]) {
        MPDownloadItemEntity *curEntity = [[MPDownloadItemEntity alloc] init];
        curEntity.downloadUrl = [rs stringForColumn:@"downloadUrl"];
        curEntity.createDate = [rs stringForColumn:@"createDate"];
        curEntity.downloadState = [rs stringForColumn:@"downloadState"];
        curEntity.currentSize = [rs stringForColumn:@"currentSize"];
        curEntity.totalSize = [rs stringForColumn:@"totalSize"];
        curEntity.downloadProgress = [rs stringForColumn:@"downloadProgress"];
        curEntity.downloadDestinationPath = [rs stringForColumn:@"downloadDestinationPath"];
        curEntity.temporaryFileDownloadPath = [rs stringForColumn:@"temporaryFileDownloadPath"];
        [downloadItemArray addObject:[MPStoreManager transferToItem:curEntity]];
    }
    [rs close];
    [db close];
    return downloadItemArray;
}

-(void)insertDownloadTask:(MPDownloadItem *)item
{
    if([self isExistDownloadTask:[item.url description]])
    {
        return;
    }
    FMDatabase *db = [self openDBWithPath:[self asiDBPath]];
    if (db == nil) {
        return;
    }
    MPDownloadItemEntity *entity = [[MPDownloadItemEntity alloc] init];
    [MPStoreManager transfer:item toEntity:entity];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd"];
    entity.createDate = [format stringFromDate:[NSDate date]];
    NSMutableDictionary *dictionaryArgs = [NSMutableDictionary dictionary];
    [dictionaryArgs setObject:entity.downloadUrl forKey:@"downloadUrl"];
    [dictionaryArgs setObject:entity.createDate forKey:@"createDate"];
    [dictionaryArgs setObject:entity.downloadState forKey:@"downloadState"];
    [dictionaryArgs setObject:entity.currentSize forKey:@"currentSize"];
    [dictionaryArgs setObject:entity.totalSize forKey:@"totalSize"];
    [dictionaryArgs setObject:entity.downloadProgress forKey:@"downloadProgress"];
    [dictionaryArgs setObject:entity.downloadDestinationPath forKey:@"downloadDestinationPath"];
    [dictionaryArgs setObject:entity.temporaryFileDownloadPath forKey:@"temporaryFileDownloadPath"];
    FMDBQuickCheck([db executeUpdate:@"insert into AsiDownload values (:downloadUrl, :createDate, :downloadState, :currentSize, :totalSize, :downloadProgress, :downloadDestinationPath, :temporaryFileDownloadPath)" withParameterDictionary:dictionaryArgs]);
    [db close];
    return;
}

-(void)deleteDownloadTask:(NSString *)url
{
    MPDownloadItemEntity *entity = [self queryEntityByUrl:url];
    if(entity)
    {
        FMDatabase *db = [self openDBWithPath:[self asiDBPath]];
        if (db == nil) {
            return;
        }
        
        BOOL result = [db executeUpdateWithFormat:@"delete from AsiDownload WHERE downloadUrl=%@", url];
        if (!result) {
            NSLog(@"delete AsiDownload failed!");
        }
        
        [db close];
        return;
    }
}

-(void)updateDownloadTask:(MPDownloadItem *)item
{
    MPDownloadItemEntity *entity = [self queryEntityByUrl:[item.url description]];
    if(entity)
    {
        FMDatabase *db = [self openDBWithPath:[self asiDBPath]];
        if (db == nil) {
            return;
        }
        
        [MPStoreManager transfer:item toEntity:entity];
        BOOL result = [db executeUpdateWithFormat:@"UPDATE AsiDownload SET downloadState=%@, currentSize=%@, totalSize=%@, downloadProgress=%@ WHERE downloadUrl=%@", entity.downloadState, entity.currentSize, entity.totalSize, entity.downloadProgress, entity.downloadUrl];
        if (!result) {
            NSLog(@"update AsiDownload failed!");
        }
        
        [db close];
        return;
    }
}

- (void)downloadFinishTask:(MPDownloadItem *)item
{
    NSString *filePath = item.downloadDestinationPath;
    NSString *ext = [filePath pathExtension];
    // 解压
    if ([ext isEqualToString:@"zip"]) {
        NSString *dir = [filePath stringByDeletingPathExtension];
        if (![[NSFileManager defaultManager] fileExistsAtPath:dir]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:dir withIntermediateDirectories:YES attributes:nil error:nil];
        }
        
        ZipArchive *za = [[ZipArchive alloc] init];
        // 1. 在内存中解压缩文件
        if ([za UnzipOpenFile: filePath]) {
            // 2. 将解压缩的内容写到缓存目录中
            BOOL ret = [za UnzipFileTo:dir overWrite: YES];
            if (NO == ret){} [za UnzipCloseFile];
        }
    }
}

// 历史记录存储
- (void)addTrack:(NSString *)appId
{
    // 获取存储的历史记录
    NSString *trackKey = [NSString stringWithFormat:STORAGE_KEY_TRACK_ARRAY, self.storeId];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *trackArray = [NSMutableArray arrayWithArray:[userDefaults arrayForKey:trackKey]];
    
    // 此次记录移至第一位
    if ([trackArray containsObject:appId]) {
        [trackArray removeObject:appId];
    }
    [trackArray insertObject:appId atIndex:0];
    // 重新存储
    [userDefaults setObject:[NSArray arrayWithArray:trackArray] forKey:trackKey];
    [userDefaults synchronize];
}

- (NSArray *)trackArray
{
    // 获取存储的历史记录
    NSString *trackKey = [NSString stringWithFormat:STORAGE_KEY_TRACK_ARRAY, self.storeId];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults arrayForKey:trackKey];
}

@end
