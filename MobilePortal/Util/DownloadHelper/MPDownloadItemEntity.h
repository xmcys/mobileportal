//
//  DownloadItemEntity.h
//
//
//  Created by zb.
//  Copyright (c) 2015 007. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MPDownloadItemEntity : NSObject

@property (nonatomic, strong) NSString * downloadUrl;
@property (nonatomic, strong) NSString * createDate;
@property (nonatomic, strong) NSString * downloadState;
@property (nonatomic, strong) NSString * currentSize;
@property (nonatomic, strong) NSString * totalSize;
@property (nonatomic, strong) NSString * downloadProgress;
@property (nonatomic, strong) NSString * downloadDestinationPath;
@property (nonatomic, strong) NSString * temporaryFileDownloadPath;

@end
