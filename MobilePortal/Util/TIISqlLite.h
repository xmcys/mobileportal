//
//  TIISqlLite.h
//  TmsInternalIos
//
//  Created by 张斌 on 14-7-24.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchCondition : NSObject

@property (nonatomic, strong) NSString *beginDate;
@property (nonatomic, strong) NSString *endDate;
@property (nonatomic, strong) NSString *msgType;
@property (nonatomic, strong) NSString *keyWords;

@end

@interface TIISqlLite : NSObject

+ (TIISqlLite *)shareManager;
- (void)createMsgTable;
- (void)addNewMsg:(NSArray *)msgArray;
- (void)deleteMsgByMsgId:(NSString *)msgId;
- (void)deleteMsgByIssueId:(NSString *)issueId;
- (NSArray *)getMsgByType:(NSString *)msgType;
- (NSArray *)getMsgByCondition:(SearchCondition *)condition;
- (SearchCondition *)getInfoByCondition:(NSString *)year withWeek:(NSString *)week;

@end
