//
//  CTXMLParser.h
//  MobilePortal
//
//  Created by 张斌 on 15/4/3.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CTXMLParser : NSObject

- (void)parseData:(NSData *)data forClass:(Class)cls complete:(void(^)(id clsObj, NSArray *array))block;
- (void)parseData:(NSData *)data forClass:(Class)cls itemElementName:(NSString *)name complete:(void(^)(id clsObj, NSArray *array))block;

@end
