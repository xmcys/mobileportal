//
//  CTXMLParser.m
//  MobilePortal
//
//  Created by 张斌 on 15/4/3.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#define DEFAULT_ELEMENT_NAME @"item"

#import "CTXMLParser.h"
#import "RMMapper.h"

@interface CTXMLParser () <NSXMLParserDelegate>

@property (nonatomic, copy) Class cls;
@property (nonatomic, strong) id tmpObj;
@property (nonatomic, strong) NSMutableString * tmpStr;
@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, copy) void(^block)(id clsObj, NSArray *array);
@property (nonatomic, strong) NSString *mainElementName;

@end

@implementation CTXMLParser

- (void)parseData:(NSData *)data forClass:(Class)cls complete:(void(^)(id clsObj, NSArray *array))block
{
    [self parseData:data forClass:cls itemElementName:DEFAULT_ELEMENT_NAME complete:block];
}

- (void)parseData:(NSData *)data forClass:(Class)cls itemElementName:(NSString *)name complete:(void(^)(id clsObj, NSArray *array))block
{
    self.block = block;
    self.cls = cls;
    self.mainElementName = name;
    self.array = [[NSMutableArray alloc] init];
    self.tmpObj = [[self.cls alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
    return;
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:self.mainElementName]) {
        id curObj = [[self.cls alloc] init];
        
        NSDictionary *clsPropDic = [RMMapper propertiesForClass:self.cls];
        for (NSString *key in attributeDict.allKeys) {
            NSString *propType = [clsPropDic objectForKey:key];
            if (!propType) {
                continue;
            }
            if (![propType isEqualToString:@"NSString"]) {
                continue;
            }
            NSString *value = [attributeDict objectForKey:key];
            [curObj setValue:value forKey:key];
        }
        
        [self.array addObject:curObj];
    } else {
        self.tmpStr = [[NSMutableString alloc] init];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    [self.tmpStr appendString:string];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:self.mainElementName]){
        self.tmpStr = nil;
    } else {
        NSDictionary *clsPropDic = [RMMapper propertiesForClass:self.cls];
        NSString *propType = [clsPropDic objectForKey:elementName];
        if (!propType) {
            return;
        }
        if (![propType isEqualToString:@"NSString"]) {
            return;
        }
        [self.tmpObj setValue:self.tmpStr forKey:elementName];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.tmpObj, self.array);
}

@end
