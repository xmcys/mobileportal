//
//  main.m
//  MobilePortal
//
//  Created by Chenhc on 15/3/3.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MPAppDelegate class]));
    }
}
