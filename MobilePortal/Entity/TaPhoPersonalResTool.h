//
//  TaPhoPersonalResTool.h
//  MobilePortal
//
//  Created by Chenhc on 15/3/12.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TaPhoPersonalRes.h"

@class LauncherItem;

@interface TaPhoPersonalResTool : NSObject

// TaPhoPersonalRes数组对象转为我的应用的图标对象数组（多维）
+ (void)resArrayToLauncherPages:(NSMutableArray *)resArray complete:(void(^)(NSMutableArray *))callback;
// 我的应用的图标对象数组转成TaPhoPersonalRes数组对象（一维）
+ (void)launcherPagesToResArray:(NSMutableArray *)pages complete:(void(^)(NSMutableArray *))callback;

+ (TaPhoPersonalRes *)launcherItemToTaPhoPersonalRes:(LauncherItem *)item;
+ (LauncherItem *)taPhoPersonalResToLauncherItem:(TaPhoPersonalRes *)res;

@end
