//
//  BiPerson.m
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-28.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "BiPerson.h"


@interface BiPerson ()

@end

@implementation BiPerson

- (id)init
{
    if (self = [super init]) {
        self.userName = @"";
        self.password = @"";
        self.personCode = @"";
        self.personName = @"";
        self.personPhoneNumber = @"";
        self.personMb = @"";
        self.sessionCode = @"";
        self.deptCode = @"";
        self.deptName = @"";
        self.duty = @"";
        self.dutyName = @"";
        self.orgCode = @"";
        self.orgName = @"";
        self.sex = @"";
    }
    return self;
}

- (id)initWithJson:(NSString *)json
{
    if (self = [super init]) {
        NSError * error;
        NSDictionary * dic = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&error];
//        self.userName = [dic objectForKey:@"userName"];
//        self.password = [dic objectForKey:@"password"];
        self.personCode = [dic objectForKey:@"personCode"];
        self.personName = [dic objectForKey:@"personName"];
        self.sessionCode = [dic objectForKey:@"sessionCode"];
        self.deptCode = [dic objectForKey:@"deptCode"];
        self.deptName = [dic objectForKey:@"deptName"];
        self.duty = [dic objectForKey:@"duty"];
        self.dutyName = [dic objectForKey:@"dutyName"];
        self.orgCode = [dic objectForKey:@"orgCode"];
        self.orgName = [dic objectForKey:@"orgName"];
        self.personMb = [dic objectForKey:@"personMb"];
        self.personPhoneNumber = [dic objectForKey:@"personPhoneNumber"];
        self.sex = [dic objectForKey:@"sex"];
    }
    return self;
    
}

- (BOOL)isLogin
{
//    if (self != nil && self.personCode.length != 0) {
//        return YES;
//    } else {
//        return NO;
//    }
    return self.sessionLogin;
}

- (NSString *)toJson
{
    NSMutableDictionary * dic = [[NSMutableDictionary alloc] init];
//    [dic setObject:self.userName forKey:@"userName"];
//    [dic setObject:self.password forKey:@"password"];
    [dic setObject:self.personCode forKey:@"personCode"];
    [dic setObject:self.personName forKey:@"personName"];
    [dic setObject:self.sessionCode forKey:@"sessionCode"];
    [dic setObject:self.deptCode forKey:@"deptCode"];
    [dic setObject:self.deptName forKey:@"deptName"];
    [dic setObject:self.duty forKey:@"duty"];
    [dic setObject:self.dutyName forKey:@"dutyName"];
    [dic setObject:self.orgCode forKey:@"orgCode"];
    [dic setObject:self.orgName forKey:@"orgName"];
    [dic setObject:self.personMb forKey:@"personMb"];
    [dic setObject:self.personPhoneNumber forKey:@"personPhoneNumber"];
    [dic setObject:self.sex forKey:@"sex"];
    
    NSError * error;
    NSData * data = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&error];
    NSString * json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return json;
}

@end
