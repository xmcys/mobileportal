//
//  TaPhoPersonalRes.h
//  MobilePortal
//
//  Created by Chenhc on 15/3/6.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+RMArchivable.h"

@interface TaPhoPersonalRes : NSObject

@property (nonatomic, strong) NSString * id; // 主键
@property (nonatomic, strong) NSString * personCode; // 人员代码
@property (nonatomic, strong) NSString * dutyCode; // 岗位代码
@property (nonatomic, strong) NSString * deptCode; // 部门代码
@property (nonatomic, strong) NSString * lastModityTime; // 更新时间

@property (nonatomic, strong) NSString * resId; // 资源ID
@property (nonatomic, strong) NSString * resName; // 资源名称
@property (nonatomic, strong) NSString * pageId; // 页面ID
@property (nonatomic, strong) NSString * appOrder; // 图标位置
@property (nonatomic, strong) NSString * upId; // 上级资源ID
@property (nonatomic, strong) NSNumber * funType;  // 功能类型
@property (nonatomic, strong) NSString * resType; // 资源类型 1 顶级资源 2 文件夹 3 子集资源 4 窗口组件
@property (nonatomic, strong) NSNumber * downFlag; // 更新标记

@property (nonatomic, strong) NSString * resOpenType; // 资源打开类型
@property (nonatomic, strong) NSString * resUrl; // 资源打开类型对应的名称
@property (nonatomic, strong) NSString * picPath; // 图片路径
@property (nonatomic, strong) NSString * appVer;           // 版本号
@property (nonatomic, strong) NSString * lastModifyTime;   // 最后修改时间


- (BOOL)isFolder;

@end
