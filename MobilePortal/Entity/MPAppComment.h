//
//  MPAppComment.h
//  MobilePortal
//
//  Created by 张斌 on 15/3/6.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MPAppComment : NSObject

@property (nonatomic, strong) NSString *personName;       // 评论人
@property (nonatomic, strong) NSString *content;          // 内容
@property (nonatomic, strong) NSString *operDate;         // 日期
@property (nonatomic, assign) BOOL isExpand;

@end
