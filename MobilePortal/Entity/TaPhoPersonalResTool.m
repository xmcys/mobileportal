//
//  TaPhoPersonalResTool.m
//  MobilePortal
//
//  Created by Chenhc on 15/3/12.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "TaPhoPersonalResTool.h"
#import "LauncherItem.h"

static TaPhoPersonalResTool * RES_TOOL_INSTANCE;

@interface TaPhoPersonalResTool ()

@property (nonatomic, strong) void (^callback)(NSMutableArray *);
@property (nonatomic, strong) NSMutableArray * launcherPages;
@property (nonatomic, strong) NSMutableArray * resArray;

- (void)startParseLauncherPages;
- (void)parsingLauncherPages;
- (void)endParseLauncherPages;
- (void)startParseResArray;
- (void)parsingResArray:(NSMutableArray *)pages;
- (void)endParseResArray;

@end

@implementation TaPhoPersonalResTool

- (void)startParseLauncherPages
{
    [self.launcherPages removeAllObjects];
    [NSThread detachNewThreadSelector:@selector(parsingLauncherPages) toTarget:self withObject:nil];
}

- (void)parsingLauncherPages
{
    NSMutableDictionary * folderDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary * pageDict = [[NSMutableDictionary alloc] init];
    
    for (TaPhoPersonalRes * res in self.resArray) {
        LauncherItem * item = [[LauncherItem alloc] initWithFrame:CGRectMake(0, 0, LAUNCHER_ITEM_WIDTH, LAUNCHER_ITEM_HEIGHT) isFolder:[res isFolder]];
        item.title = res.resName;
        item.pageNum = [res.pageId intValue];
        item.index = [res.appOrder intValue];
        item.itemId = res.resId;
        item.folderId = res.upId;
        item.imageUrl = res.picPath;
        item.newFlag = [res.downFlag intValue];
        item.actionType = res.resOpenType;
        item.actionName = res.resUrl;
        if (item.folderId != nil && item.folderId.length != 0) {
            NSMutableDictionary * folderPagesDict = [folderDict objectForKey:item.folderId];
            if (folderPagesDict == nil) {
                folderPagesDict = [[NSMutableDictionary alloc] init];
            }
            NSMutableArray * folderPage = [folderPagesDict objectForKey:res.pageId];
            if (folderPage == nil) {
                folderPage = [[NSMutableArray alloc] init];
            }
            [folderPage addObject:item];
            [folderPagesDict setObject:folderPage forKey:res.pageId];
            [folderDict setObject:folderPagesDict forKey:item.folderId];
        } else {
            NSMutableArray * page = [pageDict objectForKey:res.pageId];
            if (page == nil) {
                page = [[NSMutableArray alloc] init];
            }
            [page addObject:item];
            [pageDict setObject:page forKey:res.pageId];
        }
    }
    
    NSArray * keys = [pageDict allKeys];
    keys = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        if ([obj1 integerValue] > [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        if ([obj1 integerValue] < [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
    for (int i = 0; i < keys.count; i++) {
        NSString * key = [keys objectAtIndex:i];
        NSMutableArray * page = [pageDict objectForKey:key];
        for (int j = 0; j < page.count; j++) {
            LauncherItem * item = [page objectAtIndex:j];
            if (item.isFolder) {
                [item.folderItems removeAllObjects];
                NSMutableDictionary * folderPagesDict = [folderDict objectForKey:item.itemId];
                NSArray * folderPageKeys = [folderPagesDict allKeys];
                folderPageKeys = [folderPageKeys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                    if ([obj1 integerValue] > [obj2 integerValue]) {
                        return (NSComparisonResult)NSOrderedDescending;
                    }
                    if ([obj1 integerValue] < [obj2 integerValue]) {
                        return (NSComparisonResult)NSOrderedAscending;
                    }
                    return (NSComparisonResult)NSOrderedSame;
                }];
                for (int k = 0; k < folderPageKeys.count; k++) {
                    NSString * folderPageKey = [folderPageKeys objectAtIndex:k];
                    NSMutableArray * folderPage = [folderPagesDict objectForKey:folderPageKey];
                    for (int l = 0; l < folderPage.count; l++) {
                        LauncherItem * itemInFolder = [folderPage objectAtIndex:l];
                        itemInFolder.pageNum = k;
                    }
                    [item.folderItems addObject:folderPage];
                }
            }
            item.pageNum = i;
        }
        [self.launcherPages addObject:page];
    }
    [pageDict removeAllObjects];
    pageDict = nil;
    [folderDict removeAllObjects];
    folderDict = nil;
    [self performSelectorOnMainThread:@selector(endParseLauncherPages) withObject:nil waitUntilDone:NO];
}

- (void)endParseLauncherPages
{
    self.callback(self.launcherPages);
}

- (void)startParseResArray
{
    [self.resArray removeAllObjects];
    [NSThread detachNewThreadSelector:@selector(parsingResArray:) toTarget:self withObject:self.launcherPages];
}

- (void)parsingResArray:(NSMutableArray *)pages
{
    NSDateFormatter * fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate * now = [NSDate date];
    for (int i = 0; i < pages.count; i++) {
        NSMutableArray * page = [pages objectAtIndex:i];
        for (int j = 0; j < page.count; j++) {
            LauncherItem * item = [page objectAtIndex:j];
            TaPhoPersonalRes * res = [[TaPhoPersonalRes alloc] init];
            res.personCode = @"";
            res.deptCode = @"";
            res.dutyCode = @"";
            res.lastModityTime = [fmt stringFromDate:now];
            res.resId = item.itemId;
            res.resName = item.title;
            res.pageId = [NSString stringWithFormat:@"%lu", (unsigned long)item.pageNum];
            res.appOrder = [NSString stringWithFormat:@"%lu", (unsigned long)item.index];
            res.upId = nil;
            res.resType = @"1";
            res.downFlag = [NSNumber numberWithInteger:item.newFlag];
            res.resOpenType = item.actionType;
            res.resUrl = item.actionName;
            res.picPath = item.imageUrl;
            
            if (item.isFolder) {
                res.resType = @"2";
                [self parsingResArray:item.folderItems];
            }
            if (item.folderId != nil && item.folderId.length != 0) {
                res.upId = item.folderId;
                res.resType = @"3";
            }
            [self.resArray addObject:res];
        }
    }
    
    if (pages == self.launcherPages) {
        [self performSelectorOnMainThread:@selector(endParseResArray) withObject:nil waitUntilDone:YES];
    }
}

- (void)endParseResArray
{
    self.callback(self.resArray);
}

+ (void)resArrayToLauncherPages:(NSMutableArray *)resArray complete:(void (^)(NSMutableArray *))callback
{
    if (RES_TOOL_INSTANCE == nil) {
        RES_TOOL_INSTANCE = [[TaPhoPersonalResTool alloc] init];
        RES_TOOL_INSTANCE.launcherPages = [[NSMutableArray alloc] init];
    }
    RES_TOOL_INSTANCE.callback = callback;
    RES_TOOL_INSTANCE.resArray = resArray;
    [RES_TOOL_INSTANCE startParseLauncherPages];
}

+ (void)launcherPagesToResArray:(NSMutableArray *)pages complete:(void (^)(NSMutableArray *))callback
{
    if (RES_TOOL_INSTANCE == nil) {
        RES_TOOL_INSTANCE = [[TaPhoPersonalResTool alloc] init];
        RES_TOOL_INSTANCE.resArray = [[NSMutableArray alloc] init];
    }
    RES_TOOL_INSTANCE.callback = callback;
    RES_TOOL_INSTANCE.launcherPages = pages;
    [RES_TOOL_INSTANCE startParseResArray];
}

+ (TaPhoPersonalRes *)launcherItemToTaPhoPersonalRes:(LauncherItem *)item
{
    NSDateFormatter * fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate * now = [NSDate date];
    
    TaPhoPersonalRes * res = [[TaPhoPersonalRes alloc] init];
    res.personCode = @"";
    res.deptCode = @"";
    res.dutyCode = @"";
    res.lastModityTime = [fmt stringFromDate:now];
    res.resId = item.itemId;
    res.resName = item.title;
    res.pageId = [NSString stringWithFormat:@"%lu", (unsigned long)item.pageNum];
    res.appOrder = [NSString stringWithFormat:@"%lu", (unsigned long)item.index];
    res.upId = nil;
    res.resType = @"1";
    res.downFlag = [NSNumber numberWithInteger:item.newFlag];
    res.resOpenType = item.actionType;
    res.resUrl = item.actionName;
    res.picPath = item.imageUrl;
    
    if (item.isFolder) {
        res.resType = @"2";
    }
    if (item.folderId != nil && item.folderId.length != 0) {
        res.upId = item.folderId;
        res.resType = @"3";
    }
    
    return res;
}

+ (LauncherItem *)taPhoPersonalResToLauncherItem:(TaPhoPersonalRes *)res
{
    LauncherItem * item = [[LauncherItem alloc] initWithFrame:CGRectMake(0, 0, LAUNCHER_ITEM_WIDTH, LAUNCHER_ITEM_HEIGHT) isFolder:NO];
    item.title = res.resName;
    item.imageUrl = res.picPath;
    item.itemId = res.resId;
    return item;
}

@end
