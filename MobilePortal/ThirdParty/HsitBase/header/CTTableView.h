//
//  CTTableView.h  下拉刷新、拖动加载
//  ChyoTools
//
//  请在使用本类的视图控制器中实现UITableView Delegate、UITableView DataSource、UIScrollView Delegate及CTTableView Delegate四种协议
//  并将ctTableViewDidScroll及ctTableViewDidEndDragging分别写在相应方法中，如：
//  #pragma mark -
//  #pragma mark UIScrollView Delegate
//  - (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
//  {
//      [ctTableView ctTableViewDidEndDragging];
//  }
//
//  - (void)scrollViewDidScroll:(UIScrollView *)scrollView
//  {
//      [ctTableView ctTableViewDidScroll];
//  }
//
//  Created & Modify by 陈 宏超 on 14-1-2 VERSION 2.0.
//  Copyright (c) 2011-2099年 Chyo. All rights reserved.
//

#import <UIKit/UIKit.h>

// 视图状态
typedef NS_ENUM(NSInteger, CTTableViewStatus)
{
    CTTableViewStatusNormal,    // 0 正常
    CTTableViewStatusPullDown,  // 1 下拉
    CTTableViewStatusPullUp,    // 2 上拉
    CTTableViewStatusLoading,   // 3 加载中
    CTTableViewStatusAllLoaded  // 4 数据全部加载
};

@class CTTableView;

@protocol CTTableViewDelegate <NSObject>

@optional
// 将下拉刷新数据的代码写在此方法里，返回YES则视图维持在刷新状态
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView;
// 将上拉加载数据的代码写在此方法里，返回YES则视图维持在加载状态
- (BOOL)ctTableViewDidPullUpToLoad:(CTTableView *)ctTableView;

@end

@interface CTTableView : UITableView

@property (nonatomic) BOOL bgDefaultClear;           // 背景默认透明
@property (nonatomic) BOOL pullDownViewEnabled; // 设置是否使用下拉刷新视图
@property (nonatomic) BOOL pullUpViewEnabled;      // 设置是否使用上拉加载视图
@property (nonatomic) BOOL hidesPullUpViewWhenEmpty; // 设置是否在数据为空的时候自动隐藏“上拉加载视图”
@property (nonatomic) CTTableViewStatus status;      // 视图状态
@property (nonatomic, weak) id<CTTableViewDelegate> ctTableViewDelegate;  // 委托

// 请将此方法写在scrollViewDidScroll方法中
- (void)ctTableViewDidScroll;
// 请将此方法写在scrollViewDidEndDragging方法中
- (void)ctTableViewDidEndDragging;
// 标识数据已全部加载（此时上拖不会再执行相关方法）
- (void)dataAllLoaded;
// 设置最后更新时间 yyyy-MM-dd HH:mm
- (void)setLastestUpdatedTime:(NSString *)time;
// 加载数据
- (void)showPullDownView;
// 调用reloadData方法即可恢复视图

@end
