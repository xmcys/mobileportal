//
//  CTViewController.h
//  ChyoTools
//
//  Created by 陈 宏超 on 13-12-31.
//  Copyright (c) 2013年 Chyo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CTIndicateView.h"
#import "CTURLConnection.h"


@interface CTViewController : UIViewController <CTURLConnectionDelegate>


@property (nonatomic, strong) CTIndicateView * indicator;

@property (nonatomic) float bottomOffset;
// 设置状态栏是否隐藏
@property (nonatomic) BOOL stautsBarHidden;
// 设置Tab栏元素
- (void)setTabBarItemWithTitle:(NSString *)title titleColor:(UIColor *)color titleSelectedColor:(UIColor *)selectedColor image:(UIImage *)image selectedImage:(UIImage *)selectedImage;
@end
