//
//  异步加载图片
//  Created by a_超 on 14-7-22.
//  Copyright (c) 2014年 a_超. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AsyncImageView;

@protocol ECImageViewDelegate <NSObject>

@optional
// 图片成功加载后
- (void)asyncImageViewDidImageLoaded:(AsyncImageView *)imageView;

@optional
// 图片点击事件
- (void)asyncImageViewDidTapped:(AsyncImageView *)imageView;

@end

typedef NS_ENUM(NSInteger, AsyncImageState){
    AsyncImageStateLoading = 200,
    AsyncImageStateSuccess,
    AsyncImageStateFailure
};

@interface AsyncImageView : UIImageView

@property (nonatomic, strong) NSString * imageId;    // 图片ID
@property (nonatomic, strong) NSString * imageName;  // 图片名称，默认采用URL进行特殊处理
@property (nonatomic, strong) NSString * imageUrl;   // 图片URL
@property (nonatomic, strong) NSString * actionLink; // 事件链接
@property (nonatomic) BOOL actionEnabled;            // 设置点击事件是否可用

@property (nonatomic) AsyncImageState imageState;       // 图片加载状态
@property (nonatomic, weak) id<ECImageViewDelegate> delegate;


@end
