//
//  TIIDatePicker.m
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-6-9.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIIDatePicker.h"
#import <QuartzCore/QuartzCore.h>

@interface TIIDatePicker () <UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) UIView * container;
@property (nonatomic, strong) UIPickerView * yearPicker;
@property (nonatomic, strong) UIPickerView * monthPicker;
@property (nonatomic, strong) UIPickerView * dayPicker;
@property (nonatomic) int components;
@property (nonatomic, strong) NSMutableArray * years;
@property (nonatomic, strong) NSMutableArray * months;
@property (nonatomic, strong) NSMutableArray * days;
@property (nonatomic, strong) NSString * dateFormatter;
@property (nonatomic, strong) UIWindow * window;     // 全局弹窗
@property (nonatomic) int selectedYear;
@property (nonatomic) int selectedMonth;
@property (nonatomic) int selectedDay;

- (int)maxDayInYear:(int)year withMonth:(int)month;
- (void)onButtonClicked:(UIButton *)btn;

@end

@implementation TIIDatePicker

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (id)initWithDateFormatter:(NSString *)dateFormatter
{
    self = [super initWithFrame:[UIScreen mainScreen].bounds];
    if (self) {
        self.dateFormatter = dateFormatter;
        
        self.container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 260, 300)];
        self.container.backgroundColor = [UIColor whiteColor];
        self.container.layer.cornerRadius = 5.0;
        self.container.backgroundColor = [UIColor whiteColor];
        self.container.layer.borderColor = [[UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1] CGColor];
        self.container.layer.borderWidth = 0.5;
        self.container.layer.shadowRadius = 1;
        self.container.layer.shadowOffset = CGSizeMake(1, 1);
        self.container.center = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);
        [self addSubview:self.container];
        
        _components = 0;
        if ([dateFormatter rangeOfString:@"y"].location != NSNotFound ||
            [dateFormatter rangeOfString:@"Y"].location != NSNotFound) {
            _components++;
        }
        if ([dateFormatter rangeOfString:@"m"].location != NSNotFound ||
            [dateFormatter rangeOfString:@"M"].location != NSNotFound) {
            _components++;
        }
        if ([dateFormatter rangeOfString:@"d"].location != NSNotFound ||
            [dateFormatter rangeOfString:@"D"].location != NSNotFound) {
            _components++;
        }
        if (_components == 0) {
            _components = 1;
        }
        
        NSCalendar * calendar = [NSCalendar currentCalendar];
        [calendar setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh"]];
        
        NSInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
        NSDateComponents * comps = [calendar components:unitFlags fromDate:[NSDate date]];
        self.selectedYear = [comps year];
        self.selectedMonth = [comps month];
        self.selectedDay = [comps day];
        
        self.years = [[NSMutableArray alloc] init];
        for (int i = [comps year] + 10; i > 1900; i--) {
            [self.years addObject:[NSString stringWithFormat:@"%d", i]];
        }
        
        self.months = [[NSMutableArray alloc] init];
        for (int i = 1; i <= 12; i++) {
            [self.months addObject:[NSString stringWithFormat:@"%d", i]];
        }
        
        self.days = [[NSMutableArray alloc] init];
        int maxDay = [self maxDayInYear:[comps year] withMonth:[comps month]];
        for (int i = 1; i <= maxDay; i++) {
            [self.days addObject:[NSString stringWithFormat:@"%d", i]];
        }
        
        self.yearPicker = [[UIPickerView alloc] init];
        self.yearPicker.frame = CGRectMake(10, 10, (self.container.frame.size.width - 20) / self.components, self.yearPicker.frame.size.height);
        self.yearPicker.showsSelectionIndicator = YES;
        self.yearPicker.delegate = self;
        self.yearPicker.dataSource = self;
        [self.container addSubview:self.yearPicker];
        
        self.monthPicker = [[UIPickerView alloc] init];
        self.monthPicker.frame = CGRectMake(self.yearPicker.frame.origin.x + self.yearPicker.frame.size.width, 10, self.yearPicker.frame.size.width, self.yearPicker.frame.size.height);
        self.monthPicker.showsSelectionIndicator = YES;
        self.monthPicker.delegate = self;
        self.monthPicker.dataSource = self;
        [self.container addSubview:self.monthPicker];
        
        self.dayPicker = [[UIPickerView alloc] init];
        self.dayPicker.frame = CGRectMake(self.yearPicker.frame.origin.x + self.yearPicker.frame.size.width * 2, 10, self.yearPicker.frame.size.width, self.yearPicker.frame.size.height);
        self.dayPicker.showsSelectionIndicator = YES;
        self.dayPicker.delegate = self;
        self.dayPicker.dataSource = self;
        [self.container addSubview:self.dayPicker];
        
        UIButton * btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
        btnCancel.frame = CGRectMake(10, 10 + self.yearPicker.frame.size.height + 10, (self.container.frame.size.width - 30) / 2, 36);
        btnCancel.backgroundColor = [UIColor clearColor];
        [btnCancel setBackgroundImage:[[UIImage imageNamed:@"TIIBtnRed"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 9, 14, 9)] forState:UIControlStateNormal];
        [btnCancel setBackgroundImage:[[UIImage imageNamed:@"TIIBtnRedPressed"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 9, 14, 9)] forState:UIControlStateHighlighted];
        btnCancel.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        [btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnCancel setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        [btnCancel setTitle:@"取消" forState:UIControlStateNormal];
        [btnCancel setTitle:@"取消" forState:UIControlStateHighlighted];
        [btnCancel addTarget:self action:@selector(onButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnCancel.tag = 1;
        [self.container addSubview:btnCancel];
        
        UIButton * btnConfirm = [UIButton buttonWithType:UIButtonTypeCustom];
        btnConfirm.frame = CGRectMake(10 + btnCancel.frame.size.width + 10, btnCancel.frame.origin.y, btnCancel.frame.size.width, 36);
        btnConfirm.backgroundColor = [UIColor clearColor];
        [btnConfirm setBackgroundImage:[[UIImage imageNamed:@"TIIBtnRed"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 9, 14, 9)] forState:UIControlStateNormal];
        [btnConfirm setBackgroundImage:[[UIImage imageNamed:@"TIIBtnRedPressed"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 9, 14, 9)] forState:UIControlStateHighlighted];
        btnConfirm.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        [btnConfirm setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnConfirm setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        [btnConfirm setTitle:@"确定" forState:UIControlStateNormal];
        [btnConfirm setTitle:@"确定" forState:UIControlStateHighlighted];
        [btnConfirm addTarget:self action:@selector(onButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnConfirm.tag = 2;
        [self.container addSubview:btnConfirm];
        
        self.container.frame = CGRectMake(self.container.frame.origin.x, self.container.frame.origin.y, self.container.frame.size.width, btnConfirm.frame.origin.y + btnConfirm.frame.size.height + 10);
        
        [self.yearPicker selectRow:[[self.years objectAtIndex:0] integerValue] - self.selectedYear inComponent:0 animated:YES];
        [self.monthPicker selectRow:self.selectedMonth - 1 inComponent:0 animated:YES];
        [self.dayPicker selectRow:self.selectedDay - 1 inComponent:0 animated:YES];
        
        if ([self.dateFormatter rangeOfString:@"y"].location != NSNotFound
            || [self.dateFormatter rangeOfString:@"Y"].location != NSNotFound) {
            self.yearPicker.hidden = NO;
        } else {
            self.yearPicker.hidden = YES;
        }
        if ([self.dateFormatter rangeOfString:@"m"].location != NSNotFound
            || [self.dateFormatter rangeOfString:@"M"].location != NSNotFound) {
            self.monthPicker.hidden = NO;
        } else {
            self.monthPicker.hidden = YES;
        }
        if ([self.dateFormatter rangeOfString:@"d"].location != NSNotFound
            || [self.dateFormatter rangeOfString:@"D"].location != NSNotFound) {
            self.dayPicker.hidden = NO;
        } else {
            self.dayPicker.hidden = YES;
        }
        
        if (!self.yearPicker.hidden) {
            // 显示年
            if (!self.monthPicker.hidden) {
                // 显示月
                if (!self.dayPicker.hidden) {
                    // 年月日都显示，不做处理
                } else {
                    // 只显示年月，不用做处理
                }
            } else {
                // 不显示月
                if (!self.dayPicker.hidden) {
                    // 只显示年日，重新调整日控件位置
                    self.dayPicker.frame = self.monthPicker.frame;
                } else {
                    // 只显示年，不用做处理
                }
            }
        } else {
            // 不显示年
            if (!self.monthPicker.hidden) {
                // 显示月
                if (!self.dayPicker.hidden) {
                    // 显示月日，重新调整月日控件的位置
                    self.dayPicker.frame = self.monthPicker.frame;
                    self.monthPicker.frame = self.yearPicker.frame;
                } else {
                    // 只显示月
                    self.monthPicker.frame = CGRectMake(10, 10, self.container.frame.size.width - 20, self.monthPicker.frame.size.height);
                }
            } else {
                // 不显示月
                if (!self.dayPicker.hidden) {
                    // 只显示日
                    self.dayPicker.frame = CGRectMake(10, 10, self.container.frame.size.width - 20, self.dayPicker.frame.size.height);
                } else {
                    // 年月日都不显示，不做处理
                }
            }
        }
    }
    return self;
}

- (void)show
{
    if (self.window == nil) {
        self.window = [[UIWindow alloc] initWithFrame:self.bounds];
        self.window.windowLevel = UIWindowLevelAlert;
        self.window.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
        self.window.opaque = NO;
        self.window.alpha = 0.0;
        [self.window addSubview:self];
    }
    
    [self.layer removeAllAnimations];
    [UIView animateWithDuration:0.2 animations:^{
        self.window.alpha = 1.0;
    } completion:^(BOOL finished){
    }];
    
    CAKeyframeAnimation *popAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    popAnimation.duration = 0.4;
    popAnimation.values = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.01f, 0.01f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.02f, 1.02f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.97f, 0.97f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DIdentity]];
    popAnimation.keyTimes = @[@0.2f, @0.5f, @0.75f, @1.0f];
    popAnimation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.layer addAnimation:popAnimation forKey:nil];
    [self.window makeKeyAndVisible];
}

- (void)dismiss
{
    [self.layer removeAllAnimations];
    [UIView animateWithDuration:0.2 animations:^{
        self.window.alpha = 0.0;
        [self.window resignKeyWindow];
    } completion:^(BOOL finished){
        self.window = nil;
    }];
}

- (int)maxDayInYear:(int)year withMonth:(int)month
{
    NSDateFormatter * fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"yyyy-M-dd"];
    NSDate * date = [fmt dateFromString:[NSString stringWithFormat:@"%d-%d-01", year, month]];
    NSCalendar * calendar = [NSCalendar currentCalendar];
    [calendar setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh"]];
    NSRange dayRange = [calendar rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:date];
    return dayRange.length;
}

- (void)onButtonClicked:(UIButton *)btn
{
    if (btn.tag == 1) {
        [self.yearPicker selectRow:[[self.years objectAtIndex:0] integerValue] - self.selectedYear inComponent:0 animated:YES];
        [self.monthPicker selectRow:self.selectedMonth - 1 inComponent:0 animated:YES];
        [self.dayPicker selectRow:self.selectedDay - 1 inComponent:0 animated:YES];
    } else {
        self.selectedYear = [[self.years objectAtIndex:[self.yearPicker selectedRowInComponent:0]] integerValue];
        self.selectedMonth = [[self.months objectAtIndex:[self.monthPicker selectedRowInComponent:0]] integerValue];
        self.selectedDay = [[self.days objectAtIndex:[self.dayPicker selectedRowInComponent:0]] integerValue];
        if ([self.delegate respondsToSelector:@selector(tiiDatePicker:didSelectDate:)]) {
            [self.delegate tiiDatePicker:self didSelectDate:[self selectedFormattedDate]];
        }
    }
    [self dismiss];
}

- (void)setCurrentDate:(NSString *)currentDate
{
    NSMutableString * str = [[NSMutableString alloc] init];
    NSMutableString * fmtDate = [[NSMutableString alloc] init];
    [str appendString:self.dateFormatter];
    [fmtDate appendString:currentDate];
    if ([str rangeOfString:@"y"].location == NSNotFound && [str rangeOfString:@"Y"].location == NSNotFound) {
        [str appendString:@"-yyyy"];
        [fmtDate appendString:[NSString stringWithFormat:@"-%d", self.selectedYear]];
    }
    if ([str rangeOfString:@"m"].location == NSNotFound && [str rangeOfString:@"M"].location == NSNotFound){
        [str appendString:@"-M"];
        [fmtDate appendString:[NSString stringWithFormat:@"-%d", self.selectedMonth]];
    }
    if ([str rangeOfString:@"d"].location == NSNotFound && [str rangeOfString:@"D"].location == NSNotFound){
        [str appendString:@"-d"];
        [fmtDate appendString:[NSString stringWithFormat:@"-%d", self.selectedDay]];
    }
    NSDateFormatter * fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:str];
    NSDate * date = [fmt dateFromString:fmtDate];
    NSCalendar * calendar = [NSCalendar currentCalendar];
    [calendar setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh"]];
    
    NSInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    NSDateComponents * comps = [calendar components:unitFlags fromDate:date];
    self.selectedYear = [comps year];
    self.selectedMonth = [comps month];
    self.selectedDay = [comps day];
    [self.yearPicker selectRow:[[self.years objectAtIndex:0] integerValue] - self.selectedYear inComponent:0 animated:YES];
    [self.monthPicker selectRow:self.selectedMonth - 1 inComponent:0 animated:YES];
    [self.dayPicker selectRow:self.selectedDay - 1 inComponent:0 animated:YES];
}

- (NSString *)selectedFormattedDate
{
    NSDateFormatter * fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"yyyy-M-d"];
    NSDate * date = [fmt dateFromString:[NSString stringWithFormat:@"%d-%d-%d", self.selectedYear, self.selectedMonth, self.selectedDay]];
    [fmt setDateFormat:self.dateFormatter];
    return [fmt stringFromDate:date];
}

#pragma mark -
#pragma mark UIPickerView DataSource & Delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView == self.yearPicker) {
        return self.years.count;
    } else if (pickerView == self.monthPicker){
        return self.months.count;
    } else {
        return self.days.count;
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 36;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 36)];
    label.font = [UIFont systemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.textAlignment = NSTextAlignmentCenter;
    
    if (pickerView == self.yearPicker) {
        label.text = [NSString stringWithFormat:@"%@年", [self.years objectAtIndex:row]];
    } else if (pickerView == self.monthPicker){
        label.text = [NSString stringWithFormat:@"%@月", [self.months objectAtIndex:row]];
    } else {
        label.text = [NSString stringWithFormat:@"%@日", [self.days objectAtIndex:row]];
    }
    
    return label;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView == self.yearPicker || pickerView == self.monthPicker) {
        [self.days removeAllObjects];
        int maxDay = [self maxDayInYear:[[self.years objectAtIndex:[self.yearPicker selectedRowInComponent:0]] integerValue] withMonth:[[self.months objectAtIndex:[self.monthPicker selectedRowInComponent:0]] integerValue]];
        for (int i = 1; i <= maxDay; i++) {
            [self.days addObject:[NSString stringWithFormat:@"%d", i]];
        }
        [self.dayPicker reloadAllComponents];
    }
}

@end
