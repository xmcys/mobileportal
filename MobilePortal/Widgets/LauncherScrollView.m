//
//  LauncherScrollView.m
//  iOSForFun
//
//  Created by a_超 on 15/2/11.
//  Copyright (c) 2015年 a_超. All rights reserved.
//

#import "LauncherScrollView.h"
#import "LauncherFolderView.h"

#define MOVEABLE_RANGE 0.25 // 拖动图标时，触发图标位移的系数，越大越敏感，不建议修改
#define MAX_PADDING_Y 15 // 图标最大垂直间距

// 图标中心点X
#define CENTER_X(paddingX, column, index, itemWidth, pageNum) ((fmod(index, column) + 1) * paddingX + (fmod(index, column) * 2 + 1) * itemWidth / 2 + self.frame.size.width * pageNum)
// 图标中心点Y
#define CENTER_Y(paddingY, column, index, itemHeight) ((index / column + 1) * paddingY + (index / column * 2 + 1) * itemHeight / 2)

@interface LauncherScrollView () <LauncherFolderDelegate, LauncherFolderViewDelegate, UIScrollViewDelegate>

@property (nonatomic) CGFloat paddingX; // 图标水平间距
@property (nonatomic) CGFloat paddingY; // 图标垂直间距
@property (nonatomic, strong) NSTimer * touchTimer; // 长按定时器
@property (nonatomic, strong) NSTimer * scrollToNextPageTimer; // 换页定时器
@property (nonatomic, weak) LauncherItem * draggingItem; // 当前拖动的图标
@property (nonatomic, strong) LauncherFolderView * folderView; // 文件夹视图（会被加在UIWindow容器上全屏展示）
@property (nonatomic, strong) UIPageControl * pageControl; // 页码指示器

// 更新每个图标的位置
- (void)updateFrames;
// 将某个图标合并到某个文件夹
- (void)mergerToFolder:(LauncherItem *)targetItem;
// 图标从文件夹中拖出
- (void)removeFromFolder;
// 处理页面图标超量的方法
- (void)pageOverflow:(int)pageIndex willAddNewItem:(BOOL)willAddNewItem;

@end

@implementation LauncherScrollView

- (instancetype)initWithFrame:(CGRect)frame withColumn:(NSUInteger)column
{
    if (self = [super initWithFrame:frame]) {
        _launcherState = LauncherStateEndEdit;
        _column = column > 5 ? 5 : column;
        _column = _column <= 0 ? 4 : _column;
        _paddingX = -1;
        _paddingY = -1;
        _maxNumPerPage = _column * _column;
        _canCreateFolder = YES;
//        self.clipsToBounds = NO; // 设置成NO后，图标拖动超过滚动视图的时候仍然可见，但是在i6及i6P会出现左右两侧的图标也显示了的问题，所以就不设置了
        
        self.pagingEnabled = YES;
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
        self.alwaysBounceHorizontal = YES;
        self.bounces = YES;
        self.backgroundColor = [UIColor clearColor];
        self.multipleTouchEnabled = NO;
        self.exclusiveTouch = YES; // 独占整个touch事件
        self.delegate = self;
        self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, frame.size.height - 25, frame.size.width, 36)];
        self.pageControl.pageIndicatorTintColor = [UIColor colorWithRed:229.0/255.0 green:220.0/255.0 blue:235.0/255.0 alpha:1];
        self.pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:175.0/255.0 green:136.0/255.0 blue:187.0/255.0 alpha:1];
    }
    return self;
}

- (void)layoutLauncherItems:(NSMutableArray *)array
{
    for (UIView * view in self.subviews) {
        [view removeFromSuperview];
    }
    
    self.pages = array;
    
    _maxNumPerPage = (int)(self.bounds.size.height / LAUNCHER_ITEM_HEIGHT) * _column;
    
    // 图标之间、图标与屏幕之间的水平、垂直间距
    _paddingX = (self.bounds.size.width - _column * LAUNCHER_ITEM_WIDTH) / (_column + 1);
    _paddingY = (self.bounds.size.height - (_maxNumPerPage / _column * LAUNCHER_ITEM_HEIGHT)) / (_maxNumPerPage / _column + 1);
    if (_paddingY > MAX_PADDING_Y) {
        _paddingY = MAX_PADDING_Y;
    }
    
    // 轮询查找页面图标数量超标的第一个页面
    int overflowIndex = -1;
    for (int i = 0; i < _pages.count; i++) {
        NSMutableArray * page = [_pages objectAtIndex:i];
        if (page.count > _maxNumPerPage) {
            overflowIndex = i;
            break;
        }
    }
    
    // 处理图标超标
    if (overflowIndex != -1) {
        [self pageOverflow:overflowIndex willAddNewItem:NO];
    }
    
    self.pageControl.numberOfPages = _pages.count;
    self.pageControl.currentPage = 0;
    
    for (NSArray * page in _pages) {
        for (LauncherItem * item in page) {
            if (item.isFolder) {
                item.folderDelegate = self;
            }
            item.center = CGPointMake(CENTER_X(_paddingX, _column, item.index, LAUNCHER_ITEM_WIDTH, item.pageNum), CENTER_Y(_paddingY, _column, item.index, LAUNCHER_ITEM_HEIGHT));
            [item addTarget:self action:@selector(touchDown:) forControlEvents:UIControlEventTouchDown];
            [item addTarget:self action:@selector(touchUp:) forControlEvents:UIControlEventTouchUpInside];
            [item addTarget:self action:@selector(touchUp:) forControlEvents:UIControlEventTouchDragOutside];
            [item addTarget:self action:@selector(touchUp:) forControlEvents:UIControlEventTouchCancel];
            
            [self addSubview:item];
        }
    }
    
    self.contentSize = CGSizeMake(self.bounds.size.width * _pages.count, self.contentSize.height);
    self.contentOffset = CGPointZero;
    
    [self addSubview:self.pageControl];
}

- (void)updateFrames
{
    [UIView animateWithDuration:0.2 animations:^{
        for (NSArray * page in _pages) {
            for (LauncherItem * moveItem in page) {
                if (moveItem != self.draggingItem) {
                    moveItem.center = CGPointMake(CENTER_X(_paddingX, _column, moveItem.index, LAUNCHER_ITEM_WIDTH, moveItem.pageNum), CENTER_Y(_paddingY, _column, moveItem.index, LAUNCHER_ITEM_HEIGHT));
                }
            }
        }
    }];
    
    self.contentSize = CGSizeMake(self.bounds.size.width * _pages.count, self.contentSize.height);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    [[self nextResponder] touchesBegan:touches withEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
    [[self nextResponder] touchesMoved:touches withEvent:event];
    if (_launcherState == LauncherStateBeganEdit && _draggingItem != nil) {
        UITouch * touch = [touches anyObject];
        CGPoint point = [touch locationInView:self];
        // 图标跟着手指移动
        _draggingItem.center = point;
        _draggingItem.launcherState = LauncherStateMoving;
        
        int xOffsetInPage = fmod(point.x, self.bounds.size.width);
        
        // 图标在页面中间移动时
        if (self.scrollToNextPageTimer != nil && xOffsetInPage > 15 && xOffsetInPage < self.bounds.size.width - 15) {
            // 取消翻页定时器
            [self.scrollToNextPageTimer invalidate];
            self.scrollToNextPageTimer = nil;
        }
        
        if ((point.x > self.bounds.size.width - 15) && (point.x < self.bounds.size.width * _pages.count - 15)) {
            // 当图标被移动到边缘时，触发翻页定时器
            if (self.scrollToNextPageTimer == nil) {
                if (point.x - self.contentOffset.x > self.bounds.size.width - 15) {
                    self.scrollToNextPageTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(scrollToNextPage:) userInfo:@"NEXT" repeats:NO];
                    
                } else if (point.x - self.contentOffset.x < 15){
                    self.scrollToNextPageTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(scrollToNextPage:) userInfo:@"PREVIOUS" repeats:NO];
                }
                
            }
        }
        
        // 如果翻页定时器被触发了，就不继续做位移判断
        if (self.scrollToNextPageTimer != nil) {
            return;
        }
        
        // 计算横向位置（不会四舍五入，所以至少要移动到中心点才会变化）
        int xColumn = (fmod(point.x, self.bounds.size.width) - _paddingX -  LAUNCHER_ITEM_WIDTH / 2 ) / (_paddingX + 2 * LAUNCHER_ITEM_WIDTH / 2);
        // 计算纵向位置（不会四舍五入，所以至少要移动到中心点才会变化）
        int yRow = (point.y - _paddingY - LAUNCHER_ITEM_HEIGHT / 2) / (_paddingY + 2 * LAUNCHER_ITEM_HEIGHT / 2);
        // 新的位置
        int targetIndex = yRow * (int)_column + xColumn;
        
        // 以拖拽图标中心点为原点，向左向右移动的百分比（没有乘以100%）
        double xPercent = (fmod(point.x, self.bounds.size.width) - _paddingX -  LAUNCHER_ITEM_WIDTH / 2 ) / (_paddingX + 2 * LAUNCHER_ITEM_WIDTH / 2) - fmod(_draggingItem.index, _column);
        // 以拖拽图标中心点为原点，向上向下移动的百分比（没有乘以100%）
        double yPercent = (point.y - _paddingY - LAUNCHER_ITEM_HEIGHT / 2) / (_paddingY + 2 * LAUNCHER_ITEM_HEIGHT / 2) - _draggingItem.index / _column;
        
        // 设置Y值图标容差，当移动的坐标Y在图标1/6 - 5/6的位置时，才发生位置改变
        // 设计Y值图标容差是因为，如果垂直间距设计比较大时，图标移动到间距空白位置，不希望发生位置改变
        int rangeMinY = CENTER_Y(_paddingY, _column, targetIndex, LAUNCHER_ITEM_HEIGHT) - LAUNCHER_ITEM_HEIGHT / 3;
        int rangeMaxY = CENTER_Y(_paddingY, _column, targetIndex, LAUNCHER_ITEM_HEIGHT) + LAUNCHER_ITEM_HEIGHT / 3;
        
        
        // 如果新的位置与原先的位置不同，并且y值在新位置的Y轴图标容差范围，并且（x轴方向绝对位移了1.x或者y轴方向绝对位移了0.x）才发生位置交换 （这里的绝对位移指百分比）
        if (targetIndex != _draggingItem.index && point.y > rangeMinY && point.y < rangeMaxY
            && (fabs(xPercent) > 1 + MOVEABLE_RANGE || fabs(yPercent) > 1 - MOVEABLE_RANGE)) {
            // Y轴的绝对位移低于0.x，说明是在水平线上活动
            if (fabs(yPercent) < 1 - MOVEABLE_RANGE) {
                // 由于xColumn这个值不会四舍五入，因此向左移动时位置要加1
                // 但是这边有个特殊位置，就是每一行的第一个位置，这个位置要排除掉，否则会两个图标一起移动
                if (xPercent < -(1 + MOVEABLE_RANGE) && xOffsetInPage > _paddingX + LAUNCHER_ITEM_WIDTH / 2) {
                    targetIndex += 1;
                }
            }
            // Y轴的绝对位移大于0.x，说明在垂直线上发生了活动
            else {
                // 如果是坐落在目标图标的某个范围，位置不换位，这个设计是为了方便产生文件夹
                if (fabs(xPercent) < MOVEABLE_RANGE) {
                    return;
                }
                if ((fabs(xPercent) - (int)fabs(xPercent)) > 1 - MOVEABLE_RANGE) {
                    return;
                }
                // 如果是向上移动，由于yRow不会四舍五入，因此位置要加1，否则会变成你希望放入的那个位置的前一个图标跟着位移
                // 但是这拜年有个特殊位置，就是每一行的最后一个位置，这个位置要排除掉，否则移动到它右边的时候，它不会移动到下一行
                if (yPercent < -(1 - MOVEABLE_RANGE) && xOffsetInPage < (self.bounds.size.width - (_paddingX + LAUNCHER_ITEM_WIDTH / 2))) {
                    targetIndex += 1;
                }
            }
            bool needUpdateFrames = NO;
            NSMutableArray * page = [_pages objectAtIndex:_draggingItem.pageNum];
            // 从本页移除，让图标在数组中的位置与对应的index保持一致
            [page removeObject:_draggingItem];
            for (int i = 0; i < page.count; i++) {
                LauncherItem * item = [page objectAtIndex:i];
                if (item != _draggingItem) {
                    // 如果新的位置大于原本的位置，则 原本位置 < 需要移动的图标的位置 < 新的位置 上的图标位置都要减一
                    if (targetIndex > _draggingItem.index && item.index > _draggingItem.index && item.index <= targetIndex) {
                        item.index--;
                        needUpdateFrames = YES;
                    }
                    // 如果新的位置
                    else if (targetIndex < _draggingItem.index && item.index < _draggingItem.index && item.index >= targetIndex){
                        item.index++;
                        needUpdateFrames = YES;
                    }
                }
            }
            // 重新加入本页
            if (targetIndex < page.count) {
                [page insertObject:_draggingItem atIndex:targetIndex];
            } else {
                [page addObject:_draggingItem];
            }
            
            // 更新位移
            if (needUpdateFrames) {
                _draggingItem.index = targetIndex;
                [self updateFrames];
            }
            
            
        }
    }
}

- (void)pageOverflow:(int)pageIndex willAddNewItem:(BOOL)willAddNewItem
{
    NSMutableArray * newPage = [self.pages objectAtIndex:pageIndex];
    if (pageIndex == self.pages.count - 1) {
        [self.pages addObject:[[NSMutableArray alloc] init]];
        self.contentSize = CGSizeMake(self.bounds.size.width * self.pages.count, self.contentSize.height);
    }
    NSMutableArray * nextPage = [self.pages objectAtIndex:pageIndex + 1];
    NSMutableArray * overflowItems = [[NSMutableArray alloc] init];
    for (int i = 0; i < newPage.count; i++) {
        LauncherItem * item = [newPage objectAtIndex:i];
        if (willAddNewItem) {
            if (item.index >= _maxNumPerPage - 1) {
                [overflowItems addObject:item];
            }
        } else {
            if (item.index >= _maxNumPerPage) {
                [overflowItems addObject:item];
            }
        }
        
    }
    [newPage removeObjectsInArray:overflowItems];
    for (LauncherItem * item in nextPage) {
        item.index = item.index + overflowItems.count;
    }
    for (int i = 0; i < overflowItems.count; i++) {
        LauncherItem * item = [overflowItems objectAtIndex:i];
        item.pageNum++;
        item.index = i;
        [nextPage insertObject:item atIndex:item.index];
    }
    
    [overflowItems removeAllObjects];
    overflowItems = nil;
    
    if (nextPage.count > _maxNumPerPage) {
        [self pageOverflow:pageIndex + 1 willAddNewItem:NO];
    }
}

- (void)scrollToNextPage:(id)sender
{
    if ([self.scrollToNextPageTimer.userInfo isEqualToString:@"NEXT"]) {
        [UIView animateWithDuration:0.4 animations:^{
            self.contentOffset = CGPointMake(self.contentOffset.x + self.bounds.size.width, self.contentOffset.y);
        } completion:^(BOOL finished){
            NSMutableArray * oldPage = [self.pages objectAtIndex:self.draggingItem.pageNum];
            [oldPage removeObject:self.draggingItem];
            for (int i = 0; i < oldPage.count; i++) {
                LauncherItem * item = [oldPage objectAtIndex:i];
                if (item.index > self.draggingItem.index) {
                    item.index--;
                }
            }
            NSMutableArray * newPage = [self.pages objectAtIndex:self.draggingItem.pageNum+1];
            if (newPage.count == _maxNumPerPage) {
                [self pageOverflow:self.draggingItem.pageNum + 1 willAddNewItem:YES];
            }
            self.draggingItem.pageNum++;
            self.draggingItem.index = newPage.count;
            [newPage addObject:self.draggingItem];
            
            [self updateFrames];
        }];
        
    } else {
        if (self.contentOffset.x >= self.bounds.size.width) {
            [UIView animateWithDuration:0.4 animations:^{
                self.contentOffset = CGPointMake(self.contentOffset.x - self.bounds.size.width, self.contentOffset.y);
                
            } completion:^(BOOL finished){
                NSMutableArray * oldPage = [self.pages objectAtIndex:self.draggingItem.pageNum];
                [oldPage removeObject:self.draggingItem];
                for (int i = 0; i < oldPage.count; i++) {
                    LauncherItem * item = [oldPage objectAtIndex:i];
                    if (item.index > self.draggingItem.index) {
                        item.index--;
                    }
                }
                NSMutableArray * newPage = [self.pages objectAtIndex:self.draggingItem.pageNum-1];
                if (newPage.count == _maxNumPerPage) {
                    [self pageOverflow:self.draggingItem.pageNum - 1 willAddNewItem:YES];
                }
                self.draggingItem.pageNum--;
                self.draggingItem.index = newPage.count;
                [newPage addObject:self.draggingItem];
                [self updateFrames];
            }];
            
        }
        
    }
    [self.scrollToNextPageTimer invalidate];
    self.scrollToNextPageTimer = nil;
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesCancelled:touches withEvent:event];
    [[self nextResponder] touchesCancelled:touches withEvent:event];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (_launcherState == LauncherStateBeganEdit) {
        if (_draggingItem != nil) {
            _draggingItem.launcherState = _launcherState;
            
            CGPoint point = [[touches anyObject] locationInView:self];
            
            // 如果图标被拖动超出了上下屏幕边缘，移出文件夹
            if (!_canCreateFolder && (point.y + LAUNCHER_ITEM_HEIGHT / 2 < 0 || point.y - LAUNCHER_ITEM_HEIGHT / 2 > self.frame.size.height)) {
                [self removeFromFolder];
                return;
            }
            
            // 如果拖动的新位置超过了本页最大位置，这重置位置为本页最大位置
            NSMutableArray * page = [_pages objectAtIndex:_draggingItem.pageNum];
            if (_draggingItem.index >= page.count) {
                _draggingItem.index = page.count - 1;
                // 将图标放在数组最后
                [page removeObject:_draggingItem];
                [page addObject:_draggingItem];
                _draggingItem = nil;
                [self updateFrames];
            }
            // 如果允许创建文件夹，并且当前拖拽的对象不是文件夹
            else if (_canCreateFolder && !_draggingItem.isFolder){
                LauncherItem * targetItem = _draggingItem;
                for (LauncherItem * item in page) {
                    CGFloat minX = item.center.x - LAUNCHER_ITEM_WIDTH * MOVEABLE_RANGE;
                    CGFloat maxX = item.center.x + LAUNCHER_ITEM_WIDTH * MOVEABLE_RANGE;
                    CGFloat minY = item.center.y - LAUNCHER_ITEM_HEIGHT * MOVEABLE_RANGE;
                    CGFloat maxY = item.center.y + LAUNCHER_ITEM_HEIGHT * MOVEABLE_RANGE;
                    if (targetItem != item && minX < point.x && point.x < maxX && minY < point.y && point.y < maxY) {
                        // 如果图标坐落在目标图标中心点的某个范围内，则堆叠文件夹
                        targetItem = item;
                        break;
                    }
                }
                if (targetItem != _draggingItem) {
                    [self mergerToFolder:targetItem];
                }
            }
            _draggingItem = nil;
            [self updateFrames];
        }
        
        // 取消翻页定时器
        [self.scrollToNextPageTimer invalidate];
        self.scrollToNextPageTimer = nil;
    }
    
    
    [super touchesEnded:touches withEvent:event];
    [[self nextResponder] touchesEnded:touches withEvent:event];
}

- (void)mergerToFolder:(LauncherItem *)targetItem
{
    [_draggingItem touchUp:_draggingItem];
    
    // 保存拖拽图标的位置
    NSUInteger minIndex = _draggingItem.index;
    NSMutableArray * page = [_pages objectAtIndex:_draggingItem.pageNum];
    
    if (targetItem.isFolder) {
        // 如果是文件夹，则加入到文件夹末尾
        NSMutableArray * folderItemsPage = [targetItem.folderItems objectAtIndex:targetItem.folderItems.count - 1];
        _draggingItem.index = folderItemsPage.count;
        _draggingItem.folderId = targetItem.itemId;
        _draggingItem.pageNum = targetItem.folderItems.count - 1;
        
        [folderItemsPage addObject:_draggingItem];
        
        [page removeObject:_draggingItem];
        
        [_draggingItem removeFromSuperview];
    } else {
        
        
        // 创建文件夹
        LauncherItem * folder = [[LauncherItem alloc] initWithFrame:targetItem.frame isFolder:YES];
        folder.index = targetItem.index;
        folder.itemId = [NSString stringWithFormat:@"%@%@", _draggingItem.itemId, targetItem.itemId];
        folder.delegate = targetItem.delegate;
        folder.folderDelegate = self;
        folder.title = @"文件夹";
        folder.pageNum = targetItem.pageNum;
        [folder addTarget:self action:@selector(touchDown:) forControlEvents:UIControlEventTouchDown];
        [folder addTarget:self action:@selector(touchUp:) forControlEvents:UIControlEventTouchUpInside];
        [folder addTarget:self action:@selector(touchUp:) forControlEvents:UIControlEventTouchDragOutside];
        
        NSMutableArray * folderItemsPage = [[NSMutableArray alloc] init];
        // 把目标图标加入到文件夹
        targetItem.index = folderItemsPage.count;
        targetItem.folderId =folder.itemId;
        targetItem.pageNum = 0;
        [folderItemsPage addObject:targetItem];
        [page removeObject:targetItem];
        
        // 把拖拽图标加入到文件夹
        _draggingItem.index = folderItemsPage.count;
        _draggingItem.folderId = folder.itemId;
        _draggingItem.pageNum = 0;
        [folderItemsPage addObject:_draggingItem];
        [page removeObject:_draggingItem];
        
        [folder.folderItems addObject:folderItemsPage];
        
        // 把文件夹加入到数组
        [page addObject:folder];
        
        // 更改文件夹状态为可编辑
        [folder beganEdit:self];
        
        [targetItem removeFromSuperview];
        [_draggingItem removeFromSuperview];
        
        [self addSubview:folder];
        
        [self launcherFolderDidClick:folder];
    }
    // 将后续图标往前移动
    for (LauncherItem * item in page) {
        if (item.index > minIndex) {
            item.index--;
        }
    }
}

- (void)removeFromFolder
{
    if ([self.launcherScrollDelegate respondsToSelector:@selector(launcherScrollDidRemove:launcherItem:)]) {
        [_draggingItem removeFromSuperview];
        
        int dragginIndex = _draggingItem.index;
        int pageNum = _draggingItem.pageNum;
        
        NSMutableArray * page = [_pages objectAtIndex:pageNum];
        
        [page removeObject:_draggingItem];
        
        // 将本页的后续图标往前移动一位
        for (LauncherItem * item in page) {
            if (item.index > dragginIndex) {
                item.index--;
            }
        }
        
        // 取消拖拽对象的各种事件（将会在外层滚动视图重新加入事件）
        [_draggingItem removeTarget:self action:@selector(touchDown:) forControlEvents:UIControlEventTouchDown];
        [_draggingItem removeTarget:self action:@selector(touchUp:) forControlEvents:UIControlEventTouchUpInside];
        [_draggingItem removeTarget:self action:@selector(touchUp:) forControlEvents:UIControlEventTouchDragOutside];
        [_draggingItem removeTarget:self action:@selector(touchUp:) forControlEvents:UIControlEventTouchCancel];
        
        [self.launcherScrollDelegate launcherScrollDidRemove:self launcherItem:_draggingItem];
        
        [_draggingItem touchUp:_draggingItem];
        
        _draggingItem = nil;
        [self updateFrames];
    }
}

// 隐藏文件夹视图
- (void)removeFolderScroll
{
    if (_folderView != nil && _folderView.alpha != 0.0) {
        [UIView animateWithDuration:0.2 animations:^{
            _folderView.alpha = 0.0;
        }];
        
        [_folderView endEdit:self];
        
        // 消灭文件夹中的空白页
        NSMutableArray * emptyPages = [[NSMutableArray alloc] init];
        
        for (int i = 0; i < _folderView.folderItem.folderItems.count; i++) {
            NSArray * page = [_folderView.folderItem.folderItems objectAtIndex:i];
            if (page.count == 0) {
                [emptyPages addObject:page];
            }
        }
        
        [_folderView.folderItem.folderItems removeObjectsInArray:emptyPages];
        [emptyPages removeAllObjects];
        emptyPages = nil;
        
        // 给文件夹中每一页的图标重新赋予页码
        for (int i = 0; i < _folderView.folderItem.folderItems.count; i++) {
            NSArray * page = [_folderView.folderItem.folderItems objectAtIndex:i];
            for (LauncherItem * moveItem in page) {
                moveItem.pageNum = i;
            }
        }
        
        // 文件夹视图被打开时，滚动视图被设置成不可滚动，因此隐藏时要恢复
        self.scrollEnabled = YES;
        [_draggingItem touchUp:_draggingItem];
        _draggingItem = nil;
    }
    
}

- (void)deleteLauncherItem:(LauncherItem *)item
{
    if (item.isFolder) {
        return;
    }
    if (item.folderId == nil) {
        NSMutableArray * page = [self.pages objectAtIndex:item.pageNum];
        [page removeObject:item];
        [item removeFromSuperview];
        for (int i = 0; i < page.count; i++) {
            LauncherItem * temp = [page objectAtIndex:i];
            if (item.index < temp.index) {
                temp.index--;
            }
            
        }
        [self updateFrames];
    } else {
        NSMutableArray * page = [self.folderView.folderItem.folderItems objectAtIndex:item.pageNum];
        [page removeObject:item];
        [item removeFromSuperview];
        for (int i = 0; i < page.count; i++) {
            LauncherItem * temp = [page objectAtIndex:i];
            if (item.index < temp.index) {
                temp.index--;
            }
            
        }
        [self.folderView.launcherScroll updateFrames];
        
        // 计算文件夹中剩余的图标数量
        int count = 0;
        for (NSArray * folderPage in self.folderView.folderItem.folderItems) {
            count += folderPage.count;
        }
        
        // 如果剩余图标数量为0，删除文件夹（文件夹不带删除按钮）
        if (count == 0) {
            
            NSMutableArray * currentPage = [self.pages objectAtIndex:self.folderView.folderItem.pageNum];
            
            [currentPage removeObject:self.folderView.folderItem];
            for (LauncherItem * pageItem in currentPage) {
                if (pageItem.index > self.folderView.folderItem.index) {
                    pageItem.index--;
                }
            }
            [self.folderView.folderItem removeFromSuperview];
            [self removeFolderScroll];
            [self updateFrames];
        }
    }
}

- (void)addLauncherItem:(LauncherItem *)item
{
    // 寻找有空位的页
    BOOL isFindPlace = NO;
    for (int i = 0; i < _pages.count; i++) {
        NSMutableArray * page = [_pages objectAtIndex:i];
        if (page.count < self.maxNumPerPage) {
            isFindPlace = YES;
            item.pageNum = i;
            item.index = page.count;
            [page addObject:item];
            break;
        }
    }
    
    // 新增空白页
    if (!isFindPlace) {
        NSMutableArray * aNewPage = [[NSMutableArray alloc] init];
        item.pageNum = _pages.count;
        item.index = 0;
        [aNewPage addObject:item];
        [_pages addObject:aNewPage];
    }
    [self layoutLauncherItems:_pages];
    return;
}

#pragma mark -
#pragma mark Launcher Delegate
- (void)touchDown:(id)sender
{
    // 长按定时器
    if (_launcherState == LauncherStateEndEdit && self.touchTimer == nil) {
        self.touchTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(beganEdit:) userInfo:nil repeats:NO];
    }
    
    _draggingItem = sender;
    
//    // 长按某个图标后，自动新增一页（如果最后一页已经是空白页，就不新增）
//    if (_launcherState == LauncherStateBeganEdit && [[self.pages objectAtIndex:self.pages.count - 1] count] != 0) {
//        [self.pages addObject:[[NSMutableArray alloc] init]];
//        self.contentSize = CGSizeMake(self.bounds.size.width * self.pages.count, self.contentSize.height);
//        self.pageControl.numberOfPages = self.pages.count;
//    }
}

- (void)touchUp:(id)sender
{
    if (_draggingItem != nil) {
        [_draggingItem touchUp:_draggingItem];
    }
    
    [self.touchTimer invalidate];
    self.touchTimer = nil;
}

- (void)beganEdit:(id)sender
{
    _launcherState = LauncherStateBeganEdit;
    
    // 通知图标进入编辑状态
    for (UIView * view in self.subviews) {
        if ([view isKindOfClass:[LauncherItem class]]) {
            id<LauncherDelegate> launcherDelegate = (LauncherItem* )view;
            [launcherDelegate beganEdit:view];
        }
    }
    
    // 编辑状态时，按住某个图标后，自动新增一页（如果最后一页已经是空白页，就不新增）
    if (_launcherState == LauncherStateBeganEdit && [[self.pages objectAtIndex:self.pages.count - 1] count] != 0) {
        [self.pages addObject:[[NSMutableArray alloc] init]];
        self.contentSize = CGSizeMake(self.bounds.size.width * self.pages.count, self.contentSize.height);
        self.pageControl.numberOfPages = self.pages.count;
    }
    
    [_draggingItem touchDown:_draggingItem];
    
    // 通知委托对象进入编辑状态
    if ([self.launcherScrollDelegate respondsToSelector:@selector(launcherScrollDidBeginEdit:)]) {
        [self.launcherScrollDelegate launcherScrollDidBeginEdit:self];
    }
}

- (void)endEdit:(id)sender
{
    _launcherState = LauncherStateEndEdit;
    
    // 消灭空白页
    NSMutableArray * emptyPages = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < _pages.count; i++) {
        NSArray * page = [_pages objectAtIndex:i];
        if (page.count == 0) {
            [emptyPages addObject:page];
        }
    }
    
    [_pages removeObjectsInArray:emptyPages];
    [emptyPages removeAllObjects];
    emptyPages = nil;
    self.pageControl.numberOfPages = self.pages.count;
    
    // 重新设置页码
    for (int i = 0; i < _pages.count; i++) {
        NSArray * page = [_pages objectAtIndex:i];
        for (LauncherItem * moveItem in page) {
            moveItem.pageNum = i;
        }
    }
    
    [self updateFrames];
    
    // 通知图标退出编辑状态
    for (UIView * view in self.subviews) {
        if ([view isKindOfClass:[LauncherItem class]]) {
            id<LauncherDelegate> launcherDelegate = (LauncherItem* )view;
            [launcherDelegate endEdit:view];
        }
    }
    
    if ([self.launcherScrollDelegate respondsToSelector:@selector(launcherScrollDidEndEdit:)]) {
        [self.launcherScrollDelegate launcherScrollDidEndEdit:self];
    }
    
    // 通知文件夹退出编辑状态
    if (_folderView != nil) {
        [_folderView endEdit:sender];
    }
}

#pragma mark -
#pragma mark LauncherFolder Delegate
- (void)launcherFolderDidClick:(LauncherItem *)item
{
    // 长按文件夹图标并放开后，会触发点击事件，所以要排除
    if (_touchTimer != nil && _launcherState == LauncherStateBeganEdit) {
        return;
    }
    
    // 放开正在拖动的文件夹时，会触发点击事件，所以要排除
    if (_draggingItem != nil && _draggingItem.launcherState == LauncherStateMoving) {
        return;
    }
    
    // 初始化文件夹视图，并增加在UIWindow对象上，以实现全屏
    if (_folderView == nil) {
        UIWindow * window = [[UIApplication sharedApplication] keyWindow];
        
        _folderView = [[LauncherFolderView alloc] initWithFrame:window.bounds];
        _folderView.delegate = self;
        
        [_folderView addTarget:self action:@selector(removeFolderScroll) forControlEvents:UIControlEventTouchUpInside];
        
        [window addSubview:_folderView];
    }
    
    _folderView.folderItem = item;
    
    // 将文件夹中的图标设置为与当前状态一致
    if(_launcherState == LauncherStateBeganEdit)
    {
        [_folderView beganEdit:item];
    } else if(_launcherState == LauncherStateEndEdit){
        [_folderView endEdit:item];
    }
    
    // 父层滚动视图设置为不可滚动
    self.scrollEnabled = NO;
    
    _folderView.alpha = 0.0;
    [UIView animateWithDuration:0.2 animations:^{
        _folderView.alpha = 1.0;
    }];
}

#pragma mark -
#pragma mark LauncherFolderView Delegate
- (void)launcherFolderViewDidBeginEdit:(LauncherFolderView *)launcherFolder
{
    // 长按文件夹的图标时，会触发此委托事件，将本视图也设置成编辑状态
    if (_launcherState == LauncherStateEndEdit) {
        [self beganEdit:launcherFolder];
    }
    
}

- (void)launcherFolderViewDidRemove:(LauncherFolderView *)launcherFolder launcherItem:(LauncherItem *)item
{
    // 从文件夹拖出的图标
    LauncherItem * folder = _folderView.folderItem;
    
    // 判断是否可能造成页面图标数量超限（maxNumPerPage）
    NSMutableArray * page = [_pages objectAtIndex:folder.pageNum];
    if (page.count == _maxNumPerPage) {
        [self pageOverflow:folder.pageNum willAddNewItem:YES];
    }
    [page addObject:item];
    
    // 添加各种事件
    [item addTarget:self action:@selector(touchDown:) forControlEvents:UIControlEventTouchDown];
    [item addTarget:self action:@selector(touchUp:) forControlEvents:UIControlEventTouchUpInside];
    [item addTarget:self action:@selector(touchUp:) forControlEvents:UIControlEventTouchDragOutside];
    [item addTarget:self action:@selector(touchUp:) forControlEvents:UIControlEventTouchCancel];
    
    // 加入本页末尾
    item.index = page.count - 1;
    item.pageNum = folder.pageNum;
    item.center = CGPointMake(CENTER_X(_paddingX, _column, item.index, LAUNCHER_ITEM_WIDTH, item.pageNum), CENTER_Y(_paddingY, _column, item.index, LAUNCHER_ITEM_HEIGHT));
    [self addSubview:item];
    [self updateFrames];
    
    // 计算文件夹中剩余的图标数量
    int count = 0;
    for (NSArray * folderPage in folder.folderItems) {
        count += folderPage.count;
    }
    
    // 如果剩余图标数量为0，删除文件夹（文件夹不带删除按钮）
    if (count == 0) {
        [page removeObject:self.folderView.folderItem];
        for (LauncherItem * item in page) {
            if (item.index > self.folderView.folderItem.index) {
                item.index--;
            }
        }
        [self.folderView.folderItem removeFromSuperview];
        [self removeFolderScroll];
        item.index = page.count - 1;
        item.center = CGPointMake(CENTER_X(_paddingX, _column, item.index, LAUNCHER_ITEM_WIDTH, item.pageNum), CENTER_Y(_paddingY, _column, item.index, LAUNCHER_ITEM_HEIGHT));
        [self updateFrames];
    }
}


#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int offsetX = scrollView.contentOffset.x;
    int index = offsetX / scrollView.frame.size.width;
    self.pageControl.currentPage = index;
    self.pageControl.center = CGPointMake(scrollView.contentOffset.x + scrollView.frame.size.width / 2, self.pageControl.center.y);
}

@end
