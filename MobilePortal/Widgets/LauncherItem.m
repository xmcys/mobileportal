//
//  LauncherItem.m
//  iOSForFun
//
//  Created by a_超 on 15/2/11.
//  Copyright (c) 2015年 a_超. All rights reserved.
//

#import "LauncherItem.h"
#import "AsyncImageView.h"


@interface LauncherItem ()

@property (nonatomic, strong) AsyncImageView * imageView;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIButton * btnDel;
@property (nonatomic, strong) UIView * badge;

- (void)btnDelClick;
- (void)touchUpInSide;

@end

@implementation LauncherItem

- (instancetype)initWithFrame:(CGRect)frame isFolder:(BOOL)isFolder
{
    CGRect newframe = CGRectMake(0, 0, LAUNCHER_ITEM_WIDTH, LAUNCHER_ITEM_HEIGHT);
    
    CGPoint center = CGPointMake(frame.origin.x + frame.size.width / 2, frame.origin.y + frame.size.height / 2);
    
    if (self = [super initWithFrame:newframe]) {
        
        _launcherState = LauncherStateEndEdit;
        _isFolder = isFolder;
        _folderItems = [[NSMutableArray alloc] init];
        
        self.center = center;
        self.exclusiveTouch = YES; // 独占整个touch事件
        
        self.imageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(LAUNCHER_ITEM_PADDING, LAUNCHER_ITEM_PADDING, LAUNCHER_ITEM_IMAGE_WIDTH, LAUNCHER_ITEM_IMAGE_WIDTH)];
        if (_isFolder) {
            self.imageView.image = [UIImage imageNamed:@"icon_folder"];
        } else {
            self.imageView.image = [UIImage imageNamed:@"icon"];
        }
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        self.imageView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.imageView];
        
        self.btnDel = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btnDel.frame = CGRectMake(0, 0, 20, 20);
        self.btnDel.center = CGPointMake(self.imageView.frame.origin.x + self.imageView.frame.size.width, self.imageView.frame.origin.y);
        self.btnDel.backgroundColor = [UIColor clearColor];
        [self.btnDel setBackgroundImage:[UIImage imageNamed:@"icon_del"] forState:UIControlStateNormal];
        [self.btnDel setBackgroundImage:[UIImage imageNamed:@"icon_del"] forState:UIControlStateHighlighted];
        self.btnDel.hidden = YES;
        [self.btnDel addTarget:self action:@selector(btnDelClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.btnDel];
        
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.imageView.frame.origin.y + self.imageView.frame.size.height + LAUNCHER_ITEM_PADDING, self.frame.size.width, LAUNCHER_ITEM_TITLE_HEIGHT)];
        self.titleLabel.textColor = [UIColor colorWithRed:63.0/255.0 green:63.0/255.0 blue:63.0/255.0 alpha:1.0];
        self.titleLabel.font = [UIFont systemFontOfSize:12.0f];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.titleLabel];
        
        self.badge = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 8)];
        self.badge.backgroundColor = [UIColor redColor];
        self.badge.center = CGPointMake(self.imageView.frame.origin.x + self.imageView.frame.size.width, self.imageView.frame.origin.y + self.imageView.frame.size.height);
        self.badge.layer.cornerRadius = 4.0f;
        self.badge.hidden = YES;
        [self addSubview:self.badge];
        
        [self addTarget:self action:@selector(touchUpInSide) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return self;
}

- (void)setNewFlag:(int)newFlag
{
    _newFlag = newFlag;
    if (_newFlag == 0) {
        self.badge.hidden = YES;
    } else {
        self.badge.hidden = NO;
    }
}

- (void)setImageUrl:(NSString *)imageUrl
{
    _imageUrl = imageUrl;
    self.imageView.imageUrl = _imageUrl;
}

- (void)touchUpInSide
{
    // 图标点击事件，分为普通图标和文件夹图标两种
    if (!_isFolder && [_delegate respondsToSelector:@selector(launcherItemDidClick:)]) {
        [_delegate launcherItemDidClick:self];
    } else if (_isFolder && [_folderDelegate respondsToSelector:@selector(launcherFolderDidClick:)]){
        [_folderDelegate launcherFolderDidClick:self];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchDown:self];
    // 如果视图有绑定UIControlEvent，需要加入此行代码，否则不触发
    [super touchesBegan:touches withEvent:event];
    // 将触摸事件传递给父控件
    [[self nextResponder] touchesBegan:touches withEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
    [[self nextResponder] touchesMoved:touches withEvent:event];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchUp:self];
    [super touchesCancelled:touches withEvent:event];
    [[self nextResponder] touchesCancelled:touches withEvent:event];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchUp:self];
    [super touchesEnded:touches withEvent:event];
    [[self nextResponder] touchesEnded:touches withEvent:event];
}

- (void)setTitle:(NSString *)title
{
    _title = title;
    _titleLabel.text = _title;
}

- (void)setFolderId:(NSString *)folderId
{
    _folderId = folderId;
    if (folderId.length == 0) {
        _folderId = nil;
    }
}

- (void)btnDelClick
{
    // 删除按钮点击事件
    if ([_delegate respondsToSelector:@selector(launcherItemDidClickDelete:)]) {
        [_delegate launcherItemDidClickDelete:self];
    }
}

#pragma mark -
#pragma mark Launcher Delegate
- (void)touchUp:(id)sender
{
    // 视图形变比例1.0
    self.transform = CGAffineTransformMakeScale(1.0, 1.0);
    self.alpha = 1;
}

- (void)touchDown:(id)sender
{
    [self.superview bringSubviewToFront:self];
    
    if (_launcherState == LauncherStateEndEdit) {
        // 如果是正常状态下点击，视图缩小0.05
        self.transform = CGAffineTransformMakeScale(0.95, 0.95);
        self.alpha = 1;
    } else if (_launcherState == LauncherStateBeganEdit){
        // 如果是编辑状态下惦记，视图放大0.1
        self.transform = CGAffineTransformMakeScale(1.1, 1.1);
        self.alpha = 0.8;
    }
    
}

- (void)beganEdit:(id)sender
{
    _launcherState = LauncherStateBeganEdit;
    
    // 添加左右晃动动画
    CAKeyframeAnimation * shakeAnim = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.z"];
    
    shakeAnim.values = @[[NSNumber numberWithFloat:0],
                         [NSNumber numberWithFloat:-M_PI / 96 * 0.5],
                         [NSNumber numberWithFloat:-M_PI / 96],
                         [NSNumber numberWithFloat:-M_PI / 96 * 0.5],
                         [NSNumber numberWithFloat:0],
                         [NSNumber numberWithFloat:M_PI / 96 * 0.5],
                         [NSNumber numberWithFloat:M_PI / 96],
                         [NSNumber numberWithFloat:M_PI / 96 * 0.5],
                         [NSNumber numberWithFloat:0]];
    shakeAnim.duration = 0.2;
    shakeAnim.repeatCount = HUGE_VALF;
    [self.layer addAnimation:shakeAnim forKey:@"shakeAnim"];
    
    if (!_isFolder) {
        self.btnDel.hidden = NO;
        
        // 添加删除按钮由小变大动画
        CAKeyframeAnimation * expandAnim = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
        expandAnim.duration = 0.6;
        expandAnim.values = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.01f, 0.01f, 1.0f)],
                              [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.02f, 1.02f, 1.0f)],
                              [NSValue valueWithCATransform3D:CATransform3DIdentity]];
        expandAnim.keyTimes = @[@0.2f, @0.5f, @0.75f, @1.0f];
        expandAnim.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                       [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                       [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [self.btnDel.layer addAnimation:expandAnim forKey:@"expandAnim"];
    }
    
}

- (void)endEdit:(id)sender
{
    _launcherState = LauncherStateEndEdit;
    
    self.btnDel.hidden = YES;
    [self.layer removeAllAnimations];
    [self.btnDel.layer removeAllAnimations];
}

@end
