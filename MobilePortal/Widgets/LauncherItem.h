//
//  LauncherItem.h
//  iOSForFun
//
//  Created by a_超 on 15/2/11.
//  Copyright (c) 2015年 a_超. All rights reserved.
//

// 此控件集成了UIControl，但是不要手动加上touchUpInside方法，否则文件夹的效果无法产生
#import <UIKit/UIKit.h>
#import "LauncherDelegate.h"

@class LauncherItem;

#define LAUNCHER_ITEM_IMAGE_WIDTH 52
#define LAUNCHER_ITEM_TITLE_HEIGHT 16
#define LAUNCHER_ITEM_PADDING 5
#define LAUNCHER_ITEM_WIDTH (LAUNCHER_ITEM_PADDING + LAUNCHER_ITEM_IMAGE_WIDTH + LAUNCHER_ITEM_PADDING)
#define LAUNCHER_ITEM_HEIGHT (LAUNCHER_ITEM_PADDING + LAUNCHER_ITEM_IMAGE_WIDTH + LAUNCHER_ITEM_PADDING + LAUNCHER_ITEM_TITLE_HEIGHT + LAUNCHER_ITEM_PADDING)

@protocol LauncherItemDelegate <NSObject>

@optional
- (void)launcherItemDidClickDelete:(LauncherItem *)item;
- (void)launcherItemDidClick:(LauncherItem *)item;

@end

// 文件夹委托
@protocol LauncherFolderDelegate <NSObject>

@optional
- (void)launcherFolderDidClick:(LauncherItem *)item;

@end

@interface LauncherItem : UIControl <LauncherDelegate>

@property (nonatomic, weak) id<LauncherItemDelegate> delegate; // 作为普通对象时的委托
@property (nonatomic, weak) id<LauncherFolderDelegate> folderDelegate; // 作为文件夹时的委托
@property (nonatomic, strong) NSString * itemId;
@property (nonatomic, strong) NSString * title;
@property (nonatomic) NSUInteger index;  // 位于第几个位置
@property (nonatomic) NSUInteger pageNum; // 位于哪一页
@property (nonatomic) LauncherState launcherState; // 状态
@property (nonatomic) bool isFolder;
@property (nonatomic, strong) NSString * folderId;
@property (nonatomic, strong) NSMutableArray * folderItems;  // 此对象以 [array, array, array]形式存在，每一个array代表一页，一页中有多个图标
@property (nonatomic, strong) NSString * imageUrl; // 图标外网地址
@property (nonatomic, strong) NSString * actionType;
@property (nonatomic, strong) NSString * actionName;
@property (nonatomic) int newFlag;

- (instancetype)initWithFrame:(CGRect)frame isFolder:(BOOL)isFolder;

@end
