//
//  TIIWebView.h
//  MobilePortal
//
//  Created by Chenhc on 15/3/18.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TaPhoPersonalRes;
@class TIIWebView;

@protocol TIIWebViewDelegate <NSObject>

- (void)tiiWebViewDidFinishedLoading:(TIIWebView *)tiiWebView;
- (void)tiiWebView:(TIIWebView *)tiiWebView didFailLoadWithError:(NSError *)error;
- (void)tiiWebViewNeedsDestory:(TIIWebView *)tiiWebView;

@end

@interface TIIWebView : UIWebView

@property (nonatomic, strong) TaPhoPersonalRes * res;
@property (nonatomic, strong) NSString *issueId;
@property (nonatomic, strong) NSString *companyCode;
@property (nonatomic, weak) id<TIIWebViewDelegate> tiiWebViewDelegate;

@end
