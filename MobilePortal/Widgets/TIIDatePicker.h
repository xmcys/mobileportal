//
//  TIIDatePicker.h
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-6-9.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TIIDatePicker;

@protocol TIIDatePickerDelegate <NSObject>

@optional
- (void)tiiDatePicker:(TIIDatePicker *)datePicker didSelectDate:(NSString *)date;

@end

@interface TIIDatePicker : UIView

@property (nonatomic, weak) id<TIIDatePickerDelegate> delegate;

- (id)initWithDateFormatter:(NSString *)dateFormatter;

- (void)show;
- (void)dismiss;
- (void)setCurrentDate:(NSString *)currentDate;
- (NSString *)selectedFormattedDate;

@end
