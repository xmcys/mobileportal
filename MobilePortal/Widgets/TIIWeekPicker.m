//
//  TIIWeekPicker.m
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-29.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIIWeekPicker.h"
#import "CTPikcerView.h"
#import "TIISqlLite.h"

@interface TIIWeekPicker () <CTPikcerViewDelegate>

@property (nonatomic, strong) CTPikcerView * weekPicker;

@end

@implementation TIIWeekPicker

- (id)init
{
    if (self = [super init]) {
        
        NSDate * date = [NSDate date];
        NSCalendar * calendar = [NSCalendar currentCalendar];
        NSInteger unitFlags = NSYearCalendarUnit | NSWeekCalendarUnit;
        NSDateComponents * components = [calendar components:unitFlags fromDate:date];
        
        self.selectedYear = [components year];
        self.selectedWeek = [components week];
        
        NSMutableArray * years = [[NSMutableArray alloc] init];
        NSMutableArray * yValues = [[NSMutableArray alloc] init];
        for (int i = self.selectedYear + 5; i > 1900; i--) {
            [years addObject:[NSString stringWithFormat:@"%d", i]];
            [yValues addObject:[NSString stringWithFormat:@"%d年", i]];
        }
        
        NSMutableArray * months = [[NSMutableArray alloc] init];
        NSMutableArray * mValues = [[NSMutableArray alloc] init];
        for (int i = 1; i <= 53; i++) {
            if (i < 10) {
                [months addObject:[NSString stringWithFormat:@"0%d", i]];
                [mValues addObject:[NSString stringWithFormat:@"0%d周", i]];
            } else {
                [months addObject:[NSString stringWithFormat:@"%d", i]];
                [mValues addObject:[NSString stringWithFormat:@"%d周", i]];
            }
        }
        
        self.weekPicker = [[CTPikcerView alloc] initWithComponentNum:2 keyArray:[NSArray arrayWithObjects:years, months, nil] valueArray:[NSArray arrayWithObjects:yValues, mValues, nil] delegate:self cancelButtonTitle:@"取消" confirmButtonTitle:@"确定"];
        
        if (self.selectedWeek < 10) {
            [self setSelectedYearWeek:[NSString stringWithFormat:@"%d0%d", self.selectedYear, self.selectedWeek]];
        } else {
            [self setSelectedYearWeek:[NSString stringWithFormat:@"%d%d", self.selectedYear, self.selectedWeek]];
        }
        
    }
    return self;
}

- (NSString *)selectedYearWeekWithW
{
    if (self.selectedWeek < 10) {
        return [NSString stringWithFormat:@"%dW0%d", self.selectedYear, self.selectedWeek];
    } else {
        return [NSString stringWithFormat:@"%dW%d", self.selectedYear, self.selectedWeek];
    }
}

- (void)setSelectedYearWeek:(NSString *)yearWeek
{
    NSString * yw = [[yearWeek stringByReplacingOccurrencesOfString:@"W" withString:@""] stringByReplacingOccurrencesOfString:@"w" withString:@""];
    
    if (yw.length == 6) {
        self.selectedYear = [[yw substringToIndex:4] intValue];
        self.selectedWeek = [[yw substringFromIndex:4] intValue];
    }
    
    NSArray * years = [self.weekPicker.keyArray objectAtIndex:0];
    int maxYear = [[years objectAtIndex:0] intValue];
    int minYear = [[years objectAtIndex:years.count - 1] intValue];
    if (self.selectedYear > maxYear) {
        self.selectedYear = maxYear;
    }
    if (self.selectedYear < minYear) {
        self.selectedYear = minYear;
    }
    NSArray * weeks = [self.weekPicker.keyArray objectAtIndex:1];
    int minWeek = [[weeks objectAtIndex:0] intValue];
    int maxWeek = [[weeks objectAtIndex:weeks.count - 1] intValue];
    if (self.selectedWeek > maxWeek) {
        self.selectedWeek = maxWeek;
    }
    if (self.selectedWeek < minWeek) {
        self.selectedWeek = minWeek;
    }
    
    [self.weekPicker.pickerView selectRow:(maxYear - self.selectedYear) inComponent:0 animated:NO];
    [self.weekPicker.pickerView selectRow:(self.selectedWeek - 1) inComponent:1 animated:NO];
}

- (void)show
{
    [self.weekPicker show];
}

- (void)dismiss
{
    [self.weekPicker dismiss];
}
#pragma mark -
#pragma mark CTPickerView Delegate
- (void)ctPickerView:(CTPikcerView *)pickerView didClickedButtonOnIndex:(NSInteger)index
{
    if (index == 0) {
        NSString * maxYear = [[self.weekPicker.keyArray objectAtIndex:0] objectAtIndex:0];
        [self.weekPicker.pickerView selectRow:([maxYear intValue] - self.selectedYear) inComponent:0 animated:NO];
        [self.weekPicker.pickerView selectRow:(self.selectedWeek - 1) inComponent:1 animated:NO];
    } else {
        self.selectedYear = [[[self.weekPicker.keyArray objectAtIndex:0] objectAtIndex:[self.weekPicker.pickerView selectedRowInComponent:0]] intValue];
        self.selectedWeek = [[[self.weekPicker.keyArray objectAtIndex:1] objectAtIndex:[self.weekPicker.pickerView selectedRowInComponent:1]] intValue];
        if ([self.delegate respondsToSelector:@selector(tiiWeekPickerDidSelected:)]) {
            [self.delegate tiiWeekPickerDidSelected:self];
        }
    }
}

- (void)ctPickerView:(CTPikcerView *)pickerView didReceiveDateInterval:(UILabel *)dateInterVal {
    if (index == 0) {
        NSString * maxYear = [[self.weekPicker.keyArray objectAtIndex:0] objectAtIndex:0];
        [self.weekPicker.pickerView selectRow:([maxYear intValue] - self.selectedYear) inComponent:0 animated:NO];
        [self.weekPicker.pickerView selectRow:(self.selectedWeek - 1) inComponent:1 animated:NO];
    } else {
        self.selectedYear = [[[self.weekPicker.keyArray objectAtIndex:0] objectAtIndex:[self.weekPicker.pickerView selectedRowInComponent:0]] intValue];
        self.selectedWeek = [[[self.weekPicker.keyArray objectAtIndex:1] objectAtIndex:[self.weekPicker.pickerView selectedRowInComponent:1]] intValue];
    }
    NSString * year = [NSString stringWithFormat:@"%d", self.selectedYear];
    NSString * week = [NSString stringWithFormat:@"%d", self.selectedWeek];
    TIISqlLite *sql = [TIISqlLite shareManager];
    SearchCondition *item = [sql getInfoByCondition:year withWeek:week];
    dateInterVal.text = [NSString stringWithFormat:@"%@ -- %@", [item.beginDate stringByReplacingOccurrencesOfString:@"-" withString:@""], [item.endDate stringByReplacingOccurrencesOfString:@"-" withString:@""]];
}

@end
