//
//  SlideMenuController.m
//  iOSForFun
//
//  Created by Chenhc on 15/2/28.
//  Copyright (c) 2015年 Chenhc. All rights reserved.
//

#import "SlideMenuController.h"
#import "MPAuthViewController.h"

static const CGFloat kLeftViewInitalOffsetX = -80.0f;
static const CGFloat kOpenStartMinPositionX = 20;

@interface SlideMenuController ()

@property (nonatomic, strong) UIView * leftView;
@property (nonatomic, strong) UIView * centerView;
@property (nonatomic, strong) UIPanGestureRecognizer * panGestureRecognizer;
@property (nonatomic, strong) UITapGestureRecognizer * tapGestureRecognizer;
@property (nonatomic) CGPoint panGestureBeganLocation;
@property (nonatomic) CGPoint centerViewBeganLocation;
@property (nonatomic) CGPoint leftViewBeganLocation;


- (void)panGestureRecognized:(UIPanGestureRecognizer *) panGestureRecognizer;
- (void)tapGestureRecognized:(UITapGestureRecognizer *) tapGestureRecognizer;

@end

@implementation SlideMenuController

- (instancetype)initWithLeftViewController:(UIViewController *)leftViewController centerViewController:(UIViewController *)centerViewController
{
    if ([self initWithNibName:nil bundle:nil]) {
        _leftViewController = leftViewController;
        _centerViewController = centerViewController;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.slideMenuState = SlideMenuStateClosed;
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.leftView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.leftView.autoresizingMask = self.view.autoresizingMask;
    self.leftView.backgroundColor = [UIColor grayColor];
    self.leftView.center = CGPointMake(self.leftView.center.x + kLeftViewInitalOffsetX, self.leftView.center.y);
    [self.leftView addSubview:self.leftViewController.view];
    [self.view addSubview:self.leftView];
    
    self.centerView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.centerView.autoresizingMask = self.view.autoresizingMask;
    self.centerView.backgroundColor = [UIColor blackColor];
    self.centerView.layer.shadowOffset = CGSizeZero;
    self.centerView.layer.shadowOpacity = 0.7;
    [self.centerView addSubview:self.centerViewController.view];
    [self.view addSubview:self.centerView];
    
    _panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognized:)];
    _panGestureRecognizer.maximumNumberOfTouches = 1;
    [self.centerView addGestureRecognizer:_panGestureRecognizer];
    
    _tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognized:)];
    _tapGestureRecognizer.numberOfTapsRequired = 1;
    _tapGestureRecognizer.numberOfTouchesRequired = 1;
    [self.centerView addGestureRecognizer:_tapGestureRecognizer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recvLogOut:) name:NOTIFICATION_LOGOUT object:nil];

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *appId = [userDefaults valueForKey:AUTH_APPID];
    if (appId || appId.length > 0) {
        MPAuthViewController *controller = [[MPAuthViewController alloc] initWithNibName:@"MPAuthViewController" bundle:nil];
        controller.appId = appId;
        [self presentViewController:controller animated:YES completion:nil];
        [userDefaults setValue:nil forKey:AUTH_APPID];
        [userDefaults synchronize];
    }
    return;
}

- (void)recvLogOut:(NSNotification *)notification {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)panGestureRecognized:(UIPanGestureRecognizer *)panGestureRecognizer
{
    UIGestureRecognizerState state = panGestureRecognizer.state;
    
    // 手指的绝对坐标
    CGPoint location = [panGestureRecognizer locationInView:self.view];
    
    // 手指移动的速度
//    CGPoint velocity = [panGestureRecognizer velocityInView:self.view];

    switch (state) {
        case UIGestureRecognizerStateBegan:
            _panGestureBeganLocation = location;
            _centerViewBeganLocation = self.centerView.frame.origin;
            _leftViewBeganLocation = self.leftView.frame.origin;
            break;
            
        case UIGestureRecognizerStateChanged:
        {
            if (_gestureMustBeganFromMinPlace && _panGestureBeganLocation.x > kOpenStartMinPositionX && _slideMenuState == SlideMenuStateClosed) {
                return;
            }
            
            CGFloat offsetX = location.x - _panGestureBeganLocation.x;
            self.centerView.frame = CGRectMake(_centerViewBeganLocation.x + offsetX, self.centerView.frame.origin.y, self.centerView.frame.size.width, self.centerView.frame.size.height);
            if (self.centerView.frame.origin.x < 0) {
                self.centerView.frame = CGRectMake(0, self.centerView.frame.origin.y, self.centerView.frame.size.width, self.centerView.frame.size.height);
            } else if (self.centerView.frame.origin.x > self.view.bounds.size.width - fabs(kLeftViewInitalOffsetX)){
                self.centerView.frame = CGRectMake(self.view.bounds.size.width - fabs(kLeftViewInitalOffsetX), self.centerView.frame.origin.y, self.centerView.frame.size.width, self.centerView.frame.size.height);
            }
            
            CGFloat lvOffsetX = (offsetX / (self.view.bounds.size.width - fabs(kLeftViewInitalOffsetX))) * fabs(kLeftViewInitalOffsetX);
            self.leftView.frame = CGRectMake(_leftViewBeganLocation.x + lvOffsetX, self.leftView.frame.origin.y, self.leftView.frame.size.width, self.leftView.frame.size.height);
            if (self.leftView.frame.origin.x < kLeftViewInitalOffsetX) {
                self.leftView.frame = CGRectMake(kLeftViewInitalOffsetX, self.leftView.frame.origin.y, self.leftView.frame.size.width, self.leftView.frame.size.height);
            } else if (self.leftView.frame.origin.x > 0){
                self.leftView.frame = CGRectMake(0, self.leftView.frame.origin.y, self.leftView.frame.size.width, self.leftView.frame.size.height);
            }
            
            self.slideMenuState = SlideMenuStateMove;
        }
            break;
            
        case UIGestureRecognizerStateEnded:
        {
            if (_gestureMustBeganFromMinPlace && _panGestureBeganLocation.x > kOpenStartMinPositionX && _slideMenuState == SlideMenuStateClosed) {
                return;
            }
            
            if (fabs(location.x - _panGestureBeganLocation.x) < self.centerView.frame.size.width / 3) {
                if (_centerViewBeganLocation.x == 0) {
                    [UIView animateWithDuration:0.2 animations:^{
                        self.centerView.frame = CGRectMake(0, self.centerView.frame.origin.y, self.centerView.frame.size.width, self.centerView.frame.size.height);
                        self.leftView.frame = CGRectMake(kLeftViewInitalOffsetX, self.leftView.frame.origin.y, self.leftView.frame.size.width, self.leftView.frame.size.height);
                        self.slideMenuState = SlideMenuStateClosed;
                    }];
                } else {
                    [UIView animateWithDuration:0.2 animations:^{
                        self.centerView.frame = CGRectMake(self.view.bounds.size.width - fabs(kLeftViewInitalOffsetX), self.centerView.frame.origin.y, self.centerView.frame.size.width, self.centerView.frame.size.height);
                        self.leftView.frame = CGRectMake(0, self.leftView.frame.origin.y, self.leftView.frame.size.width, self.leftView.frame.size.height);
                        self.slideMenuState = SlideMenuStateOpen;
                    }];
                }
                
            } else {
                if (_centerViewBeganLocation.x == 0 && location.x > _panGestureBeganLocation.x) {
                    [UIView animateWithDuration:0.2 animations:^{
                        self.centerView.frame = CGRectMake(self.view.bounds.size.width - fabs(kLeftViewInitalOffsetX), self.centerView.frame.origin.y, self.centerView.frame.size.width, self.centerView.frame.size.height);
                        self.leftView.frame = CGRectMake(0, self.leftView.frame.origin.y, self.leftView.frame.size.width, self.leftView.frame.size.height);
                        self.slideMenuState = SlideMenuStateOpen;
                    }];
                } else if (_centerViewBeganLocation.x == self.view.bounds.size.width - fabs(kLeftViewInitalOffsetX) && location.x < _panGestureBeganLocation.x){
                    [UIView animateWithDuration:0.2 animations:^{
                        self.centerView.frame = CGRectMake(0, self.centerView.frame.origin.y, self.centerView.frame.size.width, self.centerView.frame.size.height);
                        self.leftView.frame = CGRectMake(kLeftViewInitalOffsetX, self.leftView.frame.origin.y, self.leftView.frame.size.width, self.leftView.frame.size.height);
                        self.slideMenuState = SlideMenuStateClosed;
                    }];
                }
            }
        }
            break;
            
        default:
            break;
    }
}

- (void)tapGestureRecognized:(UITapGestureRecognizer *)tapGestureRecognizer
{
    if (self.centerView.frame.origin.x != 0) {
        [UIView animateWithDuration:0.2 animations:^{
            self.centerView.frame = CGRectMake(0, self.centerView.frame.origin.y, self.centerView.frame.size.width, self.centerView.frame.size.height);
            self.leftView.frame = CGRectMake(kLeftViewInitalOffsetX, self.leftView.frame.origin.y, self.leftView.frame.size.width, self.leftView.frame.size.height);
            self.slideMenuState = SlideMenuStateClosed;
        }];
    }
}

- (void)setLeftViewOpened:(BOOL)opened
{
    if (opened) {
        [UIView animateWithDuration:0.2 animations:^{
            self.centerView.frame = CGRectMake(self.view.bounds.size.width - fabs(kLeftViewInitalOffsetX), self.centerView.frame.origin.y, self.centerView.frame.size.width, self.centerView.frame.size.height);
            self.leftView.frame = CGRectMake(0, self.leftView.frame.origin.y, self.leftView.frame.size.width, self.leftView.frame.size.height);
            self.slideMenuState = SlideMenuStateOpen;
        }];
    } else {
        [UIView animateWithDuration:0.2 animations:^{
            self.centerView.frame = CGRectMake(0, self.centerView.frame.origin.y, self.centerView.frame.size.width, self.centerView.frame.size.height);
            self.leftView.frame = CGRectMake(kLeftViewInitalOffsetX, self.leftView.frame.origin.y, self.leftView.frame.size.width, self.leftView.frame.size.height);
            self.slideMenuState = SlideMenuStateClosed;
        }];
    }
}

- (void)setTapGestureEnabled:(BOOL)enabled
{
    [self.centerView removeGestureRecognizer:_tapGestureRecognizer];
    
    if (enabled) {
        [self.centerView addGestureRecognizer:_tapGestureRecognizer];
    }
}

- (void)setPanGestureEnabled:(BOOL)enabled
{
    [self.centerView removeGestureRecognizer:_panGestureRecognizer];
    
    if (enabled) {
        [self.centerView addGestureRecognizer:_panGestureRecognizer];
    }
}

@end
