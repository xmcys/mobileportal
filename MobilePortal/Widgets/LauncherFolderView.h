//
//  LauncherFloderView.h
//  iOSForFun
//
//  Created by a_超 on 15/2/13.
//  Copyright (c) 2015年 a_超. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LauncherDelegate.h"

@class LauncherScrollView;
@class LauncherItem;
@class LauncherFolderView;

@protocol LauncherFolderViewDelegate <NSObject>

@optional
// 文件夹内部图标长按启动编辑模式时的事件
-(void)launcherFolderViewDidBeginEdit:(LauncherFolderView *)launcherFolder;
// 文件夹内部图标移除文件夹外时的事件
-(void)launcherFolderViewDidRemove:(LauncherFolderView *)launcherFolder launcherItem:(LauncherItem *)item;

@end

@interface LauncherFolderView : UIControl <LauncherDelegate>

// 滚动视图
@property (nonatomic, strong) LauncherScrollView * launcherScroll; // 图标滚动视图
@property (nonatomic, strong) UITextField * titleField;  // 标题栏
@property (nonatomic, strong) LauncherItem * folderItem; // 文件夹对象
@property (nonatomic, weak) id<LauncherFolderViewDelegate> delegate; 

@end
