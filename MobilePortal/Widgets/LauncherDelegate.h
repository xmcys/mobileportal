//
//  LauncherDelegate.h
//  iOSForFun
//
//  Created by a_超 on 15/2/12.
//  Copyright (c) 2015年 a_超. All rights reserved.
//

typedef NS_ENUM(NSUInteger, LauncherState){
    LauncherStateBeganEdit, // 编辑状态
    LauncherStateEndEdit,    // 正常状态
    LauncherStateMoving // 移动
};

// 定义通用接口名称（相当于java的interface）
@protocol LauncherDelegate <NSObject>

// 手指按下事件
- (void)touchDown:(id)sender;
// 手指抬起事件
- (void)touchUp:(id)sender;
// 进入编辑状态
- (void)beganEdit:(id)sender;
// 回到正常状态
- (void)endEdit:(id)sender;

@end
