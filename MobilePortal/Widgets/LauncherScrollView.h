//
//  LauncherScrollView.h
//  iOSForFun
//
//  Created by a_超 on 15/2/11.
//  Copyright (c) 2015年 a_超. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LauncherItem.h"

@class LauncherScrollView;

@protocol LauncherScrollDelegate <NSObject>

- (void)launcherScrollDidBeginEdit:(LauncherScrollView *)scrollView;
- (void)launcherScrollDidEndEdit:(LauncherScrollView *)scrollView;

@optional
// 图标被移除时的事件（此场景应用于本视图作为文件夹对象的滚动容器）
- (void)launcherScrollDidRemove:(LauncherScrollView *)scrollView launcherItem:(LauncherItem *)item;

@end

@interface LauncherScrollView : UIScrollView <LauncherDelegate>

@property (nonatomic, weak) id<LauncherScrollDelegate> launcherScrollDelegate;
@property (nonatomic, strong) NSMutableArray * pages; // 此对象以 [array, array, array]形式存在，代表页数，每一个array包含多个launcherItem
@property (nonatomic, readonly) NSUInteger column; // 每一页的列数
@property (nonatomic) LauncherState launcherState;
@property (nonatomic) bool canCreateFolder;  // 是否可以创建文件夹（作为文件夹对象的滚动容器时，设置为NO）
@property (nonatomic, readonly) NSUInteger maxNumPerPage; // 每页最大图标数，默认 column * column

- (instancetype) initWithFrame:(CGRect)frame withColumn:(NSUInteger)column;

// 重置图标页
- (void)layoutLauncherItems:(NSMutableArray *)pages;

- (void)deleteLauncherItem:(LauncherItem *)item;
- (void)addLauncherItem:(LauncherItem *)item;

// 隐藏文件夹视图
- (void)removeFolderScroll;

@end
