//
//  LauncherFloderView.m
//  iOSForFun
//
//  Created by a_超 on 15/2/13.
//  Copyright (c) 2015年 a_超. All rights reserved.
//

#import "LauncherFolderView.h"
#import "LauncherScrollView.h"
#import "LauncherItem.h"

@interface LauncherFolderView () <LauncherScrollDelegate, UITextFieldDelegate>

@property (nonatomic, strong) UIImageView * editBg; // 标题背景图
@property (nonatomic, strong) UIButton * btnClear;  // 标题清理按钮

- (void)clearText;

@end

@implementation LauncherFolderView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1.0];
        
        UIImageView * bg = [[UIImageView alloc] initWithFrame:self.bounds];
        bg.image = [UIImage imageNamed:@"folder_bg"];
        bg.contentMode = UIViewContentModeScaleToFill;
        [self addSubview:bg];
        
        UIImageView * scrollBg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 306, 320)];
        scrollBg.image = [[UIImage imageNamed:@"folder_scroll_bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(296/2-1, 296/2-1, 296/2-1, 296/2-1)];
        scrollBg.contentMode = UIViewContentModeScaleToFill;
        [self addSubview:scrollBg];
        
        self.launcherScroll = [[LauncherScrollView alloc] initWithFrame:CGRectMake(0, 0, 296, 310) withColumn:3];
        _launcherScroll.backgroundColor = [UIColor clearColor];
        _launcherScroll.layer.cornerRadius = 10.0f;
        _launcherScroll.center = CGPointMake(frame.size.width / 2, frame.size.height / 2 + 20);
        _launcherScroll.canCreateFolder = NO; // 文件夹内不能再创建文件夹
        _launcherScroll.launcherScrollDelegate = self;
        
        scrollBg.center = _launcherScroll.center;
        
        [self addSubview:_launcherScroll];
        
        
        self.editBg = [[UIImageView alloc] initWithFrame:CGRectMake((frame.size.width - _launcherScroll.frame.size.width) / 2, _launcherScroll.frame.origin.y - 60, _launcherScroll.frame.size.width, 50)];
        self.editBg.image = [[UIImage imageNamed:@"folder_edit_bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(16, 4, 16, 5)];
        self.editBg.alpha = 0.0;
        self.editBg.contentMode = UIViewContentModeScaleToFill;
        [self addSubview:self.editBg];
        
        self.titleField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, self.editBg.frame.size.width - 60, self.editBg.frame.size.height)];
        _titleField.center = self.editBg.center;
        _titleField.backgroundColor = [UIColor clearColor];
        _titleField.enabled = NO;
        _titleField.text = @"";
        _titleField.font = [UIFont systemFontOfSize:25.0f];
        _titleField.textColor = [UIColor whiteColor];
        _titleField.textAlignment = NSTextAlignmentCenter;
        _titleField.returnKeyType = UIReturnKeyDone;
        _titleField.delegate = self;
        
        self.btnClear = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnClear.frame = CGRectMake(0, 0, LAUNCHER_ITEM_TITLE_HEIGHT * 1.20, LAUNCHER_ITEM_TITLE_HEIGHT * 1.20);
        _btnClear.center = CGPointMake(self.editBg.frame.origin.x + self.editBg.frame.size.width - _btnClear.frame.size.width / 2 - 5, self.editBg.frame.origin.y + self.editBg.frame.size.height / 2);
        _btnClear.backgroundColor = [UIColor clearColor];
        [_btnClear setBackgroundImage:[UIImage imageNamed:@"icon_del"] forState:UIControlStateNormal];
        [_btnClear setBackgroundImage:[UIImage imageNamed:@"icon_del"] forState:UIControlStateHighlighted];
        _btnClear.alpha = 0.0;
        [_btnClear addTarget:self action:@selector(clearText) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_btnClear];
        
        [self addSubview:_titleField];
    }
    return self;
}

- (void)clearText
{
    self.titleField.text = @"";
}

- (void)setFolderItem:(LauncherItem *)folderItem
{
    _folderItem = folderItem;
    _titleField.text = _folderItem.title;
    [_launcherScroll layoutLauncherItems:_folderItem.folderItems];
}

#pragma mark -
#pragma mark Launcher Delegate
- (void)beganEdit:(id)sender
{
    // 通知图标滚动视图进入编辑状态（此场景应用于外层launcher进入编辑状态后，要通知文件夹视图里的子launcher也进入编辑状态）
    if (_launcherScroll.launcherState == LauncherStateEndEdit) {
        [_launcherScroll beganEdit:sender];
    }
    
    // 设置标题可编辑
    [UIView animateWithDuration:0.2 animations:^{
        _titleField.enabled = YES;
        self.btnClear.alpha = 1.0;
        self.editBg.alpha = 1.0;
    }];
    
}

- (void)endEdit:(id)sender
{
    [_titleField resignFirstResponder];
    [_launcherScroll endEdit:sender];
    _folderItem.title = _titleField.text;
    // 设置标题不可编辑
    [UIView animateWithDuration:0.2 animations:^{
        _titleField.enabled = NO;
        self.btnClear.alpha = 0.0;
        self.editBg.alpha = 0.0;
    }];
}

#pragma mark -
#pragma mark Launcher ScrollView Delegate
- (void)launcherScrollDidBeginEdit:(LauncherScrollView *)scrollView
{
    [UIView animateWithDuration:0.2 animations:^{
        _titleField.enabled = YES;
        self.btnClear.alpha = 1.0;
        self.editBg.alpha = 1.0;
    }];
    
    // 通知委托对象进入编辑状态（此场景应用于打开文件夹后，长按文件夹图标，要通知最外层的launcher进入编辑状态）
    if ([self.delegate respondsToSelector:@selector(launcherFolderViewDidBeginEdit:)]) {
        [self.delegate launcherFolderViewDidBeginEdit:self];
        [self.superview bringSubviewToFront:self];
    }
}

- (void)launcherScrollDidRemove:(LauncherScrollView *)scrollView launcherItem:(LauncherItem *)item
{
    // 移除某个图标
    NSMutableArray * folderPage = [_folderItem.folderItems objectAtIndex:item.pageNum];
    
    [folderPage removeObject:item];
    item.folderId = nil;
    
    // 通知委托对象此图标被子launcher移除，要加入委托对象的launcher
    if ([self.delegate respondsToSelector:@selector(launcherFolderViewDidRemove:launcherItem:)]) {
        [self.delegate launcherFolderViewDidRemove:self launcherItem:item];
    }
}

#pragma mark -
#pragma mark UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
