function Loading()
{
	var btn = null;
	var mLoading = null;
	var interval = null;
	var btnClickEvn = null;
	
	this.init = function(btnID, interval)
	{
		if($("#loading_id_0806449") != null)
		{
			$("#loading_id_0806449").remove();
		}
		
		$("body").append("<div id='loading_id_0806449' style='width:31px; height:31px; z-index:2000; position:absolute; left:50%; top:50%; background:url(../images/loading.gif) no-repeat;'></div>");
		var myLoadingObj = $("#loading_id_0806449");
		myLoadingObj.hide();
		
		this.btn = $("#" + btnID);
		this.interval = interval;
		this.btnClickEvn = this.btn.attr("onclick");
		this.mLoading = myLoadingObj;
	};
	
	function buttonStateChange()
	{
		if(this.btn != null)
		{
			if(this.btn.is("div"))
			{
				if(this.btnClickEvn != null)
				{
					this.btn.attr("onclick", this.btnClickEvn);
				}
			}
			else
			{
				this.btn.removeAttr("disabled");
			}
		}
	}
	
	this.show = function()
	{
		if(this.btn != null)
		{
			if(this.btn.is("div"))
			{
				this.btn.removeAttr("onclick");
			}
			else
			{
				this.btn.attr("disabled", "disabled");
			}
			
			if(this.interval != null || this.interval != 0)
			{
				setTimeout(buttonStateChange, this.interval);
			}
		}
		this.mLoading.show();
	};
	
	this.hide = function ()
	{
		if(this.btn != null)
		{
			if(this.btn.is("div"))
			{
				this.btn.attr("onclick", this.btnClickEvn);
			}
			else
			{
				this.btn.removeAttr("disabled");
			}
		}
		
		this.mLoading.hide();
	};
	
	this.intBtnBack = function(btnBack)
	{
		var backBtn = $("#" + btnBack);
		if(history.length < 2)
		{
			backBtn.hide();
		}
		else
		{
			backBtn.show();
		}
	};
}

