function YearMonth(param)
{
	var range = param;
	var date = new Date();
	var curYear = parseInt(date.getFullYear());
	var curMonth = parseInt(date.getMonth()) + 1;
	
	this.setIntoDiv = function(divID)
	{
		var div = $("#" + divID);
		var selectID = "select_id_0806449_" + divID + "";
		var select = "<select id='" + selectID + "'>";
		
		for(var i = curMonth - range; i <= curMonth; i++)
		{
			var month = i;
			var year = curYear;
			
			if(i <= 0)
			{
				year -= (1 + parseInt(-i / 12));
				month = 12 + i % 12;
			}
			
			var text = "" + year + ((month < 10)?("0" + month) : month);
			select += "<option  value='" + text + "'>" + text + "</option>";
		}
		
		select += "</select>";
		
		div.append(select);
		var selectObj = $("#" + selectID);
		selectObj.val("" + curYear + ((curMonth < 10)?("0" + curMonth) : curMonth));
		return selectObj;
	};
	
	this.setIntoDivByClass = function(divClassName)
	{
		var div = $("." + divClassName);
		var select = "<ul>";
		
		for(var i = curMonth; i >= curMonth - range ; i--)
		{
			var month = i;
			var year = curYear;
			
			if(i <= 0)
			{
				year -= (1 + parseInt(-i / 12));
				month = 12 + i % 12;
			}
			
			var value = "" + year + ((month < 10)?("0" + month) : month);
			var text = year + "/" + ((month < 10)?("0" + month) : month);
			
			if(i == curMonth)
			{
				select += "<li class='on' value='" + value + "'>" + text + "</li>";
			}
			else
			{
				select += "<li value='" + value + "'>" + text + "</li>";
			}
		}
		
		select += "</ul>";
		
		div.append(select);
		//div.scrollTop(div.height() + 65);
		var curDate = curYear + "/" + ((curMonth < 10)?("0" + curMonth) : curMonth);
		return curDate;
	};
	
	this.setIntoSelect = function(selectID)
	{
		var select = $("#" + selectID);
		
		for(var i = curMonth - range; i <= curMonth; i++)
		{
			var month = i;
			var year = curYear;
			
			if(i <= 0)
			{
				year -= (1 + parseInt(-i / 12));
				month = 12 + i % 12;
			}
			
			var text = "" + year + ((month < 10)?("0" + month) : month);
			select.append("<option  value='" + text + "'>" + text + "</option>");
			select.val("" + curYear + ((curMonth < 10)?("0" + curMonth) : curMonth));
		}
	};
	
	this.getSelectOptions = function()
	{
		var options = new Array();;
		var j = 0;
		
		for(var i = curMonth - range; i <= curMonth; i++)
		{
			var month = i;
			var year = curYear;
			
			if(i <= 0)
			{
				year -= (1 + parseInt(-i / 12));
				month = 12 + i % 12;
			}
			
			var text = "" + year + ((month < 10)?("0" + month) : month);
			
			options[j] = text;
		}
		
		return options;
	};
}

function YearMonthMonthsAgo(param, ago)
{
	var range = param;
	var date = new Date();
	var curYear = parseInt(date.getFullYear());
	var curMonth = parseInt(date.getMonth()) + 1;
	
	if (ago > 12) {
		ago = 12;
	}
	if (curMonth - ago <= 0) {
		curMonth = curMonth + 12 - ago;
		curYear -= 1;
	} else {
		curMonth = curMonth - ago;
	}
	this.setIntoDiv = function(divID)
	{
		var div = $("#" + divID);
		var selectID = "select_id_0806449_" + divID + "";
		var select = "<select id='" + selectID + "'>";
		
		for(var i = curMonth - range; i <= curMonth; i++)
		{
			var month = i;
			var year = curYear;
			
			if(i <= 0)
			{
				year -= (1 + parseInt(-i / 12));
				month = 12 + i % 12;
			}
			
			var text = "" + year + ((month < 10)?("0" + month) : month);
			select += "<option  value='" + text + "'>" + text + "</option>";
		}
		
		select += "</select>";
		
		div.append(select);
		var selectObj = $("#" + selectID);
		selectObj.val("" + curYear + ((curMonth < 10)?("0" + curMonth) : curMonth));
		return selectObj;
	};
	
	this.setIntoDivByClass = function(divClassName)
	{
		var div = $("." + divClassName);
		var select = "<ul>";
		
		for(var i = curMonth; i >= curMonth - range ; i--)
		{
			var month = i;
			var year = curYear;
			
			if(i <= 0)
			{
				year -= (1 + parseInt(-i / 12));
				month = 12 + i % 12;
			}
			
			var value = "" + year + ((month < 10)?("0" + month) : month);
			var text = year + "/" + ((month < 10)?("0" + month) : month);
			
			if(i == curMonth)
			{
				select += "<li class='on' value='" + value + "'>" + text + "</li>";
			}
			else
			{
				select += "<li value='" + value + "'>" + text + "</li>";
			}
		}
		
		select += "</ul>";
		
		div.append(select);
		//div.scrollTop(div.height() + 65);
		var curDate = curYear + "/" + ((curMonth < 10)?("0" + curMonth) : curMonth);
		return curDate;
	};
	
	this.setIntoSelect = function(selectID)
	{
		var select = $("#" + selectID);
		
		for(var i = curMonth - range; i <= curMonth; i++)
		{
			var month = i;
			var year = curYear;
			
			if(i <= 0)
			{
				year -= (1 + parseInt(-i / 12));
				month = 12 + i % 12;
			}
			
			var text = "" + year + ((month < 10)?("0" + month) : month);
			select.append("<option  value='" + text + "'>" + text + "</option>");
			select.val("" + curYear + ((curMonth < 10)?("0" + curMonth) : curMonth));
		}
	};
	
	this.getSelectOptions = function()
	{
		var options = new Array();;
		var j = 0;
		
		for(var i = curMonth - range; i <= curMonth; i++)
		{
			var month = i;
			var year = curYear;
			
			if(i <= 0)
			{
				year -= (1 + parseInt(-i / 12));
				month = 12 + i % 12;
			}
			
			var text = "" + year + ((month < 10)?("0" + month) : month);
			
			options[j] = text;
		}
		
		return options;
	};
}