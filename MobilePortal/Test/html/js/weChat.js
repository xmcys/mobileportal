// 拓展 - 获取URL参数
$.extend({
	getUrlVars :function(name){
		var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
		var r = window.location.search.substr(1).match(reg);
		if (r!=null) return unescape(r[2]); return null;
	}
});

// 拓展 - 根据是否有返回页动态隐藏返回按钮
$.extend({
	getHistory:function(name){
			if(typeof name =="string" && window.history.length<2){
				$("#"+name+"").hide();
			}
	}
});

// 拓展 - 权限检查
$.extend({
	checkPermission:function(userType){
			var wxId = $.getUrlVars("wxId");
			$.ajax({
				type: "GET",
		        dataType: "json",
		        url: "../weChatBind.do?f=getUserInfoByWxId&wxId=" + wxId,
		        success: function(user){
		            if (user == null || userType != user.userType) {
		            	$("body").hide();
						self.location = "../pages/weChatBind.html?wxId=" + wxId + "&userType=" + userType + "&redirect=" + escape(self.location);
						return;
					}      
		        },
		        error: function(){
		            self.location = "../commons/404.html";
		            return;
		        }
			});
	}
});

//拓展 - 权限检查（龙岩、三明等）
$.extend({
	checkPermissionPublic:function(userTypeTemp){
			var wxId = $.getUrlVars("wxId");
			var userType = userTypeTemp;
			$.ajax({
				type: "GET",
		        	dataType: "json",
		        	url: "../weChatBind.do?f=getUserInfoByWxId&wxId=" + wxId,
		        success: function(user){
		            if (user == null || userType != user.userType) {
		            	$("body").hide();
						self.location = "../pages/weChatBindLy.html?wxId=" + wxId + "&userType=" + userType + "&redirect=" + escape(self.location);
						return;
					}      
		        },
		        error: function(){
		            self.location = "../commons/404.html";
		            return;
		        }
			});
	}
});

$.extend({
	// 判断是否使用微信打开
	openInWx:function(){
		document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {
			WeixinJSBridge.call('hideToolbar');
//			WeixinJSBridge.call('hideOptionMenu');
		});
		var ua = navigator.userAgent.toLowerCase();
		if(ua.match(/MicroMessenger/i) == 'micromessenger') {
			return true;
		} else {
//			$("body").hide();
//			alert("请使用微信内置浏览器访问");
//			self.location = "../commons/404.html";
			return false;
		}
	}
});

//隐藏微信中网页右上角按钮
$.extend({
	hideRightTopMenu:function(){
		if(typeof WeixinJSBridge == "undefined"){
			if(document.addEventListener){
				document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
		    }else if(document.attachEvent){
		        document.attachEvent('WeixinJSBridgeReady', onBridgeReady); 
		        document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
		    }
		}else{
		    onBridgeReady();
		}
	}
});
function onBridgeReady(){
	WeixinJSBridge.call('hideOptionMenu');
}

//拓展 - 锁定表格列头
$.extend({
	tableLock:function FixTable(TableID, FixColumnNumber, width, height) {
	if ($("#" + TableID + "_tableLayout").length != 0) {
     $("#" + TableID + "_tableLayout").before($("#" + TableID));
     $("#" + TableID + "_tableLayout").empty();
     $("#" + TableID + "_tableLayout").css("width", width);
     $("#" + TableID + "_tableLayout").css("height", height);
	 }
	 else {
	     $("#" + TableID).after("<div id='" + TableID + "_tableLayout' style='overflow:hidden;height:" + height + "px; width:" + width + "px;'></div>");
	 }
	 $('<div id="' + TableID + '_tableFix"></div>'
	 + '<div id="' + TableID + '_tableHead"></div>'
	 + '<div id="' + TableID + '_tableColumn"></div>'
	 + '<div id="' + TableID + '_tableData"></div>').appendTo("#" + TableID + "_tableLayout");
	 var oldtable = $("#" + TableID);
	 var tableFixClone = oldtable.clone(true);
	 tableFixClone.attr("id", TableID + "_tableFixClone");
	 $("#" + TableID + "_tableFix").append(tableFixClone);
	 var tableHeadClone = oldtable.clone(true);
	 tableHeadClone.attr("id", TableID + "_tableHeadClone");
	 $("#" + TableID + "_tableHead").append(tableHeadClone);
	 var tableColumnClone = oldtable.clone(true);
	 tableColumnClone.attr("id", TableID + "_tableColumnClone");
	 $("#" + TableID + "_tableColumn").append(tableColumnClone);
	 $("#" + TableID + "_tableData").append(oldtable);
	 $("#" + TableID + "_tableLayout table").each(function () {
	     $(this).css("margin", "0");
	 });
	 var HeadHeight = $("#" + TableID + "_tableHead thead").height();
	 HeadHeight += 2;
	 $("#" + TableID + "_tableHead").css("height", HeadHeight);
	 
	 $("#" + TableID + "_tableFix").css("height", HeadHeight);
	 var ColumnsWidth = 0;
	 var ColumnsNumber = 0;
	 $("#" + TableID + "_tableColumn tr:last td:lt(" + FixColumnNumber + ")").each(function () {
	     ColumnsWidth += $(this).outerWidth(true);
	     ColumnsNumber++;
	 });
	 ColumnsWidth += 2;
	 if ($.browser.msie) {
	     switch ($.browser.version) {
	         case "7.0":
	             if (ColumnsNumber >= 3) ColumnsWidth--;
	             break;
	         case "8.0":
	             if (ColumnsNumber >= 2) ColumnsWidth--;
	             break;
	     }
	 }
	 $("#" + TableID + "_tableColumn").css("width", ColumnsWidth);
	 $("#" + TableID + "_tableFix").css("width", ColumnsWidth);
	 $("#" + TableID + "_tableData").scroll(function () {
	     $("#" + TableID + "_tableHead").scrollLeft($("#" + TableID + "_tableData").scrollLeft());
	     $("#" + TableID + "_tableColumn").scrollTop($("#" + TableID + "_tableData").scrollTop());
	 });
	 $("#" + TableID + "_tableFix").css({ "overflow": "hidden", "position": "relative", "z-index": "50", "background-color": "#FFF" });
	 //    部分移动设备列头无法跟着滚动，因此隐藏掉固定列外的其他猎列头。  不过会造成当高度是写死的时候，向上滚动列头无法显示的问题
	 //    $("#" + TableID + "_tableHead").css({ "overflow": "hidden", "width": width - 10, "position": "relative", "z-index": "45", "background-color": "#FFF" });
	 $("#" + TableID + "_tableHead").css({ "overflow": "hidden", "width": ColumnsWidth, "position": "relative", "z-index": "45", "background-color": "#FFF" });
	 $("#" + TableID + "_tableColumn").css({ "overflow": "hidden", "height": height - 10, "position": "relative", "z-index": "40", "background-color": "#FFF" });
	 $("#" + TableID + "_tableData").css({ "overflow": "scroll", "overflow-y": "hidden", "width": width, "height": height, "position": "relative", "z-index": "35" });
	 if ($("#" + TableID + "_tableHead").width() > $("#" + TableID + "_tableFix table").width()) {
	     $("#" + TableID + "_tableHead").css("width", $("#" + TableID + "_tableFix table").width());
	     $("#" + TableID + "_tableData").css("width", $("#" + TableID + "_tableFix table").width() + 17);
	 }
	 if ($("#" + TableID + "_tableColumn").height() > $("#" + TableID + "_tableColumn table").height()) {
	     $("#" + TableID + "_tableColumn").css("height", $("#" + TableID + "_tableColumn table").height());
	     $("#" + TableID + "_tableData").css("height", $("#" + TableID + "_tableColumn table").height() + 17);
	 }
	 $("#" + TableID + "_tableFix").offset($("#" + TableID + "_tableLayout").offset());
	 $("#" + TableID + "_tableHead").offset($("#" + TableID + "_tableLayout").offset());
	 $("#" + TableID + "_tableColumn").offset($("#" + TableID + "_tableLayout").offset());
	 $("#" + TableID + "_tableData").offset($("#" + TableID + "_tableLayout").offset());
	}
});


$.fn.smartFloat = function() {
    var position = function(element) {
        var top = element.position().top, pos = element.css("position");
		var right = element.position().right, pos = element.css("position");
        $(window).scroll(function() {
            var scrolls = $(this).scrollTop();
            if (scrolls > top) {
                if (window.XMLHttpRequest) {
                    element.css({
                        position: "fixed",
                        top: 0,
						right:12
                    });    
                } else {
                    element.css({
                        top: scrolls
                    });    
                }
            }else {
                element.css({
                    position: "absolute",
                    top: top,
					right:6
                });    
            }
        });
    };
    return $(this).each(function() {
        position($(this));                         
    });
};